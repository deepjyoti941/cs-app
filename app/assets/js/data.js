console.log("in Data.js");
// window.catData = [{"active":false,"subCat":[{"active":false,"subCat":[{"subCat":[{"subCat":[],"text":"I want to use my system font. Hike doesn't support that!","active":false,"label":"Custom fonts","theme":"HomeScreen5.0"},{"subCat":[],"text":"I would like to change the font size while using hike.","active":false,"label":"Custom fonts","theme":"HomeScreen5.0"}],"text":"Custom fonts","active":false},{"subCat":[{"subCat":[],"text":"I want to change my registered mobile number without loosing any of my Groups and Chats","active":false,"label":"Change Number","theme":"CPR"},{"subCat":[],"text":"I want to use two mobile numbers on hike at the same time.","active":true,"label":"Change Number","theme":"CPR"},{"text":"Others","subCat":[],"active":false,"label":"Change Number","theme":"CPR"}],"text":"Change Number","active":false},{"subCat":[{"subCat":[],"text":"I want a dark chat theme which is less bright in dim light especially for chatting in the night time.","active":false,"label":"Night mode","theme":"HomeScreen5.0"},{"subCat":[],"text":"I want to reduce the brightness of the screen.","active":false,"label":"Night mode","theme":"HomeScreen5.0"},{"subCat":[],"text":"I want the font color, icon color etc. to be changed when enabling night mode.","active":true,"label":"Night mode","theme":"HomeScreen5.0"}],"text":"Night Mode","active":false},{"subCat":[{"subCat":[],"text":"I want hike app widget on my phone home screen.","active":false,"label":"Widgets","theme":"HomeScreen5.0"}],"text":"Widgets","active":false,"theme":"Growth","label":"Growth"},{"subCat":[{"subCat":[],"text":"I want to know the message delivery and read time of a message","active":true,"theme":"ChatExperience","label":"Message Info"}],"text":"Message Information","active":false,"label":"Message Info","theme":"ChatExperience"},{"subCat":[{"subCat":[],"text":"Please introduce hike video calling soon.","active":true,"label":"Video Calling","theme":"VOIP&Video"}],"text":"Video Calling","active":false},{"text":"Online indicator with a green dot for Online contact","subCat":[{"text":"On hike home screen","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Online Green dot"},{"text":"On New Compose screen","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Online Green dot"}],"active":false},{"subCat":[{"subCat":[],"text":"I want a quick reply window to answer to an incoming message without opening the app.","active":true,"label":"Pop up notification","theme":"ChatExperience"}],"text":"Pop-up notifications/Quick Reply","active":false},{"subCat":[],"text":"An unread message counter on hike icon would be great.","active":false,"label":"Message Counter on icon","theme":"Growth"},{"subCat":[{"subCat":[],"text":"I want hike support for the multi-screen feature in my phone.","active":true,"theme":"HomeScreen5.0","label":"Multi-Screen"}],"text":"Multi-screen","active":false},{"subCat":[],"text":"Web/PC app","active":false,"label":"Web Client","theme":"CPR"},{"subCat":[{"subCat":[],"text":"I want to designate different notification tones to my one-on-one and group chats.","active":false,"label":"Custom Notification","theme":"ChatExperience"},{"subCat":[],"text":"I want different notification tones for Group Chats and one-on-one chats.","active":true,"label":"Custom Notification","theme":"ChatExperience"}],"text":"Custom Notification Tones","active":false},{"subCat":[{"subCat":[],"text":"Please add a feature to mute a one-on-one chat.","active":false,"label":"Mute One to One Chats","theme":"ChatExperience"},{"subCat":[],"text":"I want to mute a chat for a particular time limit.","active":true,"theme":"ChatExperience","label":"Mute chat"}],"text":"Mute a chat","active":false},{"subCat":[],"text":"Turn Off Read Notifications","active":false,"label":"Read notification","theme":"ChatExperience"},{"subCat":[],"text":"Gesture based swipes","active":false,"label":"Gestures","theme":"HomeScreen5.0"},{"subCat":[],"text":"Hide content previews in notifications","active":false,"label":"Hide preview in notification","theme":"Growth"},{"subCat":[],"text":"Birthday reminders","active":false,"label":"Birthday reminder","theme":"Growth"},{"subCat":[],"text":"Schedule Messages","active":false,"label":"Schedule Messages","theme":"ChatExperience"},{"subCat":[],"text":"GIF support","active":false,"label":"GIF","theme":"ExpressYourself"},{"subCat":[],"text":"Chat Heads","active":false,"label":"Chat Heads","theme":"ChatExperience"},{"subCat":[{"text":"Kabaddi","subCat":[],"active":false,"theme":"Content","label":"I want Live Kabaddi score updates"},{"text":"Football","subCat":[],"active":false,"theme":"Content","label":"I want Live Football score updates"},{"text":"Tennis","subCat":[],"active":false,"theme":"Content","label":"I want Live Tennis score updates"},{"text":"Hockey","subCat":[],"active":false,"theme":"Content","label":"I want Live Hockey score updates"},{"text":"Others","subCat":[],"active":false,"theme":"Content","label":"I want Live updates for other sports"}],"text":"Live Sports scores","active":false},{"text":"Animated Stickers","subCat":[],"active":true,"theme":"ExpressYourself","label":"Animated Sticker"},{"text":"Hike Home Screen","subCat":[{"text":"I want separate tabs for Calls/Group and one-on-one chats","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Tabs like Whatsapp"},{"text":"I want a gesture based swipe to switch between screens","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Gestures"}],"active":false},{"text":"New features for Conversation screen","subCat":[{"text":"Pinning important chats on top","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Pinning important chats on top"},{"text":"Multi select to clear/delete chats","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Multi select to clear/delete chats"},{"text":"An option to archive chats","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"An option to archive chats"},{"text":"An option to mark chat as unread","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"An option to mark chat as unread"},{"text":"On new compose I want to choose between broadcast/groups with a long press","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"On new compose I want to choose between broadcast/groups with a long press like Whatsapp"}],"active":false},{"text":"Custom LED colour for notifications","subCat":[{"text":"I want to set different LED colour for Group and one-one-chat","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"different LED colour for Group and one-one-chat"},{"text":"I want to set different LED colours per chat","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Different LED colours per chat"}],"active":false},{"text":"Additional settings on hike home screen","subCat":[{"text":"Network usage info","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Network usage info"},{"text":"Hide the message content in notification bar","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Hide the message content in notification bar"},{"text":"Custom ringtone for hike Voice Calls","subCat":[],"active":false,"label":"Custom ringtone for hike Voice Calls","theme":"VOIP&Video"},{"text":"Mute a one-on-one chat","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Mute a one-on-one chat"}],"active":false},{"text":"Home Screen Theme","subCat":[{"text":"I want to apply themes to my hike home screen","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Home Screen Theme"},{"text":"I want to customise the colour of the hike logo","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Home Screen Theme"},{"text":"Others","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Home Screen Theme"}],"active":false},{"text":"Horoscope","subCat":[],"active":false,"theme":"Content","label":"Horoscope"},{"subCat":[],"text":"Others","active":false,"label":"Others"}],"text":"Suggest a new feature"},{"active":false,"subCat":[{"text":"Friends/Favorites","subCat":[{"text":"I don't want to add a contact as friend to start a chat","subCat":[],"active":true,"label":"Friends/Favorites","theme":"HomeScreen5.0"}],"active":false,"label":"Friends/Favorites","theme":"HomeScreen5.0"},{"active":false,"subCat":[{"subCat":[{"text":"Tamil","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Tamil"},{"text":"Marathi","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Marathi"},{"text":"Kannada","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Kannada"},{"text":"Telugu","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Telugu"},{"text":"Gujarati","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Gujarati"},{"text":"Malayalam","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Malayalam"},{"text":"Urdu","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Urdu"},{"text":"Bengali","subCat":[],"active":false,"theme":"Content","label":"I want to get news in Bengali"},{"text":"Others","subCat":[],"active":true,"label":"Add more languages","theme":"Content"}],"text":"Add more Languages","active":true},{"text":"Add more categories","subCat":[{"text":"Employment/Jobs","subCat":[],"active":false,"theme":"Content","label":"I want a separate category for Employment/Jobs related news"},{"text":"Weather Forecast","subCat":[],"active":false,"theme":"Content","label":"I want a separate category for Weather updates"},{"text":"BSE/NSE","subCat":[],"active":false,"theme":"Content","label":"BSE/NSE News"},{"text":"Automobiles","subCat":[],"active":false,"theme":"Content","label":"I want a separate category for Automobiles related news"},{"text":"Start-Ups","subCat":[],"active":false,"theme":"Content","label":"I want a separate category for Start-Ups related news"},{"text":"Others","subCat":[],"active":true,"theme":"Content","label":"Other categories requests in news"}],"active":false},{"text":"Bookmark News articles","subCat":[],"active":false,"label":"Bookmark News articles","theme":"Content"},{"text":"Customized news categories","subCat":[],"active":false,"theme":"Content","label":"Customized news categories"},{"text":"Add refresh button in News","subCat":[],"active":false,"theme":"Content","label":"Add refresh button in News"},{"text":"Add News widget","subCat":[],"active":false,"theme":"Content","label":"Add News widget"},{"text":"Option to mark News as Read/Unread","subCat":[],"active":false,"theme":"Content","label":"Mark News Read Unread"},{"text":"Add 'Go to top' button","subCat":[],"active":false,"theme":"Content","label":"Go to Top Button"},{"text":"RSS Feed","subCat":[],"active":false,"theme":"Content","label":"RSS Feed for News"},{"text":"Add Search option in News","subCat":[],"active":false,"label":"Search for News","theme":"Content"},{"text":"Increase font Size","subCat":[],"active":false,"label":"Increase News Font Size","theme":"Content"},{"text":"I want to share news on other platforms like facebook, email etc.","subCat":[],"active":false,"theme":"Content","label":"Share News across Platforms"},{"text":"I want an option to save news articles to be able to read offline","subCat":[],"active":false,"theme":"Content","label":"Save News to read offline"},{"text":"Add a button to like news articles","subCat":[],"active":false,"theme":"Content","label":"Add a button to like news articles"},{"text":"I want Night mode in News","subCat":[],"active":false,"theme":"Content","label":"Night Mode in News"},{"text":"I want to decide when I want news notifications","subCat":[],"active":false,"theme":"Content","label":"I want to decide when I want news notifications"},{"subCat":[],"text":"Others","active":false,"theme":"Content","label":"Other News Enhancements"}],"text":"News"},{"subCat":[{"text":"I want to use my own Image from the gallery","subCat":[],"active":false,"theme":"ChatExperience","label":"Custom Chat Theme"},{"text":"I want more Chat Themes","subCat":[{"text":"I want Custom Chat Themes","subCat":[],"active":true,"theme":"ChatExperience","label":"Custom Chat Theme"},{"text":"I want hike Themes","subCat":[{"text":"More Love Themes","subCat":[],"active":false,"theme":"ChatExperience","label":"More Chat Themes"},{"text":"Night / Darker Themes","subCat":[],"active":false,"label":"More Chat Themes","theme":"ChatExperience"},{"text":"Family Themes","subCat":[],"active":false,"label":"More Chat Themes","theme":"ChatExperience"},{"text":"Official themes / Professional Themes","subCat":[],"active":true,"label":"More Chat Themes","theme":"ChatExperience"},{"text":"Others","subCat":[],"active":false,"label":"More Chat Themes","theme":"ChatExperience"}],"active":false}],"active":true,"label":"More Chat Themes","theme":"ChatExperience"},{"text":"Use a different symbol for nudges","subCat":[],"active":false,"theme":"ChatExperience","label":"Custom Nudges"},{"subCat":[],"text":"Others","active":false,"label":"Other Chat Themes Enchacements","theme":"ChatExperience"}],"text":"Chat Themes","active":false},{"subCat":[{"subCat":[],"text":"Custom Stickers","active":false,"label":"Custom stickers","theme":"ExpressYourself"},{"subCat":[{"text":"Please add categories / Tabs in sticker Shop","subCat":[],"active":false,"theme":"ExpressYourself","label":"Please add cateogries in Sticker Shop"},{"text":"Preview a Sticker Pack before downloading","subCat":[],"active":true,"theme":"ExpressYourself","label":"Preview a Sticker Pack before downloading"},{"text":""}],"text":"Sticker Shop","active":false},{"subCat":[],"text":"Preview sticker before sharing","active":false,"label":"Preview sticker","theme":"ExpressYourself"},{"text":"Reduce the size of Sticker","subCat":[],"active":false,"theme":"ExpressYourself","label":"Reduce the size of Sticker"},{"text":"I want to create assorted sticker pack of my favorite sticker","subCat":[],"active":false,"label":"I want to create assorted sticker pack of my favorite sticker","theme":"ExpressYourself"},{"text":"Clear Recent Stickers","subCat":[],"active":false,"label":"Clear Recent Stickers","theme":"ExpressYourself"},{"text":"I want to have different recent stickers for different contacts","subCat":[],"active":false,"theme":"ExpressYourself","label":"I want to have different recent stickers for different contacts"},{"text":"I want to know the sticker pack of a newly recieved sticker","subCat":[],"active":false,"theme":"ExpressYourself","label":"I want to know the sticker pack of a newly recieved sticker"},{"text":"I want stickers with sound","subCat":[],"active":false,"theme":"ExpressYourself","label":"I want stickers with sound"},{"text":"Recent stickers in each category should appear first","subCat":[],"active":false,"theme":"ExpressYourself","label":"Recent stickers in each category should appear first"},{"text":"I can't find the recently downloaded sticker pack in my sticker pallette easily","subCat":[],"active":false,"theme":"ExpressYourself","label":"I can't find the recently downloaded sticker pack in my sticker pallette easily"},{"text":"On clicking the sticker button I should see the last used sticker pack instead of recents.","subCat":[],"active":false,"theme":"ExpressYourself","label":"On clicking the sticker button I should see the last used sticker pack instead of recents."},{"text":"Others","subCat":[],"active":false,"theme":"ExpressYourself","label":"Other Sticker Enhancements"}],"text":"Stickers","active":true},{"text":"Match Up!","subCat":[{"text":"I should be able to see who has liked my profile","subCat":[],"active":true,"theme":"Growth","label":"I should be able to see who has liked my profile"},{"text":"I should be able to match with more than 10 people","subCat":[],"active":false,"label":"Increase the limit of matches","theme":"Growth"},{"text":"Match Up suggestions should be on the basis of location","subCat":[],"active":false,"theme":"Growth","label":"Location Based Match Up Suggestion"},{"text":"Others","subCat":[],"active":false,"theme":"Growth","label":"Other Match Up related requests"}],"active":false},{"active":false,"subCat":[{"text":"I want to use Hike direct for GC","subCat":[],"active":false,"theme":"ChatExperience","label":"Hike Direct for GC"},{"subCat":[],"text":"Hike direct radius should be more than 100 meters","active":true,"label":"Hike Direct Enchancement","theme":"ChatExperience"},{"text":"Download the Hike Direct files directly to SD Card.","subCat":[],"active":false,"label":"Hike Direct Enchancement","theme":"ChatExperience"},{"text":"hike direct not working.","subCat":[],"active":false,"theme":"ChatExperience","label":"Hike Direct not working","type":"Bug"},{"subCat":[],"text":"Others","active":false,"label":"Hike Direct Enchancement","theme":"ChatExperience"}],"text":"Hike Direct"},{"active":false,"subCat":[{"subCat":[],"text":"I want to share stickers in Status update","active":false,"label":"Status Update","theme":"HomeScreen5.0"},{"subCat":[],"text":"Please add more moods for Status updates","active":false,"label":"Status Update","theme":"HomeScreen5.0"},{"subCat":[],"text":"Copy pasting my friends Status updates","active":false,"label":"Status Update","theme":"HomeScreen5.0"},{"subCat":[],"text":"Sharing hike daily in Status update","active":true,"label":"Status Update","theme":"HomeScreen5.0"}],"text":"Status Updates"},{"text":"Just For Laughs","subCat":[],"active":false,"theme":"Content","label":"JFL Enhancement requests"},{"subCat":[{"subCat":[],"text":"Please add a feature to search for messages between particular dates.","active":false,"label":"Search Between Dates","theme":"ChatExperience"},{"subCat":[],"text":"I want a universal search button to search keywords on hike conversation list screen.","active":false,"label":"Universal Search","theme":"HomeScreen5.0"}],"text":"Search","active":false},{"active":false,"subCat":[{"subCat":[],"text":"Please add a comment feature for timeline posts","active":false,"label":"Comment on Timeline","theme":"HomeScreen5.0"},{"subCat":[],"text":"I don't want the like feature","active":false,"label":"Don't want likes Timeline","theme":"HomeScreen5.0"},{"subCat":[],"text":"Others","active":false,"label":"Other Timeline Enchancements","theme":"HomeScreen5.0"}],"text":"Timeline"},{"subCat":[{"text":"I want to recover my hidden chats forgotten password without deleting my chats","subCat":[],"active":true,"label":"Hidden mode","theme":"HomeScreen5.0"},{"subCat":[],"text":"Hide the timeline posts for contacts that are hidden","active":false,"label":"Hidden Mode Enhancements","theme":"HomeScreen5.0"},{"subCat":[],"text":"Sharing media from gallery directly to a hidden chat","active":false,"label":"Hidden Mode Enhancements","theme":"HomeScreen5.0"},{"subCat":[],"text":"Hiding the hidden chat notification","active":false,"label":"Hidden Mode Enhancements","theme":"HomeScreen5.0"},{"subCat":[],"text":"Hiding media shared in a hidden chat","active":false,"label":"Hidden Mode Enhancements","theme":"HomeScreen5.0"},{"subCat":[],"text":"Others","active":false,"label":"Hidden Mode Enhancements","theme":"HomeScreen5.0"}],"text":"Hidden Mode","active":false},{"subCat":[{"subCat":[],"text":"I want Google Drive Backup","active":false,"label":"Backup","theme":"HomeScreen5.0"},{"subCat":[],"text":"Others","active":false,"label":"Backup","theme":"HomeScreen5.0"}],"text":"Message Backup","active":false},{"subCat":[{"subCat":[],"text":"Image crop option","active":false,"label":"Image Crop","theme":"ChatExperience"},{"subCat":[],"text":"Adding a caption to an image","active":true,"label":"File Transfer","theme":"ChatExperience"},{"subCat":[],"text":"Rotating an image while sharing","active":false,"theme":"ChatExperience","label":"Rotate Image"},{"text":"I want to share my installed Apps as APKs via hike","subCat":[],"active":false,"theme":"ChatExperience","label":"I want to share my installed Apps as APKs via hike"},{"text":"Voice Messages/Walkie-Talkie","subCat":[{"text":"I'm unable to send Voice Messages","subCat":[],"active":false,"theme":"ChatExperience","label":"I'm unable to send Voice Messages","type":"Bug"},{"text":"My Voice Message volume is too low","subCat":[],"active":false,"label":"Walkie Talkie Issue","theme":"ChatExperience","type":"Bug"}],"active":false},{"subCat":[],"text":"Others","active":false,"theme":"ChatExperience","label":"Other File Sharing Enchancements"}],"text":"File Sharing","active":false},{"subCat":[{"subCat":[],"text":"Disable Nudges in Group Chats","active":true,"label":"Disable Nudges in Group","theme":"ChatExperience"},{"text":"How to leave group?","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/41498480-How-do-I-exit-a-group-chat-","theme":"ChatExperience"},{"text":"I want to hide the mobile number of my Group Chat Members from others.","subCat":[],"active":false,"theme":"ChatExperience","label":"Hide the mobile number of my Group Chat Members"}],"text":"Group Chats","active":false},{"subCat":[],"text":"Profile Photo","active":false,"label":"Profile Photo","theme":"HomeScreen5.0"},{"subCat":[{"text":"I want 3 Tab design","subCat":[],"active":true,"label":"Home Screen design","theme":"HomeScreen5.0"},{"subCat":[],"text":"Others","active":false,"label":"Home Screen design","theme":"HomeScreen5.0"}],"text":"hike app design feedback","active":false},{"subCat":[],"text":"Broadcast","active":false,"label":"Broadcast","theme":"HomeScreen5.0"},{"subCat":[{"text":"I want hike daily quotes from Indians","subCat":[],"active":false,"theme":"Content","label":"I want hike daily quotes from Indians"},{"text":"I want hike daily in Hindi","subCat":[],"active":false,"theme":"Content","label":"I want hike daily in Hindi"},{"text":"Send more quotes","subCat":[],"active":true,"theme":"Content","label":"I want to get more quotes daily"}],"text":"Hike Daily","active":false},{"subCat":[],"text":"Cricket","active":false,"label":"Cricket","theme":"Content"},{"subCat":[{"text":"Add more coupons","subCat":[],"active":true,"theme":"Content","label":"Add more coupons"}],"text":"Coupons","active":false},{"text":"Profile Enhancement","subCat":[{"text":"Save profile picture of friends while visiting their profile","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Save profile picture of friends while visiting their profile"},{"text":"Profile picture privacy similar to last seen privacy(sharing with everyone, favourites, my contacts and none)","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Profile picture privacy similar to last seen privacy(sharing with everyone, favourites, my contacts and none)"},{"text":"More photo filters","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"More photo filters"}],"active":false},{"text":"hike caller","subCat":[{"text":"Add the option to \"Mark Spam and block\"","subCat":[],"active":false,"theme":"Growth","label":"Add the option to \"Mark Spam and block\" in hike Caller"},{"text":"Update my Hike Caller Details","subCat":[],"active":false,"label":"Update my Hike Caller Details","theme":"Growth"}],"active":false},{"text":"Natasha","subCat":[{"text":"Add more languages","subCat":[],"active":false,"theme":"Growth","label":"Add more languages for Natasha"},{"text":"Voice response from Natasha like Siri, Google voice etc.","subCat":[],"active":false,"theme":"Growth","label":"Voice response from Natasha like Siri etc."}],"active":false},{"text":"Block Contact","subCat":[{"text":"I want to unblock a contact","subCat":[],"active":false,"theme":"ChatExperience","type":"weburl","url":"http://support.hike.in/entries/21599545-Annoyed-by-someone-Block-Unblock-the-contact"},{"text":"Block contact for a Particular time like 1 hour or 1 day","subCat":[],"active":false,"type":"Feature","theme":"ChatExperience","label":"Block contact for a Particular time like 1 hour or 1 day"},{"text":"Others","subCat":[],"active":false,"theme":"ChatExperience","label":"Other Block Contact Enhacement"}],"active":false},{"text":"Last Seen","subCat":[{"text":"How to turn OFF last seen","subCat":[],"active":false,"theme":"ChatExperience","type":"weburl","url":"http://support.hike.in/entries/93467388-How-do-I-configure-Privacy-Settings-for-Last-Seen-"},{"text":"I'm not able to see last seen","subCat":[],"active":true,"theme":"ChatExperience","label":"Last seen"}],"active":false},{"text":"Nudge","subCat":[{"text":"Remove Nudge","subCat":[],"active":false,"theme":"ChatExperience","label":"Remove Nudge"},{"text":"I'm unable to nudge","subCat":[],"active":true,"theme":"ChatExperience","label":"I'm unable to nudge"}],"active":false},{"subCat":[],"text":"Others","active":false,"label":"Other Feature Enhacements"}],"text":"Improve an existing feature"},{"active":false,"subCat":[],"text":"Suggest a new Sticker","label":"New Sticker Suggestion","theme":"ExpressYourself"},{"text":"Share a joke with us","subCat":[],"active":false,"theme":"Content","label":"JFL"}],"icon":"feature","type":"Feature","text":"Suggest Something New"},{"active":true,"subCat":[{"active":true,"subCat":[{"subCat":[{"text":"Slowness from changing from ✓ to ✓✓","subCat":[{"text":"Slow with ALL my friends","subCat":[],"active":true,"theme":"CPR","label":"Messaging latency","logkey":"mqtt"},{"text":"Slow with 1-2 friends in Specific","subCat":[{"text":"There is a very high chance that if your friend is on certain Android devices (Asus, Xiaomi, Lenovo, Huawei etc) that s/he will need to do something via the respective FAQ that we will share with you and then the problem should solve itself.","subCat":[],"active":false,"nextScreen":"0"},{"text":"My Friend is using a Xiaomi Phone","subCat":[{"text":"With MiUI7","subCat":[],"active":false,"url":"http://support.hike.in/entries/98277657-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-7-","type":"weburl"},{"text":"With MiUI6","subCat":[],"active":false,"url":"http://support.hike.in/entries/55998480-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-6-","type":"weburl"},{"text":"With MiUI5","subCat":[],"active":false,"url":"http://support.hike.in/entries/95306838-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-5-","type":"weburl"},{"text":"I don't know how to check the MiUI Version","subCat":[],"active":true,"type":"weburl","url":"http://support.hike.in/entries/98675887-How-do-I-know-my-MiUI-version-"}],"active":false},{"text":"My Friend is using a Lenovo Phone","subCat":[],"active":false,"theme":"CPR","label":"Messaging latency"},{"text":"My Friend is using a Huawei Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/94785897-Enable-notifications-for-Huawei-Phones"},{"text":"MY Friend is using an Asus Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/95935217-Enable-Notification-for-Asus"},{"text":"My Friend is using a Samsung Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/98608597-Not-getting-notification-on-my-Samsung-Phone-S6-Galaxy-S6-Edge-Plus-Note-Edge-etc-"},{"text":"Others","subCat":[],"active":false,"label":"Message slow from tick to double tick","theme":"CPR"}],"active":false,"logkey":"mqtt"}],"active":true},{"subCat":[{"subCat":[{"subCat":[],"text":"Please tell us which WiFi network are you using on the next screen.","active":true,"theme":"CPR","label":"Messaging latency"}],"text":"My messages are stuck on Clock while I'm on WiFi.","active":false},{"subCat":[{"subCat":[],"text":"Please tell us which network operator services are you using on the next screen.","active":true,"label":"Message Latency","theme":"CPR"}],"text":"My messages are stuck on Clock while I'm on Mobile Data (2G/3G/4G).","active":false},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-","subCat":[],"text":"Have you signed up on a different device using the same number? You can restore data from one device, reset the account and sign up on the device you wish to.","active":false},{"subCat":[],"text":"I'm not signed up on two devices simultaneously.","active":true,"label":"Connectivity Issue","theme":"CPR"}],"text":"My messages are stuck on Clock on both WiFi and Mobile Data (2G/3G/4G).","active":true}],"text":"My messages are stuck on clock","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please tell us which WiFi network are you using on the next screen.","active":true,"theme":"CPR","label":"Connectivity"}],"text":"My messages are stuck on single tick (✓) while I'm on WiFi","active":true},{"subCat":[{"subCat":[],"text":"Please tell us which network operator services are you using on the next screen.","active":true,"theme":"CPR","label":"Connectivity"}],"text":"My messages are stuck on single tick (✓) while I'm on Mobile Data (2G/3G/4G).","active":false},{"subCat":[],"text":"If the above options didn't help, please enter the mobile number of the contact with whom you are facing this issue on the next screen.","active":false,"label":"Connectivity","theme":"CPR"}],"text":"My messages are stuck on single tick (✓)","active":false},{"subCat":[{"subCat":[],"text":"My messages are stuck on double tick (✓✓) with my contacts who are on iOS.","active":false,"label":"iOS double tick hack","theme":"CPR"},{"subCat":[{"subCat":[],"text":"Please share some contact details with whom you are facing this issue on the next screen.","active":true,"label":"Double tick but not delivered","theme":"CPR"}],"text":"My messages are stuck on double tick (✓✓) with all my contacts.","active":true}],"text":"My messages are stuck on double tick (✓✓) but not delivered.","active":false},{"active":false,"subCat":[],"text":"Slowness from changing from Clock to ✓","theme":"CPR","label":"Message Latency"},{"text":"Slowness from changing from ✓✓ to ✓✓R","subCat":[],"active":false,"theme":"CPR","label":"Message Latency"}],"text":"Sending messages to friends on hike","active":true},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the mobile number of the contact with whom you are facing this issue on the next screen.","active":true,"theme":"CPR","label":"Free SMS not delivered","logkey":"mqtt"}],"text":"My messages are not delivered to friends who are not on hike.","active":true},{"subCat":[{"subCat":[],"text":"Please tell us which WiFi network are you using on the next screen.","active":true,"theme":"CPR","label":"Connectivity","logkey":"mqtt"}],"text":"My messages are stuck in Clock state and I'm facing this on WiFi.","active":false},{"subCat":[{"subCat":[],"text":"Please tell us which network operator services are you using on the next screen.","active":true,"theme":"CPR","label":"Connectivity","logkey":"mqtt"}],"text":"My messages are stuck in Clock state and I'm facing this on mobile data(2G/3G/4G).","active":false},{"type":"weburl","url":"http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-","subCat":[],"text":"Have you signed up with the same number on a different device? You can restore data from one device, reset the account and sign up on the device you wish to.","active":false}],"text":"Sending messages to friends not on hike","active":false},{"subCat":[{"subCat":[],"text":"The pop up for sending an offline message appears very late.","active":false,"label":"H2O Pop up is very Late","theme":"ChatExperience","type":"Feature"},{"subCat":[],"text":"I would like an option to send the offline message as an SMS directly instead of waiting for the popup to appear.","active":false,"label":"Messaging latency","theme":"CPR"}],"text":"Sending messages to friends on hike who are offline","active":false},{"text":"I'm seeing 'No internet Connection' But my internet is working fine.","subCat":[{"text":"I'm facing this issue only on WiFi","subCat":[{"text":"Please on test.hike.in and ensure that \"Everything is working fine\"","subCat":[],"active":false,"nextScreen":"0","logkey":"mqtt"},{"text":"test.hike.in","subCat":[],"active":false,"type":"weburl","url":"http://test.hike.in","logkey":"mqtt"},{"text":"I've checked test.hike.in and Everything is working fine.","subCat":[{"text":"I'm using a Home WiFi","subCat":[{"text":"Please share your WiFi Provider details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":false},{"text":"I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)","subCat":[{"text":"Please share your WiFi Provider details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":true}],"active":false,"theme":"CPR","label":"Connectivity","logkey":"mqtt"},{"text":"I'm seeing an error in test.hike.in","subCat":[{"text":"I'm using a Home WiFi","subCat":[{"text":"Please share a screenshot of the error and your WiFi Provider details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":false},{"text":"I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)","subCat":[{"text":"Please share a screenshot of the error and your WiFi Provider details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":true}],"active":true}],"active":false},{"text":"I'm facing this issue only on Mobile Data","subCat":[{"text":"Please on test.hike.in and ensure that \"Everything is working fine\"","subCat":[],"active":false,"nextScreen":"0","logkey":"mqtt"},{"text":"http://test.hike.in","subCat":[],"active":false,"url":"http://test.hike.in","type":"weburl","logkey":"mqtt"},{"text":"I've checked test.hike.in and Everything is working fine.","subCat":[{"text":"Please share your Service Provide/Operator details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":false},{"text":"I'm seeing an error in test.hike.in","subCat":[{"text":"Please share a screenshot of the error and your Service Provider/Operator details on the next screen","subCat":[],"active":true,"theme":"CPR","label":"Connectivity","logkey":"mqtt"}],"active":true}],"active":false},{"text":"I'm seeing this on Mobile Mobile Data and WiFi","subCat":[{"text":"Please on test.hike.in and ensure that \"Everything is working fine\"","subCat":[],"active":false,"nextScreen":"0","logkey":"mqtt"},{"text":"http://test.hike.in","subCat":[],"active":false,"type":"weburl","url":"http://test.hike.in","logkey":"mqtt"},{"text":"I've checked test.hike.in and Everything is working fine.","subCat":[{"text":"Please share your Service Provide/Operator details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":false},{"text":"I'm seeing an error in test.hike.in","subCat":[{"text":"Please share a screenshot of the error and your Service Provider/Operator details on the next screen","subCat":[],"active":true,"label":"Connectivity","theme":"CPR","logkey":"mqtt"}],"active":true}],"active":true,"nextScreen":"0"}],"active":false},{"text":"Opening a chat in hike is slow","subCat":[{"text":"I'm facing this with all my chats","subCat":[],"active":false,"label":"Slow CT Loading","theme":"ChatExperience","logkey":"mqtt"},{"text":"I'm facing this with one or two chats only","subCat":[],"active":true,"label":"Slow CT Loading","theme":"ChatExperience","logkey":"mqtt"}],"active":false}],"text":"Message delivery is Slow / Not working"},{"active":false,"subCat":[{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending both media and non-media documents is slow","active":true}],"text":"I'm sending from a Wi-Fi connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending both media and non-media documents is slow","active":true}],"text":"I'm sending from my 2G mobile connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Sending both media and non-media documents is slow","active":true}],"text":"I'm sending from my 3G mobile connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Sending media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Sending both media and non-media documents is slow","active":true}],"text":"I'm sending from my 4G mobile connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Sending both media and non-media documents is slow.","active":true}],"text":"Sending is slow on both Wi-Fi and Mobile Data (2G/3G/4G)","active":true}],"text":"Sending anything is very slow and takes a long time to upload.","active":false},{"active":false,"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading media (photos & videos) is slow.","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading both media and non-media documents is slow","active":true}],"text":"I'm downloading from a Wi-Fi connection","active":false},{"active":false,"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading both media and non-media documents is slow","active":true}],"text":"I'm downloading from my 2G mobile connection","theme":"CPR","label":"File Transfer"},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading both media and non-media documents is slow","active":true}],"text":"I'm downloading from my 3G mobile connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading media (photos & videos) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"type":"Bug","theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Downloading both media and non-media documents is slow","active":true,"label":"File Transfer"}],"text":"I'm downloading from my 4G mobile connection","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading media (photos & videos) is slow.","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","logkey":"ft"}],"text":"Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow.","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Downloading both media and non-media documents is slow.","active":true}],"text":"Downloading is slow on both Wi-Fi and Mobile Data (2G/3G/4G)","active":true}],"text":"Downloading anything is very slow and takes a long time to download."},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"The error message when uploading is 'Could not upload the file. Check Network Settings'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"The error message when uploading is 'Unable to read file'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"The error message when uploading is 'No app found that can handle this action'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"The error message when uploading is 'Unsupported file'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"The error message when uploading is 'Max file size can be 100 MB'","active":false},{"subCat":[{"subCat":[],"text":"Please share a screenshot of the error message on the next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"Another error message comes which is not shown above appears.","active":true}],"text":"I'm getting an error when uploading.","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"The error message when downloading is 'Could not download the file. Check Network Settings'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"The error message when downloading is 'This file no longer exists'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"The error message when downloading is 'Your phone does not have enough space to download the file'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"theme":"CPR","label":"File Transfer","logkey":"ft"}],"text":"The error message when downloading is 'Please insert an SD Card'","active":false},{"subCat":[{"subCat":[],"text":"Please share the screenshot and other details on next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"The error message when uploading is 'Max file size can be 100 MB'","active":false},{"subCat":[{"subCat":[],"text":"Please share a screenshot of the error message on the next screen.","active":true,"label":"File Transfer","theme":"CPR","logkey":"ft"}],"text":"Another error message comes which is not shown above appears.","active":true}],"text":"I'm getting an error when downloading.","active":false},{"subCat":[],"text":"Takes a lot of time to open the gallery after clicking it to choose a photo to upload.","active":false,"label":"Taking time to open gallery","theme":"ChatExperience","logkey":"ft"},{"subCat":[{"subCat":[{"subCat":[],"text":"I choose 'Compressed' option","active":false,"label":"Image shared is pixelated","theme":"ChatExperience"},{"subCat":[],"text":"I choose 'Normal' option","active":false,"label":"Image shared is pixelated","theme":"ChatExperience"},{"subCat":[],"text":"I choose 'Original' option","active":true,"label":"Image shared is pixelated","theme":"ChatExperience"}],"text":"I shared the photo from inside chat thread > Attachments > Gallery","active":false},{"subCat":[{"subCat":[],"text":"I choose 'Compressed' option","active":false,"label":"Image shared is pixelated","theme":"ChatExperience"},{"subCat":[],"text":"I choose 'Normal' option","active":false,"label":"Image shared is pixelated","theme":"ChatExperience"},{"subCat":[],"text":"I choose 'Original' option","active":true,"label":"Image shared is pixelated","theme":"ChatExperience"}],"text":"I shared the photo from inside chat thread > Attachments > Camera","active":false},{"subCat":[],"text":"I shared the photo from outside hike > Phone photo gallery > Share via hike","active":false,"label":"Image shared is pixelated","theme":"CPR"},{"subCat":[],"text":"The photos I receive from my friend get black or colour stripped out after I download it.","active":false,"label":"Image quality issue","theme":"CPR"}],"text":"The quality of the photo is poor and pixelated after I send it to my friend.","active":false},{"text":"Others","subCat":[],"active":false,"theme":"CPR","label":"File Transfer","logkey":"ft"},{"text":"When I exit the app the file transfer stops in the background","subCat":[],"active":false,"label":"When I exit the app the file transfer stops in the background","theme":"CPR","logkey":"ft"},{"text":"Not able to download PDF","subCat":[{"text":"I am not able to download any PDF received on hike","subCat":[],"active":false,"label":"I am not able to download any PDF received on hike","theme":"CPR","logkey":"ft"}],"active":true}],"text":"File Transfer Issues"},{"subCat":[{"subCat":[{"text":"I'm using a Samsung J5/J7 or similar device","subCat":[],"active":false,"url":"http://support.hike.in/entries/100776847-Not-getting-Notification-on-my-Samsung-J5-J7-similar-series-device","type":"weburl"},{"text":"I'm using a Samsung S6, Galaxy S6 Edge Plus, Note Edge","subCat":[],"active":false,"url":"http://support.hike.in/entries/98608597-Not-getting-notification-on-my-Samsung-Phone-S6-Galaxy-S6-Edge-Plus-Note-Edge-etc-","type":"weburl"},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/98277657-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-7-","subCat":[],"text":"I'm using hike on Xiaomi MiUI 7","active":false},{"type":"weburl","url":"http://support.hike.in/entries/95306838-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-5-","subCat":[],"text":"I'm using hike on Xiaomi MiUi 5.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/55998480-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-6-","subCat":[],"text":"I'm using hike on Xiaomi MiUi 6.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/98675887-How-do-I-know-my-MiUI-version-","subCat":[],"text":"I don't know my MiUI Version","active":false}],"text":"I'm using a Xiaomi Phone","active":false},{"text":"I'm using a Asus Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/95935217-Enable-Notification-for-Asus"},{"text":"I'm using an Oppo device.","subCat":[],"active":false,"url":"http://support.hike.in/entries/101337607-No-Notification-on-Oppo-device","type":"weburl"},{"text":"I'm using a Vivo Phone","subCat":[],"active":true,"url":"http://support.hike.in/entries/101346768-No-Notification-on-Vivo-device","type":"weburl"},{"text":"I'm using a Lenovo Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/98036948-Enable-Notification-For-Lenovo-Phones"},{"text":"I'm using a Meizu Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/95428747-Enable-notifications-for-MEIZU-Phones"},{"text":"I'm using a Huawei Phone","subCat":[],"active":false,"type":"weburl","url":"http://support.hike.in/entries/94785897-Enable-notifications-for-Huawei-Phones"},{"subCat":[],"text":"I have given necessary permissions to hike from phone settings and still not getting any notification for an incoming message.","active":false,"theme":"CPR","label":"Push notification"},{"subCat":[{"subCat":[],"text":"If you have installed a Custom ROM, please share the OS version details on the next screen.","active":true,"label":"Notification Issue","theme":"CPR"}],"text":"I'm using a rooted device.","active":false},{"subCat":[{"subCat":[],"text":"Please share the details of the app on the next screen.","active":true,"theme":"CPR","label":"Push notification"}],"text":"I'm using Clean Master or similar apps on my mobile phone","active":false}],"text":"I'm not getting any notification for a new message until I open the app.","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Are you listening to FM radio?","active":false,"theme":"ChatExperience","label":"Notification Sound"},{"subCat":[{"subCat":[],"text":"Please share music player details on the next screen.","active":true,"theme":"CPR","label":"Sound notification"}],"text":"Are you listening to a music player?","active":false}],"text":"I don't get any sound notification for an incoming message while listening to music.","active":false},{"subCat":[],"text":"On receiving any new hike message the music playing in the background stops.","active":false,"theme":"CPR","label":"Music Player Issue"},{"subCat":[],"text":"Phone goes into Silent mode after using hike for a while. I have to force kill the app to resume to its default sound settings","active":false,"theme":"CPR","label":"Phone switched to silent mode"},{"subCat":[{"subCat":[],"text":"It's the conversation tone.","active":false,"theme":"CPR","label":"Continuous notification"},{"subCat":[],"text":"It's the sound notification for the new message.","active":true,"theme":"CPR","label":"Continuous notification"}],"text":"There is a continuous sound playing in the background on sending a message.","active":false},{"subCat":[],"text":"The phone doesn't vibrate on receiving a new hike message while Vibration settings are turned ON.","active":false,"theme":"CPR","label":"Sound notification"},{"text":"Others","subCat":[],"active":false,"theme":"CPR","label":"Sound notification"}],"text":"I'm facing issues with Notification Sounds","active":false},{"text":"I don’t want my friends to be notified when I re-install the app saying “xyz has joined hike. Say hi”","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"I don't want RUJ"},{"text":"There are too many unnecessary notifications. Give us settings to disable some notifications","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"There are too many unnecessary notifications. Give us settings to disable some notifications"}],"text":"Notifications","active":false},{"text":"All my chats have suddenly disappeared","subCat":[{"text":"Go to device settings > apps > hike > force stop. Now open hike and let us know what happens","subCat":[{"text":"I got my chats back","subCat":[],"active":false,"label":"Chats disappearing","theme":"HomeScreen5.0"},{"text":"I'm still not not seeing my chats","subCat":[],"active":true,"theme":"CPR","label":"Chats disappearing"}],"active":false}],"active":false},{"text":"Favorites","subCat":[{"text":"Can’t see last seen status of favorites","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Can’t see last seen status of favorites"},{"text":"Can’t see profile picture of favorites","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"Can’t see profile picture of favorites"},{"text":"I don't want to add my contact as a \"Friend\" to start a chat","subCat":[],"active":false,"label":"Friends/Favorites","theme":"HomeScreen5.0"},{"text":"My posts are visible to favourites on their timeline but not visible to me","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Favourites"}],"active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share a screenshot with us on the next screen.","active":true,"theme":"HomeScreen5.0","label":"Profile photo"}],"text":"I'm facing an issue while uploading the profile photo.It goes till 75% but never completes.","active":false},{"subCat":[{"subCat":[],"text":"Please share a screenshot on the next screen.","active":true,"theme":"HomeScreen5.0","label":"Profile photo"}],"text":"I'm not able to upload a profile photo while using hike on mobile data (2G/3G/4G)","active":false},{"subCat":[{"subCat":[],"text":"Please help us by sharing a screenshot on the next screen","active":true,"theme":"HomeScreen5.0","label":"Profile photo"}],"text":"I'm not able to upload a profile photo while using hike on WiFi.","active":false},{"subCat":[{"subCat":[],"text":"Please share screenshots of the original photo and the screenshot of the profile photo on hike.","active":true,"theme":"HomeScreen5.0","label":"Profile photo"}],"text":"The profile photo quality is bad.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/93478737-How-do-I-remove-My-Profile-Picture-","subCat":[],"text":"How do I remove my profile photo?","active":false,"label":"Remove DP","theme":"HomeScreen5.0"},{"type":"weburl","url":"http://support.hike.in/entries/95378668-How-do-I-delete-a-Profile-Picture-that-I-posted-earlier-","subCat":[],"text":"How do I delete profile photo posted earlier?","active":false,"label":"Delete earlier DP","theme":"HomeScreen5.0"},{"type":"weburl","url":"http://support.hike.in/entries/95378768-How-do-I-configure-Privacy-Settings-for-my-Profile-Picture-","subCat":[],"text":"How do I hide my profile photo from favorite contacts?","active":true,"theme":"HomeScreen5.0","label":"Hide DP"}],"text":"Profile Photo","active":false},{"subCat":[{"subCat":[{"text":"Are you trying to post a text/mood/image post?","subCat":[],"active":true,"label":"Status Update","theme":"HomeScreen5.0"}],"text":"I'm not able to post a status update.","active":true},{"subCat":[{"subCat":[],"text":"Please share a screenshot on the next screen.","active":true,"theme":"HomeScreen5.0","label":"Status Update"}],"text":"I'm not able to delete my status update. I'm getting an error message.","active":false}],"text":"Status Updates","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the original image and the screenshot of the photo update on the next screen.","active":true,"label":"Timeline","theme":"HomeScreen5.0"}],"text":"I'm seeing an unclear/blurred image for my photo updates on the Timeline.","active":false},{"subCat":[{"subCat":[],"text":"Please share the favorite contact details with us on the next screen.","active":false,"label":"Timeline","theme":"HomeScreen5.0"}],"text":"I'm unable to see the pictures posted by my favorites","active":true},{"subCat":[{"subCat":[],"text":"Please do a long press on the post and tap on the option to delete the post.","active":true,"label":"Timeline","theme":"HomeScreen5.0"}],"text":"I'm unable to delete any post from the timeline.","active":false},{"text":"I have deleted some posts on my timeline but they are still visible to my favourites","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"I have deleted some posts on my timeline but they are still visible to my favourites"}],"text":"Timeline","active":false},{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Still facing this error after restarting? Please share a screenshot on the next screen.","active":false,"label":"Sticker retry","theme":"ExpressYourself"}],"text":"Please restart your phone and then try to download stickers.","active":true,"theme":"ExpressYourself","label":"Sticker retry"}],"text":"I'm seeing 'Retry' option every time I download stickers.","active":false},{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Sticker recommendation still not working. Please share a screenshot of Sticker Settings (hike settings > chat settings > Sticker Settings) on the next screen.","active":true,"label":"Sticker recommendation","theme":"ExpressYourself"}],"text":"Turn off and then turn on your sticker recommendation in hike settings Or Force stop and restart hike.","active":false}],"text":"My sticker recommendation is not working.","active":true,"label":"Sticker recommendation","theme":"ExpressYourself"},{"subCat":[{"subCat":[],"text":"Please share the exact words for which you get wrong sticker suggestions and add a screenshot on the next screen.","active":false,"theme":"ExpressYourself","label":"Sticker Recommendation"}],"text":"I'm getting wrong sticker recommendations for some words.","active":false,"label":"Sticker recommendation","theme":"ExpressYourself"}],"text":"Sticker recommendation","active":true},{"subCat":[{"subCat":[],"text":"Please share a screenshot of the hike stickers you see in your phone gallery on the next screen.","active":true,"label":"Sticker in gallery","theme":"ExpressYourself"}],"text":"I'm seeing stickers in my phone gallery.","active":false},{"text":"Sticker loads slowly when I recieve them in a chat","subCat":[],"active":false,"theme":"ExpressYourself","label":"Sticker loads slowly when I recieve them in a chat"},{"text":"Stickers are consuming memory, I'm running out of Storage Space on my phone","subCat":[],"active":false,"theme":"ExpressYourself","label":"Stickers are consuming memory, I'm running out of Storage Space on my phone"},{"text":"While scrolling stickers are sent accidently","subCat":[],"active":false,"theme":"ExpressYourself","label":"While scrolling stickers are sent accidently"},{"text":"My downloaded stickers are deleted and I need to redownload them.","subCat":[],"active":false,"theme":"ExpressYourself","label":"My downloaded stickers are deleted and I need to redownload them."},{"text":"I want to report an offensive sticker","subCat":[],"active":false,"theme":"ExpressYourself","label":"I want to report an offensive sticker"},{"text":"Stickers are too big in Chat conversation","subCat":[],"active":false,"theme":"ExpressYourself","label":"Stickers are too big in Chat conversation"}],"text":"Stickers","active":false},{"text":"Match Up!","subCat":[{"text":"I don't have Match Up feature","subCat":[{"text":"Why I am not able to get the Match Up feature in my hike account","subCat":[{"text":"Match Up feature is only available for Android OS version 4.4 and above.","subCat":[],"active":true,"theme":"Growth","label":"Why I am not able to get the Match Up feature in my hike account"}],"active":true}],"active":false},{"text":"My number is being displayed in the Match Up profile","subCat":[{"text":"My number is being displayed to others on the match up profile","subCat":[],"active":true,"theme":"Growth","label":"My number is being displayed to others on the match up profile"}],"active":false,"theme":"Growth","label":"My number is being displayed in the Match Up profile"},{"text":"Not able to change my profile pic","subCat":[{"text":"I'm not able to change my profile picture on Match Up","subCat":[],"active":false,"theme":"Growth","label":"Unable to Change profile picture on Match Up"}],"active":false},{"text":"Notification","subCat":[{"text":"I get notifications saying I have a match but I don't see anything inside Match Up","subCat":[],"active":false,"theme":"Growth","label":"I get notifications saying I have a match but I don't see anything inside Match Up"},{"text":"I'm getting notifications from my match even after blocking the person","subCat":[],"active":true,"theme":"Growth","label":"I'm getting notifications from my match even after blocking the person"}],"active":false},{"text":"Match-up is not suggesting any profiles for match","subCat":[],"active":false,"theme":"Growth","label":"Match-up is not suggesting any profiles for match"},{"text":"Delete my Profile","subCat":[{"text":"I cannot find how to delete my profile from Match Up","subCat":[{"text":"To delete your match up account, locate \"Match Up!\" on the conversation screen, long press the chat and select \"Block and Delete\". You may also block it by tapping on \"Block\", available under the three dot menu inside match up.","subCat":[],"active":true,"theme":"Growth","label":"I cannot find how to delete my profile from Match Up"}],"active":true}],"active":false},{"text":"I'm getting notifications from match-up even after blocking it","subCat":[],"active":false,"theme":"Growth","label":"Block not working for match up"},{"text":"Others","subCat":[],"active":false,"theme":"Growth","label":"Other Match Up related Issues"}],"active":false},{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the details of the app.","active":false,"theme":"CPR","label":"Backup/restore"}],"text":"Have you installed any RAM killer or app optimizer like the Clean master on your mobile phone?","active":false,"label":"BackUp/Restore","theme":"HomeScreen5.0"},{"text":"I'm not using any RAM killer","subCat":[],"active":true,"label":"Backup/Restore","theme":"HomeScreen5.0"}],"text":"Not creating backup/ Getting an error: backup failed.","active":false},{"subCat":[{"text":"I'm getting an error 'Backup failed something went wrong'","subCat":[{"text":"Please confirm if you have installed Cleanmaster app on your device.","subCat":[],"active":false,"theme":"HomeScreen5.0","label":"Backup Failed"}],"active":true,"label":"Backup/Restore","theme":"HomeScreen5.0"}],"text":"I'm not able to restore my backup.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-","subCat":[],"text":"I want to change my phone device, how do I take a backup?","active":false,"label":"Backup/Restore","theme":"HomeScreen5.0"},{"subCat":[],"text":"I'm re-signing up to my hike account, but the backup restore option is not appearing.","active":false,"label":"Backup/restore","theme":"HomeScreen5.0"},{"type":"weburl","url":"http://support.hike.in/entries/88935677-How-do-I-restore-a-chat-backup-from-a-Backup-File-","subCat":[],"text":"How do I restore my backup file?","active":false,"label":"Backup/Restore","theme":"HomeScreen5.0"},{"subCat":[],"text":"I deleted my hike account. I'm creating a new one. I had a backup file saved with me, but it is not getting restored as the backup restore option doesn't appear during sign up.","active":false,"label":"Backup/restore","theme":"HomeScreen5.0"},{"subCat":[{"subCat":[],"text":"Please add a screenshot on the next screen.","active":true,"label":"BackUp/Restore","theme":"HomeScreen5.0"}],"text":"I cannot delete my backup.","active":true}],"text":"Backup and Restore","active":false},{"subCat":[{"text":"I'm unable to hide/unhide a chat","subCat":[{"text":"I'm unable to draw the pattern for accessing my hidden chats","subCat":[],"active":true,"label":"Hidden chat","theme":"HomeScreen5.0"},{"text":"I'm unable to enter the PIN for my hidden chats","subCat":[],"active":false,"label":"Hidden chat","theme":"HomeScreen5.0"},{"text":"Others","subCat":[],"active":false,"label":"Hidden chat","theme":"HomeScreen5.0"}],"active":false},{"type":"weburl","url":"http://support.hike.in/entries/40163340-How-do-I-unhide-a-Chat-","subCat":[],"text":"How do I unhide a hidden chat?","active":false,"label":"Hidden Chat","theme":"HomeScreen5.0"},{"subCat":[],"text":"I can't share any pictures from the gallery and/or forward a picture to hidden chat.","active":false,"theme":"HomeScreen5.0","label":"Hidden Chat"},{"type":"weburl","url":"http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-#hidden","subCat":[],"text":"I'm unable to find a hidden chat","active":true,"label":"Hidden Chat","theme":"HomeScreen5.0"},{"type":"weburl","url":"http://support.hike.in/entries/40164720-How-do-I-know-if-I-ve-gotten-a-message-on-a-Hidden-Chat-","subCat":[],"text":"I'm not getting notification for new messages in a hidden chat","active":false,"theme":"HomeScreen5.0","label":"Hidden chat"},{"type":"weburl","url":"http://support.hike.in/entries/43159734-How-do-I-change-my-password-","subCat":[],"text":"I forgot my hidden chat password. How do I enter into hidden mode?","active":false,"label":"Hidden chat","theme":"HomeScreen5.0"}],"text":"Hidden Chats","active":false},{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the phone device and OS version details for the caller and the receiver.","active":true,"theme":"VOIP&Video","label":"VoIP"}],"text":"I cannot hear the voice of my call partner clearly.","active":false},{"subCat":[],"text":"I'm hearing an echo.","active":false,"label":"VoIP","theme":"VOIP&Video"},{"subCat":[],"text":"I'm facing a lag in the conversation.","active":false,"label":"VoIP","theme":"VOIP&Video"},{"subCat":[],"text":"The volume is too low.","active":true,"label":"VoIP","theme":"VOIP&Video"}],"text":"I'm facing issues with hike to hike calls.","active":false},{"subCat":[{"subCat":[{"subCat":[{"subCat":[],"text":"Please share a brief description of the issue if the above tips don't help.","active":true,"label":"VoIP","theme":"VOIP&Video"}],"text":"Please note that conference call host needs to be on WiFi or 4G.","active":true}],"text":"Please ensure that you are using  hike latest version available on the Play Store.","active":true}],"text":"I'm not able to make hike Conference Calls with my friends.","active":false},{"text":"Quality of the calls are very poor","subCat":[{"text":"I'm facing this issue on 2G","subCat":[],"active":false,"theme":"VOIP&Video","label":"VoIP"},{"text":"I'm facing this issue on 3G","subCat":[],"active":false,"label":"VoIP","theme":"VOIP&Video"},{"text":"I'm facing this issue on WiFi","subCat":[],"active":true,"theme":"VOIP&Video","label":"VoIP"}],"active":false},{"text":"Hike calls are not getting connected","subCat":[{"text":"I'm facing this issue on 2G","subCat":[],"active":false,"theme":"VOIP&Video","label":"VoIP"},{"text":"I'm facing this issue on 3G","subCat":[],"active":false,"label":"VoIP","theme":"VOIP&Video"},{"text":"I'm facing this issue on WiFi","subCat":[],"active":true,"label":"VoIP","theme":"VOIP&Video"}],"active":false}],"text":"Hike Calls","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share the phone number of the contact with whom you are facing this issue and add a screenshot on the next screen.","active":true,"theme":"ChatExperience","label":"Last Seen"}],"text":"I'm seeing Incorrect Time Stamp for my contact on the top of the chat window.","active":false},{"subCat":[{"subCat":[],"text":"Please share the phone number of the contact with whom you are facing this issue and add a screenshot on the next screen.","active":true,"theme":"ChatExperience","label":"Last Seen"}],"text":"When my contact is typing, it's showing last seen time instead of 'Online'","active":false},{"subCat":[],"text":"It's showing 'A while ago' for my contact.","active":false,"theme":"ChatExperience","label":"Last Seen"},{"subCat":[{"subCat":[],"text":"Please share the phone number of the contact with whom you are facing this issue and add a screenshot.","active":true,"theme":"ChatExperience","label":"Last Seen"}],"text":"Last seen is shown after some time for my contact. It's basically slow to upload.","active":false},{"text":"I'm not able to see last seen","subCat":[{"text":"Please share any one mobile number of your favourite contact for whom you are unable to see the last seen.","subCat":[],"active":true,"theme":"CPR","label":"Last seen"}],"active":true,"label":"Last seen","theme":"ChatExperience"}],"text":"Last seen","active":false,"label":"I'm not able to see last seen","theme":"ChatExperience"},{"text":"Hike is consuming too much space","subCat":[{"text":"I'm referring to disk storage space","subCat":[{"text":"Any screenshots to indicate or show how much storage is hike taking?","subCat":[],"active":true,"theme":"CPR","label":"RAM consumption"}],"active":false},{"text":"I'm referring to RAM usage","subCat":[{"text":"Any screenshots to indicate or show how much RAM is hike taking?","subCat":[],"active":true,"theme":"CPR","label":"RAM consumption"}],"active":false},{"text":"Too many updates and it's consuming data","subCat":[],"active":true,"theme":"CPR","label":"RAM consumption"}],"active":false},{"text":"Hike is consuming too much Battery","subCat":[{"text":"Any screenshots to indicate or show how much battery hike is consuming?","subCat":[],"active":true,"theme":"CPR","label":"Battery consumption"}],"active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share a screenshot on the next screen.","active":true,"theme":"CPR","label":"Delete Acc"}],"text":"I'm unable to delete my hike account. I'm getting an error message.","active":true}],"text":"Delete Account","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please share your comments and screenshots on the next screen.","active":true,"theme":"Growth","label":"Natasha"}],"text":"Natasha bot is not sharing correct information. Please improve.","active":true},{"type":"weburl","url":"http://support.hike.in/entries/93169938-How-do-I-activate-Natasha-","subCat":[],"text":"How to subscribe/unsubscribe to Natasha bot?"}],"text":"Natasha","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"News feature is only available for Android OS version 4.1 and above.","active":false,"theme":"Content","label":"News"}],"text":"I'm not getting News.","active":false},{"subCat":[{"subCat":[],"text":"Please share your comments and add screenshots on the next screen.","active":false,"theme":"Content","label":"News"}],"text":"I'm seeing a blank screen on opening news.","active":false},{"subCat":[{"subCat":[],"text":"Please share your comments and screenshots on the next screen.","active":true,"theme":"Content","label":"News"}],"text":"I'm getting a notification for a News from hike, but there's nothing inside the app.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/95567027-How-do-I-subscribe-to-Hike-News","subCat":[],"text":"How do I subscribe/unsubscribe to news??","active":false},{"subCat":[{"subCat":[],"text":"Please share your comments and screenshots on the next screen.","active":true,"theme":"Content","label":"News"}],"text":"I'm not able to scroll through the news properly.","active":false},{"subCat":[{"subCat":[],"text":"Please share your comments and screenshots on the next screen.","active":true,"label":"News Content Issue","theme":"Content"}],"text":"News is showing up in the incorrect category.","active":true},{"text":"News is not working","subCat":[{"text":"Please share your comments and screenshots on the next screen.","subCat":[],"active":false,"theme":"Content","label":"News is not working"}],"active":false},{"text":"I am not getting News even after enabling it","subCat":[{"text":"You might have blocked news in the past. Please unblock and re-enable to start getting it. Follow these steps to Unblock: 3-Dot Menu(Available on the top-right corner) >> Settings >> Privacy >> Blocked List >> Select News >> Save.","subCat":[],"active":true,"theme":"Content","label":"I have enabled news but can't see it on the conversation screen"}],"active":false},{"text":"Error in a news article","subCat":[],"active":false,"theme":"Content","label":"There is an error in the news article"},{"text":"Full story redirects to other websites","subCat":[],"active":false,"theme":"Content","label":"Full story button is redirecting me to other websites"},{"text":"I am getting Ads in news","subCat":[],"active":false,"theme":"Content","label":"I am getting Ads in news"},{"text":"News is not getting updated","subCat":[],"active":false,"theme":"Content","label":"I'm not getting the latest news"},{"text":"It only shows \"Fetching News\"","subCat":[],"active":false,"theme":"Content","label":"It only shows \"Fetching News\""},{"text":"I do not get notifications for News","subCat":[],"active":false,"theme":"Content","label":"I do not get notifications for News"},{"text":"Image quality is Poor","subCat":[],"active":false,"theme":"Content","label":"News image quality is poor"},{"text":"Photos in news take time to load","subCat":[],"active":false,"theme":"Content","label":"Photos in news take time to load"},{"text":"Others","subCat":[],"active":false,"theme":"Content","label":"Other news related issues"}],"text":"News","active":false},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/95756038-How-do-I-share-the-Admin-rights-of-a-Group-Chat-","subCat":[],"text":"How to make a member a group admin in the Group Chat?","active":false},{"subCat":[],"text":"If you have any awesome ideas to make Group Chat better, please share it on the next screen.","active":false,"theme":"ChatExperience","label":"Group Chat"}],"text":"Group Chats","active":false},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/21762434-Walkie-Talkie","subCat":[],"text":"How do I use hike Walkie Talkie?","active":false},{"subCat":[],"text":"The voice quality in the Walkie-Talkie message is poor.","active":false,"label":"Walkie Talkie","theme":"ChatExperience"},{"subCat":[],"text":"For any other feedback on hike Walkie-Talkie, please share your comments on the next screen.","active":true,"theme":"ChatExperience","label":"Walkie Talkie"}],"text":"Walkie Talkie","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Please remove the media in that chat and then try to email the Chat again.","active":true,"theme":"HomeScreen5.0","label":"Email conversation"}],"text":"My hike is crashing while emailing the chats.","active":true},{"subCat":[],"text":"For any further feedback on Email Conversation please leave your comments.","active":false,"theme":"HomeScreen5.0","label":"Email conversation"}],"text":"Issues while Emailing Conversations","active":false},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-","subCat":[],"text":"I'm not able to see the names of the contacts I have added to my address book recently.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/94779718-I-cannot-see-my-contacts-on-hike-For-Xiaomi-phones-","subCat":[],"text":"My contacts are not syncing in hike contact list and I'm using a Xiaomi device.","active":false},{"type":"weburl","url":"http://support.hike.in/entries/96161427-I-cannot-see-my-contacts-on-hike-For-Lenovo-phones-","subCat":[],"text":"My contacts are not syncing in hike contact list and I'm using a Lenovo device.","active":false},{"subCat":[{"subCat":[],"text":"Please share the details on the next screen.","active":false,"theme":"CPR","label":"Contact sync","logkey":"ab"}],"text":"The above contact sync options don't help.","active":false},{"subCat":[{"subCat":[],"text":"Please share your number and the number from which you are receiving messages","active":true,"theme":"CPR","label":"Block contact issue","logkey":"ab"}],"text":"I'm receiving messages from blocked contacts.","active":true}],"text":"Contacts","active":false},{"subCat":[{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/95735838-How-to-use-Stickey-in-Xiaomi-Phones-","subCat":[],"text":"I'm using a Xiaomi device."},{"subCat":[],"text":"I'm using a Lenovo device","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"I'm using a Motorola device with android version 5.1","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"I'm using an Intex Aqua device","active":true,"label":"Stickey","theme":"Growth"}],"text":"I don't see Stickey under my hike account settings","active":false},{"subCat":[{"type":"weburl","url":"http://support.hike.in/entries/96153217-Why-am-I-not-able-to-see-Stickey-","subCat":[],"text":"Please read this FAQ to know more."}],"text":"I'm not able to enable hike stickey."},{"subCat":[],"text":"Stickey is not getting enabled for me. I have given all permissions to hike Stickey from phone settings and still not getting this enabled.","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"I'm not happy with the hike stickey accessibility permissions required for using hike Stickey.","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"Stickers are appearing like images when we are sharing them with friends via Stickey. They should look like stickers.","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"Increase the number of stickers we can share via hike Stickey. 5 is too less.","active":false,"label":"Stickey","theme":"Growth"},{"subCat":[],"text":"I need a sticker recommendation feature while using hike Stickey.","active":false,"label":"Stickey","theme":"ExpressYourself"},{"subCat":[{"subCat":[],"text":"Please share a screenshot on the next screen.","active":true,"label":"Stickey","theme":"Growth"}],"text":"When I use Stickey it leaves a black circle.","active":true},{"text":"Xiaomi device","subCat":[{"text":"I am not able to use stickey on my Xiaomi device","subCat":[],"active":true,"theme":"Growth","label":"I am not able to use stickey on my Xiaomi device"}],"active":false}],"text":"Stickey","active":false},{"text":"Hike Direct","subCat":[{"text":"My hike direct is not connecting","subCat":[{"text":"Please share the number with which you are trying to connect on the next screen","subCat":[],"active":true,"label":"Hike direct not working","theme":"ChatExperience"}],"active":false},{"text":"It's taking too much time to connect","subCat":[{"text":"Please share the number with which you are trying to connect on the next screen","subCat":[],"active":true,"label":"Hike direct not working","theme":"ChatExperience"}],"active":true}],"active":false},{"text":"Free SMS","subCat":[{"text":"My free SMS are not delivered to my contacts","subCat":[{"text":"I'm sending SMS to hike contacts who are offline","subCat":[{"text":"Please share the number of the contact with whom the messages are not delivered and approximate date and time of the message on the next screen","subCat":[],"active":true,"label":"Free SMS","theme":"Growth"}],"active":false},{"text":"I'm sending SMS to contacts who are not on hike.","subCat":[{"text":"Please share the number of the contact with whom the messages are not delivered and approximate date and time of the message on the next screen","subCat":[],"active":true,"theme":"Growth","label":"H2S"}],"active":true}],"active":true},{"text":"My Free SMS are not increasing","subCat":[{"text":"Free SMS will increase according to your hike usage. The more you use hike, the more free SMS you will get. However, if your SMS are still not increasing after using hike, please let us know.","subCat":[],"active":true,"theme":"Growth","label":"H2S"}],"active":false}],"active":false},{"text":"I'm getting an error while updating hike from Playstore","subCat":[{"text":"I'm seeing some error code on Playstore","subCat":[{"text":"Please share the details of the error code in the next screen","subCat":[],"active":true,"label":"Update error"}],"active":true},{"text":"Others","subCat":[],"active":false}],"active":false},{"text":"My hike account has been hacked","subCat":[{"text":"Please let us know in details why you are suspecting that your hike account might be hacked on the next screen","subCat":[],"active":true,"label":"Account hacked","theme":"CPR"}],"active":false},{"active":false,"subCat":[{"subCat":[{"subCat":[],"text":"Please share the Operator Name (Airtel, Vodafone, idea, etc.) and State (Circle) in the next screen","active":true,"theme":"Growth","label":"7 day challenge"}],"text":"I'm unable to recharge.","active":false},{"text":"My hike Meter is not working.","subCat":[{"text":"My meter is stuck at 1 Minute","subCat":[],"active":false,"theme":"Growth","label":"7 day challenge"},{"text":"My meter was working fine, but it stopped working","subCat":[],"active":true,"theme":"Growth","label":"7 day challenge"}],"active":false},{"subCat":[],"text":"Others","active":false,"theme":"Growth","label":"7 day challenge"}],"text":"7 Day Challenge"},{"text":"Just For Laughs","subCat":[{"text":"I am not getting Just For Laughs","subCat":[{"text":"Just For Laughs feature is only available on Hike App version 4.1 and above.","subCat":[],"active":true,"theme":"Content","label":"Request to enable JFL"}],"active":false},{"text":"I am getting Just For Laughs even after blocking it","subCat":[],"active":false,"theme":"Content","label":"Block not working for JFL"},{"text":"Just For Laughs is not funny","subCat":[],"active":false,"theme":"Content","label":"JFL is not funny"},{"text":"Report offensive content","subCat":[],"active":true,"theme":"Content","label":"Offended by JFL content"},{"text":"Image quality is Poor","subCat":[],"active":false,"theme":"Content","label":"JFL Image quality is poor"},{"text":"I'm not able to download Just For Laughs images","subCat":[],"active":false,"theme":"Content","label":"Unable to download JFL images"}],"active":false},{"text":"Coupons","subCat":[{"text":"The coupon code shows invalid at the brand website","subCat":[{"text":"Please mention your comments and add screenshots on the next screen","subCat":[],"active":true,"theme":"Content","label":"Invalid coupon code"}],"active":false},{"text":"The coupon was not accepted at the brand outlet","subCat":[{"text":"Please mention the brand name and details of the outlet you visited.","subCat":[],"active":true,"theme":"Content","label":"Coupon code not accepted at outlet"}],"active":false},{"text":"I'm getting an error while downloading a coupon","subCat":[{"text":"Please mention your comments and add screenshots on the next screen","subCat":[],"active":false,"theme":"Content","label":"Error on downloading a coupon"}],"active":true},{"text":"Others","subCat":[],"active":false,"theme":"Content","label":"Other coupon related issues"}],"active":false,"label":"Other coupon related issues","theme":"Content"},{"text":"Hike Daily","subCat":[{"text":"I did not get hike daily today","subCat":[],"active":false,"theme":"Content","label":"Hike Daily not received"},{"text":"The fact you shared is not correct","subCat":[],"active":false,"theme":"Content","label":"Discrepancy in the fact shared"},{"text":"Others","subCat":[],"active":false,"theme":"Content","label":"Other hike daily related issues"}],"active":false},{"text":"Cricket","subCat":[{"text":"I am not getting cricket updates","subCat":[{"text":"Cricket feature is only available for Android OS version 4.0 and above.","subCat":[],"active":true,"theme":"Content","label":"Request to enable Cricket"}],"active":false},{"text":"I do not have videos in cricket","subCat":[{"text":"Videos in cricket are enabled for android users on hike app version 4.2.5 and above.","subCat":[],"active":false,"label":"Wants Hotstar videos","theme":"Content"}],"active":false},{"text":"Cricket videos are not playing","subCat":[{"text":"I have Google Chrome","subCat":[{"text":"The videos play only on Google Chrome version 45 and above. If you are using a lower version of Google Chrome, please update to the latest version from the Playstore.","subCat":[],"active":false,"label":"Hotstar videos not playing on lower versions of Chrome","theme":"Content"},{"text":"I am using Google Chrome version 45 or above. Go to Chrome settings and clear data.","subCat":[],"active":true,"label":"Hotstar videos not playing on required versions of Chrome","theme":"Content"}],"active":false},{"text":"I do not have Google Chrome","subCat":[{"text":"Please download the latest version of Google Chrome from the Playstore.","subCat":[],"active":true,"label":"Unable to play Hotstar videos without Chrome","theme":"Content"}],"active":false}],"active":true},{"text":"Cricket fact disappears before I can read it","subCat":[],"active":false,"theme":"Content","label":"Cricket fact disappears before I can read it"},{"text":"I am Stuck on the Cricket Fact screen","subCat":[{"text":"Please mention your comments and add screenshots on the next screen","subCat":[],"active":true,"theme":"Content","label":"Getting stuck on Cricket fact screen"}],"active":false},{"text":"I'm seeing a blank screen on opening cricket","subCat":[{"text":"Please mention your comments and add screenshots on the next screen","subCat":[],"active":true,"theme":"Content","label":"Blank screen in Cricket"}],"active":false},{"text":"Scores are not updating/Delay in score updation","subCat":[],"active":false,"theme":"Content","label":"Scores not updating/lagging"},{"text":"I am getting Ads in Cricket","subCat":[],"active":false,"theme":"Content","label":"Ads in cricket"},{"text":"Others","subCat":[],"active":false,"theme":"Content","label":"Other cricket related issues"}],"active":false},{"text":"Block contact","subCat":[{"text":"I am receiving messages from a blocked contact","subCat":[],"active":true,"theme":"HomeScreen5.0","label":"I am receiving messages from a blocked contact"}],"active":false},{"text":"Languages and Keyboard","subCat":[{"text":"Languages","subCat":[{"text":"Report incorrect Translation","subCat":[{"text":"Hindi","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Tamil","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"label":"Languages","theme":"ChatExperience"}],"active":false},{"text":"Marathi","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Telugu","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Gujarathi","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Bengali","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Kannada","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":false},{"text":"Malayalam","subCat":[{"text":"Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen","subCat":[],"active":true,"theme":"ChatExperience","label":"Languages"}],"active":true}],"active":false},{"active":true,"subCat":[],"text":"Add more languages","theme":"ChatExperience","label":"Keyboard"}],"active":false},{"text":"Keyboard","subCat":[{"text":"Keyboard is hanging while Typing","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Keyboard Layout is not good","subCat":[{"text":"Which all Keyboard are you using","subCat":[{"text":"Hindi","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Tamil","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Marathi","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Telugu","subCat":[],"active":false,"label":"Keyboard","theme":"ChatExperience"},{"text":"Gujarathi","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Bengali","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Kannada","subCat":[],"active":false,"theme":"ChatExperience","label":"Keyboard"},{"text":"Malayalam","subCat":[],"active":true,"theme":"ChatExperience","label":"Keyboard"}],"active":true}],"active":true}],"active":true}],"active":false},{"text":"hike caller","subCat":[{"text":"I do not have hike caller in my mobile","subCat":[],"active":false,"theme":"Growth","label":"I do not have hike caller in my mobile"},{"text":"I want to unlist/de-register my number from hike caller","subCat":[],"active":false,"theme":"Growth","label":"I want to unlist/de-register my number from hike caller"},{"text":"I want to change my name appearing on hike caller","subCat":[],"active":true,"theme":"Growth","label":"I want to change my name appearing on hike caller"}],"active":false},{"active":false,"subCat":[{"subCat":[{"subCat":[],"text":"Please try to change the color of your LED notification from hike settings > Notifications > LED Notification > change the color","active":true,"label":"LED Notification","theme":"CPR"}],"text":"My LED is not blinking when receiving a new message?","active":false},{"subCat":[{"subCat":[{"subCat":[],"text":"Does not help, then share your comments and screenshots on the next screen"}],"text":"Go to hike settings > Notifications > Contact Joining hike > Turn this OFF","active":true,"theme":"HomeScreen5.0","label":"NUJ/RUJ"}],"text":"I wish to turn OFF Notification related to joining of users.","active":false},{"subCat":[],"text":"Others","active":true}],"text":"Other Issues"},{"text":"Android N","subCat":[{"text":""}],"active":false,"theme":"Growth","label":"Android N"}],"icon":"issue","type":"Bug","text":"Report an Issue"},{"active":false,"subCat":[],"url":"http://www.hike.in/help/android","icon":"faq","type":"weburl","text":"FAQs"},{"active":false,"subCat":[],"url":"http://www.twitter.com/hikestatus","icon":"system-health","type":"weburl","text":"System Health"},{"active":false,"subCat":[],"url":"http://www.hike.in/terms/android","icon":"tnc","type":"weburl","text":"Terms and Conditions"}];


/*staging data.js*/
window.catData = [
	{
		"active": false,
		"type": "Feature",
		"text": "Suggest Something New",
		"subCat": [
			{
				"active": true,
				"text": "Suggest a new feature",
				"subCat": [
					{
						"active": false,
						"text": "Custom fonts",
						"subCat": [
							{
								"active": false,
								"text": "I want to use my system font. Hike doesn't support that!",
								"theme": "Content",
								"subCat": [],
								"label": "Test"
							},
							{
								"active": true,
								"text": "I would like to change the font size while using hike.",
								"theme": "Content",
								"subCat": [],
								"label": "Test"
							}
						]
					},
					{
						"active": false,
						"text": "Change Number",
						"subCat": [
							{
								"active": true,
								"text": "I use a dual SIM phone and wish to use hike with both my numbers.",
								"theme": "Content",
								"subCat": [],
								"label": "Test123"
							},
							{
								"text": "I want to use my hike account with multiple mobile numbers.",
								"subCat": []
							},
							{
								"text": "I want to have multiple device support for my hike account.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Night Mode",
						"subCat": [
							{
								"text": "I want a dark chat theme which is less bright in dim light especially for chatting in the night time.",
								"subCat": []
							},
							{
								"text": "I want to reduce the brightness of the screen.",
								"subCat": []
							},
							{
								"text": "I want the font color, icon color etc. to be changed when enabling night mode.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Widgets",
						"subCat": [
							{
								"text": "I want hike app widget on my phone home screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "Message Information",
						"subCat": [
							{
								"text": "I want to know the message delivery and read time especially in group chats.",
								"subCat": []
							}
						]
					},
					{
						"text": "Video Calling",
						"subCat": [
							{
								"text": "Please introduce hike video calling soon.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Pop-up notifications/Quick Reply",
						"subCat": [
							{
								"text": "I want a quick reply window to answer to an incoming message without opening the app.",
								"subCat": []
							}
						]
					},
					{
						"text": "An unread message counter on hike icon would be great.",
						"subCat": []
					},
					{
						"text": "Multi-screen",
						"subCat": [
							{
								"text": "I want hike support for the multi-screen feature in my phone.",
								"subCat": []
							}
						]
					},
					{
						"text": "Web/PC app",
						"subCat": []
					},
					{
						"active": false,
						"text": "Custom Notification Tones",
						"subCat": [
							{
								"text": "I want to designate different notification tones to my one-on-one and group chats.",
								"subCat": []
							},
							{
								"active": false,
								"text": "I want different notification tones for Group Chats and one-on-one chats.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Mute a chat",
						"subCat": [
							{
								"text": "Please add a feature to mute a one-on-one chat.",
								"subCat": []
							},
							{
								"text": "I want to mute a chat for a particular time limit.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Turn Off Read Notifications",
						"subCat": []
					},
					{
						"active": false,
						"text": "Gesture based swipes",
						"subCat": []
					},
					{
						"text": "Hide content previews in notifications",
						"subCat": []
					},
					{
						"text": "Birthday reminders",
						"subCat": []
					},
					{
						"text": "Schedule Messages",
						"subCat": []
					},
					{
						"text": "GIF support",
						"subCat": []
					},
					{
						"active": false,
						"text": "Chat Heads",
						"subCat": []
					},
					{
						"active": false,
						"text": "Live Sports scores",
						"subCat": [
							{
								"text": "Football"
							},
							{
								"text": "Hockey"
							},
							{
								"text": "Kabaddi"
							},
							{
								"text": "Tennis"
							},
							{
								"text": "Others"
							}
						]
					},
					{
						"active": false,
						"text": "Horoscope",
						"subCat": []
					},
					{
						"active": false,
						"text": "Others",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Improve an existing feature",
				"subCat": [
					{
						"active": false,
						"text": "Hike Direct",
						"subCat": [
							{
								"text": "Hike Direct for Group Chats.",
								"subCat": []
							},
							{
								"active": false,
								"text": "Hike direct radius should be more than 100 meters",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "News",
						"subCat": [
							{
								"active": false,
								"text": "Add more Languages",
								"subCat": [
									{
										"text": "Tamil"
									},
									{
										"text": "Marathi"
									},
									{
										"text": "Kannada"
									},
									{
										"text": "Telugu"
									},
									{
										"text": "Gujarati"
									},
									{
										"text": "Malayalam"
									},
									{
										"text": "Urdu"
									},
									{
										"text": "Bengali"
									},
									{
										"text": "Others"
									}
								]
							},
							{
								"active": false,
								"text": "Add more categories",
								"subCat": [
									{
										"text": "Employment/Jobs"
									},
									{
										"text": "Weather Forecast"
									},
									{
										"text": "BSE/NSE"
									},
									{
										"text": "Automobiles"
									},
									{
										"text": "Start-Ups"
									},
									{
										"text": "Others"
									}
								]
							},
							{
								"text": "Add refresh button in News Section"
							},
							{
								"active": false,
								"text": "Bookmark News articles",
								"subCat": []
							},
							{
								"text": "Customized news categories"
							},
							{
								"text": "Share News cross Platforms"
							},
							{
								"text": "Option to mark News as Read/Unread"
							},
							{
								"text": "Add News widget"
							},
							{
								"text": "Add Search option in News"
							},
							{
								"text": "Save news to read offline"
							},
							{
								"text": "Add Like button"
							},
							{
								"text": "Night Mode"
							},
							{
								"text": "Add \"Go to Top\" button"
							},
							{
								"text": "Publish more stories"
							},
							{
								"text": "I want to decide when I want news notifications"
							},
							{
								"text": "Others"
							}
						]
					},
					{
						"active": true,
						"text": "Stickers",
						"subCat": [
							{
								"text": "Animated Stickers",
								"subCat": []
							},
							{
								"text": "Custom Stickers",
								"subCat": []
							},
							{
								"active": true,
								"text": "Sticker Shop",
								"subCat": [
									{
										"active": false,
										"text": "Hi 1",
										"theme": "erergdsfgdg",
										"subCat": [],
										"label": "saferg"
									},
									{
										"active": false,
										"text": "Hi 2",
										"theme": "wergdfgrthythyhr",
										"subCat": [],
										"label": "sgrtgrtgafsfdfrg"
									},
									{
										"active": false,
										"text": "Test",
										"subCat": []
									},
									{
										"text": "hi 3"
									}
								]
							},
							{
								"active": false,
								"text": "Preview sticker before sharing",
								"subCat": []
							},
							{
								"text": "Sticker Confirmation before sending sticker"
							},
							{
								"text": "Reduce the size of Sticker"
							}
						]
					},
					{
						"active": false,
						"text": "Status Updates",
						"subCat": [
							{
								"text": "I want to share stickers in Status update",
								"subCat": []
							},
							{
								"text": "Please add more moods for Status updates",
								"subCat": []
							},
							{
								"text": "Copy pasting my friends Status updates",
								"subCat": []
							},
							{
								"text": "Sharing hike daily in Status update",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Search",
						"subCat": [
							{
								"text": "Please add a feature to search for messages between particular dates.",
								"subCat": []
							},
							{
								"text": "I want a universal search button to search keywords on hike conversation list screen.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Timeline",
						"subCat": [
							{
								"text": "Please add a comment feature for timeline posts",
								"subCat": []
							},
							{
								"text": "I want to know who all have liked the posts I'm liking on Timeline",
								"subCat": []
							},
							{
								"text": "I don't want the like feature",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"text": "Hidden Mode",
						"subCat": [
							{
								"text": "Hide the timeline posts for contacts that are hidden",
								"subCat": []
							},
							{
								"text": "Sharing media from gallery directly to a hidden chat",
								"subCat": []
							},
							{
								"text": "Hiding the hidden chat notification",
								"subCat": []
							},
							{
								"text": "Hiding media shared in a hidden chat",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"text": "Chat Themes",
						"subCat": [
							{
								"text": "Custom Chat Themes",
								"subCat": []
							},
							{
								"text": "Custom Nudges in Chat Themes",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"text": "Message Backup",
						"subCat": [
							{
								"text": "I want Google Drive Backup",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "File Sharing",
						"subCat": [
							{
								"text": "Image crop option",
								"subCat": []
							},
							{
								"text": "Adding a caption to an image",
								"subCat": []
							},
							{
								"text": "Rotating an image while sharing",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							}
						]
					},
					{
						"text": "Group Chats",
						"subCat": [
							{
								"text": "Disable Nudges in Group Chats",
								"subCat": []
							}
						]
					},
					{
						"text": "Profile Photo",
						"subCat": []
					},
					{
						"active": false,
						"text": "hike app design feedback",
						"subCat": [
							{
								"active": false,
								"text": "I want multiple tabs",
								"subCat": []
							},
							{
								"text": "Others",
								"subCat": []
							},
							{
								"active": false,
								"text": "I want 3 Tab design",
								"subCat": []
							}
						]
					},
					{
						"text": "Broadcast",
						"subCat": []
					},
					{
						"active": false,
						"text": "Hike Daily",
						"subCat": [
							{
								"active": true,
								"text": "I want hike daily quotes from Indians",
								"subCat": []
							},
							{
								"active": false,
								"text": "I want hike daily in Hindi",
								"subCat": []
							},
							{
								"active": false,
								"text": "Send more quotes",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Coupons",
						"subCat": []
					},
					{
						"active": false,
						"text": "Cricket",
						"subCat": []
					},
					{
						"active": false,
						"text": "Just For Laughs",
						"subCat": []
					},
					{
						"active": false,
						"text": "Match Up!",
						"subCat": [
							{
								"active": true,
								"text": "I should be able to see who has liked my profile",
								"subCat": []
							},
							{
								"active": false,
								"text": "Increase the limit of matches",
								"subCat": []
							},
							{
								"active": false,
								"text": "Match up suggestions should be on basis of location",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Others",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Suggest a new Sticker",
				"subCat": []
			},
			{
				"active": false,
				"text": "Share a joke with us",
				"subCat": []
			},
			{
				"active": false,
				"text": "test",
				"subCat": [
					{
						"active": true,
						"text": "tester",
						"theme": "tester",
						"subCat": [],
						"label": "test"
					}
				]
			},
			{
				"text": "teste2"
			},
			{
				"text": "sfsfsfsfgghh"
			}
		],
		"icon": "feature"
	},
	{
		"active": false,
		"type": "Bug",
		"text": "Report an App Issue",
		"subCat": [
			{
				"active": false,
				"text": "Message delivery is Slow / Not working 105",
				"subCat": [
					{
						"active": false,
						"text": "Sending messages to friends on hike",
						"subCat": [
							{
								"active": true,
								"text": "Slowness from changing from ✓ to ✓✓",
								"subCat": [
									{
										"active": true,
										"text": "Slow with ALL my friends",
										"subCat": []
									},
									{
										"active": false,
										"text": "Slow with 1-2 friends in Specific",
										"subCat": [
											{
												"active": false,
												"text": "There is a very high chance that if your friend is on certain Android devices (Asus, Xiaomi, Lenovo, Huawei etc) that s/he will need to do something via the respective FAQ that we will share with you and then the problem should solve itself.",
												"subCat": [],
												"nextScreen": "0"
											},
											{
												"active": true,
												"text": "My Friend is using a Xiaomi Phone",
												"subCat": [
													{
														"active": false,
														"text": "With MiUI7",
														"subCat": [],
														"url": "http://support.hike.in/entries/98277657-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-7-",
														"type": "weburl"
													},
													{
														"active": false,
														"text": "With MiUI6",
														"subCat": [],
														"url": "http://support.hike.in/entries/55998480-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-6-",
														"type": "weburl"
													},
													{
														"active": false,
														"text": "With MiUI5",
														"subCat": [],
														"url": "http://support.hike.in/entries/95306838-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-5-",
														"type": "weburl"
													},
													{
														"active": false,
														"text": "I don't know how to check the MiUI Version",
														"subCat": [],
														"url": "http://support.hike.in/entries/98675887-How-do-I-know-my-MiUI-version-",
														"type": "weburl"
													}
												]
											},
											{
												"text": "My Friend is using a Lenovo Phone"
											},
											{
												"text": "My Friend is using a Huawei Phone"
											},
											{
												"text": "MY Friend is using an Asus Phone"
											}
										]
									}
								]
							},
							{
								"active": false,
								"text": "My messages are stuck on clock",
								"subCat": [
									{
										"text": "My messages are stuck on Clock while I'm on WiFi.",
										"subCat": [
											{
												"text": "Please tell us which WiFi network are you using on the next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "My messages are stuck on Clock while I'm on Mobile Data (2G/3G/4G).",
										"subCat": [
											{
												"text": "Please tell us which network operator services are you using on the next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "My messages are stuck on Clock on both WiFi and Mobile Data (2G/3G/4G).",
										"subCat": [
											{
												"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
												"subCat": [],
												"type": "weburl",
												"text": "Have you signed up on a different device using the same number? You can restore data from one device, reset the account and sign up on the device you wish to."
											},
											{
												"text": "I'm not signed up on two devices simultaneously.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "My messages are stuck on single tick (✓)",
								"subCat": [
									{
										"text": "My messages are stuck on single tick (✓) while I'm on WiFi",
										"subCat": [
											{
												"text": "Please tell us which WiFi network are you using on the next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "My messages are stuck on single tick (✓) while I'm on Mobile Data (2G/3G/4G).",
										"subCat": [
											{
												"text": "Please tell us which network operator services are you using on the next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "If the above options didn't help, please enter the mobile number of the contact with whom you are facing this issue on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "My messages are stuck on double tick (✓✓) but not delivered.",
								"subCat": [
									{
										"text": "My messages are stuck on double tick (✓✓) with my contacts who are on iOS.",
										"subCat": []
									},
									{
										"text": "My messages are stuck on double tick (✓✓) with all my contacts.",
										"subCat": [
											{
												"text": "Please share some contact details with whom you are facing this issue on the next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"active": false,
								"text": "Slowness from changing from Clock to ✓",
								"subCat": []
							},
							{
								"text": "Slowness from changing from ✓✓ to ✓✓R"
							}
						]
					},
					{
						"active": false,
						"text": "Sending messages to friends not on hike",
						"subCat": [
							{
								"text": "My messages are not delivered to friends who are not on hike.",
								"subCat": [
									{
										"text": "Please share the mobile number of the contact with whom you are facing this issue on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "My messages are stuck in Clock state and I'm facing this on WiFi.",
								"subCat": [
									{
										"text": "Please tell us which WiFi network are you using on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "My messages are stuck in Clock state and I'm facing this on mobile data(2G/3G/4G).",
								"subCat": [
									{
										"text": "Please tell us which network operator services are you using on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
								"subCat": [],
								"type": "weburl",
								"text": "Have you signed up with the same number on a different device? You can restore data from one device, reset the account and sign up on the device you wish to."
							}
						]
					},
					{
						"active": false,
						"text": "Sending messages to friends on hike who are offline",
						"subCat": [
							{
								"text": "The pop up for sending an offline message appears very late.",
								"subCat": []
							},
							{
								"text": "I would like an option to send the offline message as an SMS directly instead of waiting for the popup to appear.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I'm seeing 'No internet Connection' But my internet is working fine.",
						"subCat": [
							{
								"active": false,
								"text": "I'm facing this issue only on WiFi",
								"subCat": [
									{
										"active": true,
										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
										"subCat": [],
										"nextScreen": "0"
									},
									{
										"active": false,
										"text": "test.hike.in",
										"subCat": [],
										"url": "http://test.hike.in",
										"type": "weburl"
									},
									{
										"active": false,
										"text": "I've checked test.hike.in and Everything is working fine.",
										"subCat": [
											{
												"active": false,
												"text": "I'm using a Home WiFi",
												"subCat": [
													{
														"text": "Please share your WiFi Provider details on the next screen"
													}
												]
											},
											{
												"active": false,
												"text": "I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)",
												"subCat": [
													{
														"text": "Please share your WiFi Provider details on the next screen"
													}
												]
											}
										]
									},
									{
										"active": false,
										"text": "I'm seeing an error in test.hike.in",
										"subCat": [
											{
												"active": true,
												"text": "I'm using a Home WiFi",
												"subCat": [
													{
														"active": true,
														"text": "Please share a screenshot of the error and your WiFi Provider details on the next screen",
														"subCat": []
													}
												]
											},
											{
												"active": false,
												"text": "I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)",
												"subCat": [
													{
														"active": true,
														"text": "Please share a screenshot of the error and your WiFi Provider details on the next screen",
														"subCat": []
													}
												]
											}
										]
									}
								]
							},
							{
								"active": false,
								"text": "I'm facing this issue only on Mobile Data",
								"subCat": [
									{
										"active": true,
										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
										"subCat": [],
										"nextScreen": "0"
									},
									{
										"active": false,
										"text": "http://test.hike.in",
										"subCat": [],
										"url": "http://test.hike.in",
										"type": "weburl"
									},
									{
										"active": false,
										"text": "I've checked test.hike.in and Everything is working fine.",
										"subCat": [
											{
												"text": "Please share your Service Provide/Operator details on the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "I'm seeing an error in test.hike.in",
										"subCat": [
											{
												"text": "Please share a screenshot of the error and your Service Provider/Operator details on the next screen"
											}
										]
									}
								]
							},
							{
								"active": true,
								"text": "I'm seeing this on Mobile Mobile Data and WiFi",
								"subCat": [
									{
										"active": false,
										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
										"subCat": [],
										"nextScreen": "0"
									},
									{
										"active": false,
										"text": "http://test.hike.in",
										"subCat": [],
										"url": "http://test.hike.in",
										"type": "weburl"
									},
									{
										"active": false,
										"text": "I've checked test.hike.in and Everything is working fine.",
										"subCat": [
											{
												"text": "Please share your Service Provide/Operator details on the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "I'm seeing an error in test.hike.in",
										"subCat": [
											{
												"text": "Please share a screenshot of the error and your Service Provider/Operator details on the next screen"
											}
										]
									}
								],
								"nextScreen": "0"
							},
							{
								"text": "test"
							}
						]
					},
					{
						"active": false,
						"text": "Opening a chat in hike is slow",
						"subCat": [
							{
								"text": "I'm facing this with all my chats"
							},
							{
								"text": "I'm facing this with one or two chats only"
							}
						]
					}
				],
				"priority": "4"
			},
			{
				"active": false,
				"text": "7 Day Challenge",
				"subCat": [
					{
						"active": false,
						"text": "I'm unable to recharge.",
						"subCat": [
							{
								"active": true,
								"text": "Please share the Operator Name (Airtel, Vodafone, idea, etc.) and State (Circle) in the next screen",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "My hike Meter is not working.",
						"subCat": [
							{
								"active": true,
								"text": "My meter is stuck at 1 Minute",
								"subCat": []
							},
							{
								"text": "My meter was working fine, but it stopped working"
							}
						]
					},
					{
						"text": "Others",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Languages and Keyboard",
				"subCat": [
					{
						"active": false,
						"text": "Languages",
						"subCat": [
							{
								"active": false,
								"text": "Report incorrect Translation",
								"subCat": [
									{
										"active": false,
										"text": "Hindi",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Tamil",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Marathi",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Telugu",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Gujarathi",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Bengali",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Kannada",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									},
									{
										"active": false,
										"text": "Malayalam",
										"subCat": [
											{
												"text": "Please share the current incorrect Translation, which screen you are seeing this and your suggestion in the next screen"
											}
										]
									}
								]
							},
							{
								"active": true,
								"text": "Add more languages",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Keyboard",
						"subCat": [
							{
								"active": false,
								"text": "Keyboard is hanging while Typing",
								"subCat": []
							},
							{
								"active": false,
								"text": "Keyboard Layout is not good",
								"subCat": [
									{
										"active": true,
										"text": "Which all Keyboard are you using",
										"subCat": [
											{
												"text": "Hindi"
											},
											{
												"text": "Tamil"
											},
											{
												"text": "Marathi"
											},
											{
												"text": "Telugu"
											},
											{
												"text": "Gujarathi"
											},
											{
												"text": "Bengali"
											},
											{
												"text": "Kannada"
											},
											{
												"text": "Malayalam"
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "File Transfer Issues",
				"subCat": [
					{
						"active": false,
						"text": "Sending anything is very slow and takes a long time to upload.",
						"subCat": [
							{
								"active": true,
								"text": "I'm sending from a Wi-Fi connection",
								"subCat": [
									{
										"active": true,
										"text": "Sending media (photos & videos) is slow",
										"subCat": [
											{
												"active": true,
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "I'm sending from my 2G mobile connection",
								"subCat": [
									{
										"text": "Sending media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "I'm sending from my 3G mobile connection",
								"subCat": [
									{
										"text": "Sending media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "I'm sending from my 4G mobile connection",
								"subCat": [
									{
										"text": "Sending media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "Sending is slow on both Wi-Fi and Mobile Data (2G/3G/4G)",
								"subCat": [
									{
										"text": "Sending media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Sending both media and non-media documents is slow.",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "Downloading anything is very slow and takes a long time to download.",
						"subCat": [
							{
								"text": "I'm downloading from a Wi-Fi connection",
								"subCat": [
									{
										"text": "Downloading media (photos & videos) is slow.",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"active": false,
								"text": "I'm downloading from my 2G mobile connection",
								"subCat": [
									{
										"text": "Downloading media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"active": true,
										"text": "uytuytuytuyt",
										"subCat": [
											{
												"active": true,
												"text": "jkhykjkkj",
												"subCat": [
													{
														"text": "guytuytuyt"
													}
												]
											}
										]
									}
								]
							},
							{
								"text": "I'm downloading from my 3G mobile connection",
								"subCat": [
									{
										"text": "Downloading media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "I'm downloading from my 4G mobile connection",
								"subCat": [
									{
										"text": "Downloading media (photos & videos) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading both media and non-media documents is slow",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "Downloading is slow on both Wi-Fi and Mobile Data (2G/3G/4G)",
								"subCat": [
									{
										"text": "Downloading media (photos & videos) is slow.",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow.",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "Downloading both media and non-media documents is slow.",
										"subCat": [
											{
												"text": "Please share the details on next screen.",
												"subCat": []
											}
										]
									}
								]
							}
						]
					},
					{
						"active": true,
						"text": "I'm getting an error when uploading.",
						"subCat": [
							{
								"text": "The error message when uploading is 'Could not upload the file. Check Network Settings'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "The error message when uploading is 'Unable to read file'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": false,
								"text": "The error message when uploading is 'No app found that can handle this action'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": false,
								"text": "The error message when uploading is 'Unsupported file'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": true,
								"text": "The error message when uploading is 'Max file size can be 100 MB'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": false,
								"text": "Another error message comes which is not shown above appears.",
								"subCat": [
									{
										"text": "Please share a screenshot of the error message on the next screen.",
										"subCat": []
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "I'm getting an error when downloading.",
						"subCat": [
							{
								"text": "The error message when downloading is 'Could not download the file. Check Network Settings'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "The error message when downloading is 'This file no longer exists'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "The error message when downloading is 'Your phone does not have enough space to download the file'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "The error message when downloading is 'Please insert an SD Card'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "The error message when uploading is 'Max file size can be 100 MB'",
								"subCat": [
									{
										"text": "Please share the screenshot and other details on next screen.",
										"subCat": []
									}
								]
							},
							{
								"text": "Another error message comes which is not shown above appears.",
								"subCat": [
									{
										"text": "Please share a screenshot of the error message on the next screen.",
										"subCat": []
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "Takes a lot of time to open the gallery after clicking it to choose a photo to upload.",
						"subCat": []
					},
					{
						"active": false,
						"text": "The quality of the photo is poor and pixelated after I send it to my friend.",
						"subCat": [
							{
								"text": "I shared the photo from inside chat thread > Attachments > Gallery",
								"subCat": [
									{
										"text": "I choose 'Compressed' option",
										"subCat": []
									},
									{
										"text": "I choose 'Normal' option",
										"subCat": []
									},
									{
										"text": "I choose 'Original' option",
										"subCat": []
									}
								]
							},
							{
								"text": "I shared the photo from inside chat thread > Attachments > Camera",
								"subCat": [
									{
										"text": "I choose 'Compressed' option",
										"subCat": []
									},
									{
										"text": "I choose 'Normal' option",
										"subCat": []
									},
									{
										"text": "I choose 'Original' option",
										"subCat": []
									}
								]
							},
							{
								"text": "I shared the photo from outside hike > Phone photo gallery > Share via hike",
								"subCat": []
							},
							{
								"text": "The photos I receive from my friend get black or colour stripped out after I download it.",
								"subCat": []
							}
						]
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"active": false,
				"text": "Notifications",
				"subCat": [
					{
						"active": false,
						"text": "I'm not getting any notification for a new message until I open the app.",
						"subCat": [
							{
								"active": false,
								"text": "I'm using a Xiaomi Phone",
								"subCat": [
									{
										"url": "http://support.hike.in/entries/98277657-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-7-",
										"subCat": [],
										"type": "weburl",
										"active": false,
										"text": "I'm using hike on Xiaomi MiUI 7"
									},
									{
										"url": "http://support.hike.in/entries/95306838-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-5-",
										"subCat": [],
										"type": "weburl",
										"active": false,
										"text": "I'm using hike on Xiaomi MiUi 5."
									},
									{
										"url": "http://support.hike.in/entries/55998480-I-m-not-getting-notification-on-my-Xiaomi-Phone-For-MIUI-6-",
										"subCat": [],
										"type": "weburl",
										"active": false,
										"text": "I'm using hike on Xiaomi MiUi 6."
									},
									{
										"url": "http://support.hike.in/entries/98675887-How-do-I-know-my-MiUI-version-",
										"subCat": [],
										"type": "weburl",
										"active": false,
										"text": "I don't know my MiUI Version"
									}
								]
							},
							{
								"active": false,
								"text": "I'm using a Lenovo Phone",
								"subCat": [],
								"url": "http://support.hike.in/entries/98036948-Enable-Notification-For-Lenovo-Phones",
								"type": "weburl"
							},
							{
								"active": false,
								"text": "I have given necessary permissions to hike from phone settings and still not getting any notification for an incoming message.",
								"subCat": []
							},
							{
								"active": false,
								"text": "I'm using a rooted device.",
								"subCat": [
									{
										"text": "If you have installed a Custom ROM, please share the OS version details on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": false,
								"text": "I'm using Clean Master or similar apps on my mobile phone",
								"subCat": [
									{
										"text": "Please share the details of the app on the next screen.",
										"subCat": []
									}
								]
							},
							{
								"active": false,
								"text": "I'm using a Asus Phone",
								"subCat": [],
								"url": "http://support.hike.in/entries/95935217-Enable-Notification-for-Asus",
								"type": "weburl"
							},
							{
								"active": false,
								"text": "I'm using a Meizu Phone",
								"subCat": [],
								"url": "http://support.hike.in/entries/95428747-Enable-notifications-for-MEIZU-Phones",
								"type": "weburl"
							},
							{
								"active": false,
								"text": "I'm using a Samsung S6, Galaxy S6 Edge Plus, Note Edge",
								"subCat": [],
								"url": "http://support.hike.in/entries/98608597-Not-getting-notification-on-my-Samsung-Phone-S6-Galaxy-S6-Edge-Plus-Note-Edge-etc-",
								"type": "weburl"
							},
							{
								"active": false,
								"text": "I'm using a Huawei Phone",
								"subCat": [],
								"url": "http://support.hike.in/entries/94785897-Enable-notifications-for-Huawei-Phones",
								"type": "weburl"
							}
						]
					},
					{
						"active": true,
						"text": "I'm facing issues with Notification Sounds",
						"subCat": [
							{
								"text": "I don't get any sound notification for an incoming message while listening to music.",
								"subCat": [
									{
										"text": "Are you listening to FM radio?",
										"subCat": []
									},
									{
										"text": "Are you listening to a music player?",
										"subCat": [
											{
												"text": "Please share music player details on the next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "On receiving any new hike message the music playing in the background stops.",
								"subCat": []
							},
							{
								"text": "Phone goes into Silent mode after using hike for a while. I have to force kill the app to resume to its default sound settings",
								"subCat": []
							},
							{
								"active": true,
								"text": "There is a continuous sound playing in the background on sending a message.",
								"subCat": [
									{
										"text": "It's the conversation tone.",
										"subCat": []
									},
									{
										"text": "It's the sound notification for the new message.",
										"subCat": []
									}
								]
							},
							{
								"text": "The phone doesn't vibrate on receiving a new hike message while Vibration settings are turned ON.",
								"subCat": []
							},
							{
								"text": "Others"
							}
						]
					}
				]
			},
			{
				"text": "Profile Photo",
				"subCat": [
					{
						"text": "I'm facing an issue while uploading the profile photo.It goes till 75% but never completes.",
						"subCat": [
							{
								"text": "Please share a screenshot with us on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm not able to upload a profile photo while using hike on mobile data (2G/3G/4G)",
						"subCat": [
							{
								"text": "Please share a screenshot on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm not able to upload a profile photo while using hike on WiFi.",
						"subCat": [
							{
								"text": "Please help us by sharing a screenshot on the next screen",
								"subCat": []
							}
						]
					},
					{
						"text": "The profile photo quality is bad.",
						"subCat": [
							{
								"text": "Please share screenshots of the original photo and the screenshot of the profile photo on hike.",
								"subCat": []
							}
						]
					},
					{
						"url": "http://support.hike.in/entries/93478737-How-do-I-remove-My-Profile-Picture-",
						"subCat": [],
						"type": "weburl",
						"text": "How do I remove my profile photo?"
					},
					{
						"url": "http://support.hike.in/entries/95378668-How-do-I-delete-a-Profile-Picture-that-I-posted-earlier-",
						"subCat": [],
						"type": "weburl",
						"text": "How do I delete profile photo posted earlier?"
					},
					{
						"url": "http://support.hike.in/entries/95378768-How-do-I-configure-Privacy-Settings-for-my-Profile-Picture-",
						"subCat": [],
						"type": "weburl",
						"text": "How do I hide my profile photo from favorite contacts?"
					}
				]
			},
			{
				"active": false,
				"text": "Status Updates",
				"subCat": [
					{
						"text": "I'm not able to post a status update.",
						"subCat": [
							{
								"text": "Please go to device settings > Force stop and try again.",
								"subCat": [
									{
										"text": "Still not working.",
										"subCat": [
											{
												"text": "Please share the details on the next screen.",
												"subCat": []
											}
										]
									},
									{
										"text": "I'm seeing an error",
										"subCat": [
											{
												"text": "Please share the screenshot of the error on the next screen.",
												"subCat": []
											}
										]
									}
								]
							}
						]
					},
					{
						"text": "I'm not able to delete my status update. I'm getting an error message.",
						"subCat": [
							{
								"text": "Please share a screenshot on the next screen.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"active": true,
				"text": "Stickers",
				"subCat": [
					{
						"active": true,
						"text": "I'm seeing 'Retry' option every time I download stickers.",
						"subCat": [
							{
								"text": "Please restart your phone and then try to download stickers.",
								"subCat": [
									{
										"text": "Still facing this error after restarting? Please share a screenshot on the next screen.",
										"subCat": []
									}
								]
							}
						],
						"priority": "3"
					},
					{
						"active": false,
						"text": "Sticker recommendation",
						"subCat": [
							{
								"text": "My sticker recommendation is not working.",
								"subCat": [
									{
										"text": "Turn off and then turn on your sticker recommendation in hike settings Or Force stop and restart hike.",
										"subCat": [
											{
												"text": "Sticker recommendation still not working. Please share a screenshot of Sticker Settings (hike settings > chat settings > Sticker Settings) on the next screen.",
												"subCat": []
											}
										]
									}
								]
							},
							{
								"text": "I'm getting wrong and inappropriate sticker recommendations for some words.",
								"subCat": [
									{
										"text": "Please share the exact words for which you get wrong sticker suggestions and add a screenshot on the next screen.",
										"subCat": []
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "I'm seeing stickers in my phone gallery.",
						"subCat": [
							{
								"active": false,
								"text": "Please share a screenshot of the hike stickers you see in your phone gallery on the next screen.",
								"subCat": []
							}
						]
					}
				],
				"priority": "2"
			},
			{
				"active": false,
				"text": "Backup and Restore",
				"subCat": [
					{
						"active": false,
						"text": "Not creating backup/ Getting an error: backup failed.",
						"subCat": [
							{
								"active": false,
								"text": "Have you installed any RAM killer or app optimizer like the Clean master on your mobile phone?",
								"subCat": [
									{
										"active": false,
										"text": "Please share the details of the app.",
										"subCat": []
									}
								]
							},
							{
								"active": true,
								"text": "I'm not using any RAM killer",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I'm not able to restore my backup.",
						"subCat": [
							{
								"active": true,
								"text": "I'm getting an error 'Backup failed something went wrong'",
								"subCat": [
									{
										"text": "Please confirm if you have installed Cleanmaster app on your device."
									}
								]
							}
						]
					},
					{
						"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
						"subCat": [],
						"type": "weburl",
						"active": false,
						"text": "I want to change my phone device, how do I take a backup?"
					},
					{
						"text": "I'm re-signing up to my hike account, but the backup restore option is not appearing.",
						"subCat": []
					},
					{
						"url": "http://support.hike.in/entries/88935677-How-do-I-restore-a-chat-backup-from-a-Backup-File-",
						"subCat": [],
						"type": "weburl",
						"text": "How do I restore my backup file?"
					},
					{
						"text": "I deleted my hike account. I'm creating a new one. I had a backup file saved with me, but it is not getting restored as the backup restore option doesn't appear during sign up.",
						"subCat": []
					},
					{
						"active": true,
						"text": "I cannot delete my backup.",
						"subCat": [
							{
								"active": true,
								"text": "Please add a screenshot on the next screen.",
								"theme": "Home Screen",
								"subCat": [],
								"label": "Backup/Restore"
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Hike Calls",
				"subCat": [
					{
						"text": "I'm facing issues with hike to hike calls.",
						"subCat": [
							{
								"text": "I cannot hear the voice of my call partner clearly.",
								"subCat": [
									{
										"text": "Please share the phone device and OS version details for the caller and the receiver.",
										"subCat": []
									}
								]
							},
							{
								"text": "I'm hearing an echo.",
								"subCat": []
							},
							{
								"text": "I'm facing a lag in the conversation.",
								"subCat": []
							},
							{
								"text": "The volume is too low.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I'm not able to make hike Conference Calls with my friends.",
						"subCat": [
							{
								"text": "Please ensure that you are using  hike latest version available on the Play Store.",
								"subCat": [
									{
										"text": "Please note that conference call host needs to be on WiFi or 4G.",
										"subCat": [
											{
												"text": "Please share a brief description of the issue if the above tips don't help.",
												"subCat": [
													{
														"text": "Please share a brief description of the issue if the above tips don't help.",
														"subCat": []
													}
												]
											}
										]
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "Quality of the calls are very poor",
						"subCat": [
							{
								"text": "I'm facing this issue on 2G"
							},
							{
								"text": "I'm facing this issue on 3G"
							},
							{
								"text": "I'm facing this issue on WiFi"
							}
						]
					},
					{
						"active": true,
						"text": "Hike calls are not getting connected",
						"subCat": [
							{
								"text": "I'm facing this issue on 2G"
							},
							{
								"text": "I'm facing this issue on 3G"
							},
							{
								"text": "I'm facing this issue on WiFi"
							}
						]
					}
				]
			},
			{
				"text": "Last seen",
				"subCat": [
					{
						"text": "I'm seeing Incorrect Time Stamp for my contact on the top of the chat window.",
						"subCat": [
							{
								"text": "Please share the phone number of the contact with whom you are facing this issue and add a screenshot on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "When my contact is typing, it's showing last seen time instead of 'Online'",
						"subCat": [
							{
								"text": "Please share the phone number of the contact with whom you are facing this issue and add a screenshot on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "It's showing 'A while ago' for my contact.",
						"subCat": []
					},
					{
						"text": "Last seen is shown after some time for my contact. It's basically slow to upload.",
						"subCat": [
							{
								"text": "Please share the phone number of the contact with whom you are facing this issue and add a screenshot.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"text": "Timeline",
				"subCat": [
					{
						"text": "I'm seeing an unclear/blurred image for my photo updates on the Timeline.",
						"subCat": [
							{
								"text": "Please share the original image and the screenshot of the photo update on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm unable to see the pictures posted by my favorites",
						"subCat": [
							{
								"text": "Please share the favorite contact details with us on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm unable to delete any post from the timeline.",
						"subCat": [
							{
								"text": "Please do a long press on the post and tap on the option to delete the post.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"text": "Delete Account",
				"subCat": [
					{
						"text": "I'm unable to delete my hike account. I'm getting an error message.",
						"subCat": [
							{
								"text": "Please share a screenshot on the next screen.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Hidden Chats",
				"subCat": [
					{
						"text": "I'm unable to hide a chat",
						"subCat": [
							{
								"text": "Please share the mobile number of the hidden contact and any other relevant information on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"url": "http://support.hike.in/entries/40163340-How-do-I-unhide-a-Chat-",
						"subCat": [],
						"type": "weburl",
						"text": "How do I unhide a hidden chat?"
					},
					{
						"text": "I can't share any pictures from the gallery and/or forward a picture to hidden chat.",
						"subCat": []
					},
					{
						"url": "http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-#hidden",
						"subCat": [],
						"type": "weburl",
						"text": "I'm unable to find a hidden chat"
					},
					{
						"url": "http://support.hike.in/entries/40164720-How-do-I-know-if-I-ve-gotten-a-message-on-a-Hidden-Chat-",
						"subCat": [],
						"type": "weburl",
						"text": "I'm not getting notification for new messages in a hidden chat"
					},
					{
						"url": "http://support.hike.in/entries/43159734-How-do-I-change-my-password-",
						"subCat": [],
						"type": "weburl",
						"text": "I forgot my hidden chat password. How do I enter into hidden mode?"
					}
				]
			},
			{
				"active": false,
				"text": "Natasha",
				"subCat": [
					{
						"text": "Natasha bot is not sharing correct information. Please improve.",
						"subCat": [
							{
								"text": "Please share your comments and screenshots on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"url": "http://support.hike.in/entries/93169938-How-do-I-activate-Natasha-",
						"subCat": [],
						"type": "weburl",
						"text": "How to subscribe/unsubscribe to Natasha bot?"
					}
				]
			},
			{
				"active": false,
				"text": "News",
				"subCat": [
					{
						"active": false,
						"text": "I'm not getting News.",
						"subCat": [
							{
								"active": false,
								"text": "News feature is only available for Android OS version 4.1 and above.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "News is not working",
						"subCat": [
							{
								"text": "Please share your comments and screenshots on the next screen."
							}
						]
					},
					{
						"active": false,
						"text": "I am not getting News even after enabling it",
						"subCat": [
							{
								"text": "You might have blocked news in the past. Please unblock and re-enable to start getting it. Follow these steps to Unblock: 3-Dot Menu(Available on the top-right corner) >> Settings >> Privacy >> Blocked List >> Select News >> Save."
							}
						]
					},
					{
						"active": false,
						"text": "I'm seeing a blank screen on opening news.",
						"subCat": [
							{
								"text": "Please share your comments and add screenshots on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Error in a news article",
						"subCat": []
					},
					{
						"active": false,
						"text": "Full story redirects to other websites",
						"subCat": []
					},
					{
						"active": false,
						"text": "I'm not able to scroll through the news properly.",
						"subCat": [
							{
								"text": "Please share your comments and screenshots on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "News is showing up the in the incorrect category.",
						"subCat": [
							{
								"text": "Please share your comments and screenshots on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I am getting Ads in news",
						"subCat": []
					},
					{
						"active": false,
						"text": "News is not getting updated",
						"subCat": []
					},
					{
						"active": false,
						"text": "It only shows \"Fetching News\"",
						"subCat": []
					},
					{
						"active": false,
						"text": "I do not get notifications for News",
						"subCat": []
					},
					{
						"active": false,
						"text": "Image quality is Poor",
						"subCat": []
					},
					{
						"active": true,
						"text": "Photos take time to load",
						"subCat": []
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"text": "Group Chats",
				"subCat": [
					{
						"url": "http://support.hike.in/entries/95756038-How-do-I-share-the-Admin-rights-of-a-Group-Chat-",
						"subCat": [],
						"type": "weburl",
						"text": "How to make a member a group admin in the Group Chat?"
					},
					{
						"text": "For any comments on hike group chats, please share your feedback with us on the next screen",
						"subCat": []
					}
				]
			},
			{
				"text": "Walkie Talkie",
				"subCat": [
					{
						"url": "http://support.hike.in/entries/21762434-Walkie-Talkie",
						"subCat": [],
						"type": "weburl",
						"text": "How do I use hike Walkie Talkie?"
					},
					{
						"text": "The voice quality in the Walkie-Talkie message is poor.",
						"subCat": []
					},
					{
						"text": "For any other feedback on hike Walkie-Talkie, please share your comments on the next screen.",
						"subCat": []
					}
				]
			},
			{
				"text": "Issues while Emailing Conversations",
				"subCat": [
					{
						"text": "My hike is crashing while emailing the chats.",
						"subCat": [
							{
								"text": "Please remove the media in that chat and then try to email the Chat again.",
								"subCat": []
							}
						]
					},
					{
						"text": "For any further feedback on Email Conversation please leave your comments.",
						"subCat": []
					}
				]
			},
			{
				"text": "Contacts",
				"subCat": [
					{
						"url": "http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-",
						"subCat": [],
						"type": "weburl",
						"text": "I'm not able to see the names of the contacts I have added to my address book recently."
					},
					{
						"url": "http://support.hike.in/entries/94779718-I-cannot-see-my-contacts-on-hike-For-Xiaomi-phones-",
						"subCat": [],
						"type": "weburl",
						"text": "My contacts are not syncing in hike contact list and I'm using a Xiaomi device."
					},
					{
						"url": "http://support.hike.in/entries/96161427-I-cannot-see-my-contacts-on-hike-For-Lenovo-phones-",
						"subCat": [],
						"type": "weburl",
						"text": "My contacts are not syncing in hike contact list and I'm using a Lenovo device."
					},
					{
						"text": "The above contact sync options don't help.",
						"subCat": [
							{
								"text": "Please share the details on the next screen.",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm receiving messages from blocked contacts.",
						"subCat": [
							{
								"text": "Please share your number and the number from which you are receiving messages",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"text": "Stickey",
				"subCat": [
					{
						"text": "I don't see Stickey under my hike account settings",
						"subCat": [
							{
								"url": "http://support.hike.in/entries/95735838-How-to-use-Stickey-in-Xiaomi-Phones-",
								"subCat": [],
								"type": "weburl",
								"text": "I'm using a Xiaomi device."
							},
							{
								"text": "I'm using a Lenovo device",
								"subCat": []
							},
							{
								"text": "I'm using a Motorola device with android version 5.1",
								"subCat": []
							},
							{
								"text": "I'm using an Intex Aqua device",
								"subCat": []
							}
						]
					},
					{
						"text": "I'm not able to enable hike stickey.",
						"subCat": [
							{
								"url": "http://support.hike.in/entries/96153217-Why-am-I-not-able-to-see-Stickey-",
								"subCat": [],
								"type": "weburl",
								"text": "Please read this FAQ to know more."
							}
						]
					},
					{
						"text": "Stickey is not getting enabled for me. I have given all permissions to hike Stickey from phone settings and still not getting this enabled.",
						"subCat": []
					},
					{
						"text": "I'm not happy with the hike stickey accessibility permissions required for using hike Stickey.",
						"subCat": []
					},
					{
						"text": "Stickers are appearing like images when we are sharing them with friends via Stickey. They should look like stickers.",
						"subCat": []
					},
					{
						"text": "Increase the number of stickers we can share via hike Stickey. 5 is too less.",
						"subCat": []
					},
					{
						"text": "I need a sticker recommendation feature while using hike Stickey.",
						"subCat": []
					},
					{
						"text": "When I use Stickey it leaves a black circle.",
						"subCat": [
							{
								"text": "Please share a screenshot on the next screen.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Other Issues",
				"subCat": [
					{
						"text": "My is LED not blinking when receiving a new message?",
						"subCat": [
							{
								"text": "Please try to change the color of your LED notification from hike settings > Notifications > LED Notification > change the color",
								"subCat": []
							}
						]
					},
					{
						"text": "Still not working? Please share the details and screenshots on the next screen",
						"subCat": []
					},
					{
						"text": "I wish to turn OFF Notification related to joining of users.",
						"subCat": [
							{
								"text": "Go to hike settings > Notifications > Contact Joining hike > Turn this OFF",
								"subCat": [
									{
										"text": "Does not help, then share your comments and screenshots on the next screen",
										"subCat": []
									}
								]
							}
						]
					},
					{
						"text": "Others",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Games",
				"subCat": []
			},
			{
				"active": false,
				"text": "Hike Direct",
				"subCat": [
					{
						"active": false,
						"text": "My hike direct is not connecting",
						"subCat": [
							{
								"active": false,
								"text": "Please share the number with which you are trying to connect on the next screen",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "It's taking too much time to connect",
						"subCat": [
							{
								"text": "Please share the number with which you are trying to connect on the next screen"
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "All my chats are suddenly disappeared",
				"subCat": [
					{
						"active": true,
						"text": "Go to device settings > apps > hike > force stop. Now open hike and let us know what happens",
						"subCat": [
							{
								"text": "I got my chats back"
							},
							{
								"text": "I'm still not not seeing my chats"
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Hike is consuming too much space",
				"subCat": [
					{
						"active": false,
						"text": "I'm referring to disk storage space",
						"subCat": [
							{
								"text": "Any screenshots to indicate or show how much storage is hike taking?"
							}
						]
					},
					{
						"active": false,
						"text": "I'm referring to RAM usage",
						"subCat": [
							{
								"active": true,
								"text": "Any screenshots to indicate or show how much RAM is hike taking?",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Hike's Playstore updates are huge./APK size of hike is big",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Hike is consuming too much Battery",
				"subCat": [
					{
						"text": "Any screenshots to indicate or show how much battery hike is consuming?"
					}
				]
			},
			{
				"active": false,
				"text": "Free SMS",
				"subCat": [
					{
						"active": false,
						"text": "My free SMS are not delivered to my contacts",
						"subCat": [
							{
								"active": false,
								"text": "I'm sending SMS to hike contacts who are offline",
								"subCat": [
									{
										"active": true,
										"text": "Please share the number of the contact with whom the messages are not delivered and approximate date and time of the message on the next screen",
										"subCat": []
									}
								]
							},
							{
								"active": true,
								"text": "I'm sending SMS to contacts who are not on hike.",
								"subCat": [
									{
										"text": "Please share the number of the contact with whom the messages are not delivered and approximate date and time of the message on the next screen"
									}
								]
							}
						]
					},
					{
						"active": false,
						"text": "My Free SMS are not increasing",
						"subCat": [
							{
								"active": true,
								"text": "Free SMS will increase according to your hike usage. The more you use hike, the more free SMS you will get. However, if your SMS are still not increasing after using hike, please let us know.",
								"subCat": []
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "I'm getting an error while updating hike from Playstore",
				"subCat": [
					{
						"active": true,
						"text": "I'm seeing some error code on Playstore",
						"subCat": [
							{
								"text": "Please share the details of the error code in the next screen"
							}
						]
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"active": false,
				"text": "My hike account has been hacked",
				"subCat": [
					{
						"text": "Please let us know in details why you are suspecting that your hike account might be hacked on the next screen"
					}
				]
			},
			{
				"active": false,
				"text": "Cricket",
				"subCat": [
					{
						"active": true,
						"text": "I am not getting cricket updates",
						"subCat": [
							{
								"active": false,
								"text": "Cricket feature is only available for Android OS version 4.0 and above.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Cricket fact disappears before I can read it",
						"subCat": []
					},
					{
						"active": false,
						"text": "I am Stuck on the Cricket Fact screen",
						"subCat": [
							{
								"text": "Please mention your comments and add screenshots on the next screen"
							}
						]
					},
					{
						"active": false,
						"text": "I'm seeing a blank screen on opening cricket",
						"subCat": [
							{
								"text": "Please mention your comments and add screenshots on the next screen"
							}
						]
					},
					{
						"active": false,
						"text": "Scores are not updating/Delay in score updation",
						"subCat": []
					},
					{
						"active": false,
						"text": "I am getting Ads in Cricket",
						"subCat": []
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"active": false,
				"text": "Hike Daily",
				"subCat": [
					{
						"text": "I did not get hike daily today"
					},
					{
						"text": "The fact you shared is not correct"
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"active": false,
				"text": "Just For Laughs",
				"subCat": [
					{
						"active": false,
						"text": "i am not getting Just For Laughs",
						"subCat": [
							{
								"active": false,
								"text": "Just For Laughs feature is only available on Hike App version 4.1 and above.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I am getting Just For Laughs even after blocking it",
						"subCat": []
					},
					{
						"active": false,
						"text": "Just For Laughs is not funny",
						"subCat": []
					},
					{
						"active": false,
						"text": "Report offensive content",
						"subCat": []
					},
					{
						"active": false,
						"text": "Image quality is Poor",
						"subCat": []
					},
					{
						"active": false,
						"text": "Unable to download images",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Coupons",
				"subCat": [
					{
						"active": false,
						"text": "The coupon code shows invalid at the brand website",
						"subCat": [
							{
								"text": "Please mention your comments and add screenshots on the next screen"
							}
						]
					},
					{
						"active": false,
						"text": "The coupon was not accepted at the brand outlet",
						"subCat": [
							{
								"active": true,
								"text": "Please mention the brand name and details of the outlet you visited.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I'm getting an error while downloading a coupon",
						"subCat": []
					},
					{
						"active": false,
						"text": "Add more coupons",
						"subCat": []
					},
					{
						"text": "Others"
					}
				]
			},
			{
				"active": false,
				"text": "Match Up!",
				"subCat": [
					{
						"active": true,
						"text": "I don't have match up",
						"subCat": [
							{
								"active": true,
								"text": "Match Up feature is only available for Android OS version 4.4 and above.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "My number is being displayed in the Match Up profile",
						"subCat": []
					},
					{
						"active": false,
						"text": "I'm not able to change my profile picture on match up",
						"subCat": []
					},
					{
						"active": false,
						"text": "I get notifications saying I have a match but I don't see anything inside match up",
						"subCat": []
					},
					{
						"active": false,
						"text": "Match-up is not suggesting any profiles for match",
						"subCat": []
					},
					{
						"active": false,
						"text": "I cannot find how to delete my profile from match-up",
						"subCat": [
							{
								"active": true,
								"text": "To delete your match up account, locate \"Match Up!\" on the conversation screen, long press the chat and select \"Block and Delete\". You may also block it by tapping on \"Block\", available under the three dot menu inside match up.",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "I'm getting notifications from match-up even after blocking it",
						"subCat": []
					},
					{
						"active": false,
						"text": "I cannot see if my match has read the message i sent",
						"subCat": []
					},
					{
						"active": false,
						"text": "Others",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Recharge",
				"subCat": [
					{
						"active": true,
						"text": "Transaction Failed",
						"theme": "Payments",
						"subCat": []
					},
					{
						"active": false,
						"text": "Transaction Failed (txid 2)",
						"theme": "Payments",
						"subCat": [],
						"txid": 2
					}
				]
			}
		],
		"icon": "issue"
	},
	{
		"subCat": [
			{
				"active": false,
				"text": "Recharge",
				"theme": "Payments",
				"deepLinkId": "Recharge_Txn",
				"subCat": [
					{
						"subCat": [
							{
								"active": false,
								"text": "Unable to recharge/failing repeatedly",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Please check that you are selecting the valid amount for the respective operator. If number is ported, make sure that you select circle/operator manually. Recharges also fail due to issues/load at the operator’s end. We suggest that you wait for some time and retry later."
							},
							{
								"active": false,
								"text": "Amount deducted but recharge failed",
								"subCat": [],
								"nextScreen": "3",
								"desc": "If the recharge fails and amount is deducted, Hike refunds the entire amount of failed recharge back to your Hike Wallet within 48 hrs. If you have not paid through Hike wallet and it has been more than 48 hours since recharge failure, the bank will refund the amount within 3-10 working days in your bank account/card."
							},
							{
								"active": false,
								"text": "Status shows pending- when will I get recharge?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Please understand ‘Pending’ means we are awaiting for final status from your operator. It may take upto 48 hours to know the final status. We advise you not to initiate a second transaction in this case."
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						],
						"text": "Recharge Failed/Pending/Processing",
						"priority": "3",
						"theme": "Payments",
						"deepLinkId": "Recharge_Status",
						"active": false
					},
					{
						"active": false,
						"text": "Recharge successful but service not received",
						"subCat": [
							{
								"active": false,
								"text": "Status shows success but bill payment/recharge not received",
								"subCat": [],
								"nextScreen": "3",
								"desc": "The bill payment takes 3 days to be updated at the operator’s end. For recharges, please verify the plan whether you have received a special recharge benefit or not.  Please verify the status of the recharge with your service provider with the operator ID received."
							},
							{
								"active": false,
								"text": "Wrong benefit received",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Kindly confirm the benefit by referring to recharge plan for respective operator and circle. In case of incorrect benefit(s), please get in touch with the operator."
							},
							{
								"active": false,
								"text": "Recharged wrong number by mistake",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Unfortunately none of the mobile operators allow reversal/refund of a recharge once made. You may contact your Operator for further help with reverse recharge."
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						],
						"nextScreen": "0"
					},
					{
						"active": true,
						"text": "Refunds",
						"subCat": [
							{
								"active": false,
								"text": "Refund not received",
								"subCat": [],
								"nextScreen": "3",
								"desc": "All bank refunds are credited back to your bank account within 3-10 business days. If the amount is not credited in your account within this time frame then we request you to please check with your bank."
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						],
						"nextScreen": "1",
						"priority": "4"
					},
					{
						"active": false,
						"text": "My query is not listed here",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Add / Transfer / Request Money",
				"theme": "Payments",
				"subCat": [
					{
						"active": true,
						"text": "Add Money",
						"subCat": [
							{
								"active": true,
								"text": "How can I add money to my Hike wallet?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. You can add money to your Hike Wallet by selecting the Add Money tab on homepage.<br/> 2. You can choose to add money via UPI, Credit/Debit card or Net Banking. <br/> 3. Enter the amount you want to add and tap on “Add Money”.<br/> 4. You will be redirected to a secure payment page where you need to provide payment details. <br/>5. If you have any saved credit or debit card, it will be shown upfront and you just need to enter CVV and proceed to bank page. If you do not have any saved card you can choose to save a credit or debit card for faster payment next time. 6. Once you enter details on your bank’s page & complete the transaction, you will be redirected to Hike with money added to your Hike wallet"
							},
							{
								"active": false,
								"text": "Are there any limitations to add money to Hike Wallet?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "If you are a Non-KYC customer you can add upto Rs.20,000 in a calendar month to your Wallet.  If you are a KYC customer you can add any amount as long as your wallet balance does not exceed Rs.1,00,000 at any point in time."
							},
							{
								"active": false,
								"text": "My money has been debited but it is not reflecting in Wallet, why?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "At Hike, your money is yours. If ever an amount is deducted from your bank/card but the payment or transaction is not successful, we will refund the amount to your card/bank the moment we get a confirmation from your bank. Refunds are credited to your bank/card usually in 3 – 5 working days, however, sometimes your bank can take upto 10 working days. If the amount is not credited in your account within this time frame, then we request you to please check with your bank. If your bank is unable to share your refund status, then please raise a ticket along with your latest bank statement from the date of transaction till date."
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Send Money",
						"subCat": [
							{
								"active": false,
								"text": "How can I send money to someone using Hike?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. Open the Hike Pay App. 2. Tap the Pay or Send icon. 3. Enter the phone number (You may select the recipient from your contact list). 4. Enter the amount you want to send.  5. Tap Send. 6. Select payment mode. 7. Submit"
							},
							{
								"active": false,
								"text": "I have sent money to a friend who does not have Hike Wallet, how will he/she receive money?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "An SMS is sent to the receiver asking them to download Hike. Once they activate, a request from the receiver will be sent to you."
							},
							{
								"active": false,
								"text": "What if the transfer fails?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "In case the transfer fails, the amount shall be refunded back to your Hike wallet."
							},
							{
								"active": false,
								"text": "How can I transfer money from Hike wallet to bank account?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. Open the Hike App. 2. Tap the Pay or Send icon. 3. Tap the Send to Bank option. 4. Enter the Name you want to give to this beneficiary. 5. Enter the account number. 6. Enter the IFSC code of the branch. You can get the IFSC code by tapping the Get IFSC and selecting your bank and branch. 7. Enter the amount you want to send.  8. Tap Send. 9. Your money will be successfully transferred to the bank account."
							},
							{
								"active": false,
								"text": "What are the charges to send money to bank?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "There is no fee on bank transfers at the moment."
							},
							{
								"active": false,
								"text": "How much amount can I transfer from my Wallet to Bank?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "You can transfer upto Rs.20000 in a calendar month. The maximum limit for transfer per transaction is Rs.5000."
							},
							{
								"active": false,
								"text": "How can I transfer money from Hike wallet to any VPA?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. Open the Hike Pay App. 2. Tap the Pay or Send icon. 3. Tap on VPA tab. 4. Select the VPA account (you may add a new VPA by tapping on \"\"Add new VPA\"\" 4. Enter the amount you want to send.  5. Tap Send. 6. Select payment mode. 7. Submit"
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						]
					},
					{
						"active": false,
						"text": "Request Money",
						"subCat": [
							{
								"active": false,
								"text": "How can I request for money?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. Open the Hike Pay App 2. Go to \"Profile\" section. 3. Tap on \"Hike Pay\". 4. Tap on \"Request\" icon. 5. Enter either one of the following, Mobile Number, VPA or Account of the person from whom you want to request money. 6. Enter the amount you want to request. 7. Your friend will receive a Request money notification."
							},
							{
								"active": false,
								"text": "How much amount can I request?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "If you are a Non-KYC customer you can request upto Rs.20,000 in a calendar month to your Wallet.  If you are a KYC customer you can request any amount as long as your wallet balance does not exceed Rs.1,00,000 at any point in time."
							},
							{
								"active": false,
								"text": "When will my request expire?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "The requests are valid for 30 days from the date of initiation."
							},
							{
								"active": false,
								"text": "Where can I see my request status?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "You may see the list of all successful and pending transactions in the transaction history. Just tap on \"Wallet\" icon and go to transaction history."
							},
							{
								"text": "My query is not listed here"
							}
						]
					},
					{
						"active": false,
						"text": "My query is not listed here",
						"subCat": []
					}
				]
			},
			{
				"active": true,
				"text": "My Account",
				"subCat": [
					{
						"active": false,
						"text": "UPI",
						"deepLinkId": "UPI_Onboard",
						"subCat": [
							{
								"active": false,
								"text": "How do I set up my UPI account",
								"subCat": [],
								"nextScreen": "3",
								"desc": "1. Go to settings 2. Verify your phone number 3. Select your bank 4. We will find the linked bank account and link it."
							},
							{
								"active": false,
								"text": "What is VPA",
								"subCat": [],
								"nextScreen": "3",
								"desc": "VPA- is a Virtual Payment Address which uniquely links a user's identity with his/her bank a/c. For instance, the VPA for Hike customers is in the format xyz@ybl  You can just share your VPA with any merchant site and it can make a payment request to you. You can also send money to anyone by using just their VPA instead of their mobile number. There is no need to share private and confidential information like mobile number, bank account number/ IFSC code, etc."
							},
							{
								"active": false,
								"text": "How are you getting all my bank a/c information?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "This is a feature of the UPI payment platform (built by NPCI- an RBI regulated entity). The UPI platform retrieves the accounts details linked with your mobile number in a masked manner- i.e. PhonePe can't see all the details. This exchange is done over secure banking networks and we don't store or ever use it."
							},
							{
								"active": false,
								"text": "Can I link multiple bank accounts with Hike Pay?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Yes, you can. However, at any point of time, only 1 bank account can be the default with your Hike Pay account. If money is transferred to you using your mobile number then it will be credited into your default account."
							},
							{
								"active": false,
								"text": "What is UPI PIN?",
								"subCat": [],
								"nextScreen": "3",
								"desc": "UPI PIN (Unified Payment Interface) is a 4-6 digit secret code issued by your bank when you sign up for mobile banking. This UPI PIN is used to authenticate all fund transfers made via mobile banking over the IMPS or UPI platform."
							},
							{
								"active": false,
								"text": "How do I de-register UPI",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Go to wallet settings -> Tap on \"Unlink UPI\"."
							},
							{
								"active": false,
								"text": "My query is not listed here",
								"subCat": []
							}
						]
					},
					{
						"subCat": [],
						"text": "Why is my account locked?",
						"nextScreen": "3",
						"priority": "3",
						"active": false,
						"desc": "For your safety and security, Hike may temporarily block your account upon noticing any suspicious transactions or activity, or if your transactions are not as per Terms & Conditions listed on our website. If your account has been locked, please contact us for resolution."
					},
					{
						"active": false,
						"text": "Wallet settings",
						"deepLinkId": "Wallet_settings",
						"subCat": [
							{
								"active": false,
								"text": "How to save debit card or credit card details",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Go to wallet settings -> tap on \"Saved cards\" -> tap on \"Add a new card\" -> Enter card details -> Tap \"Save\""
							},
							{
								"active": false,
								"text": "How to delete saved card details",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Go to wallet settings -> tap on \"Saved cards\" -> tap on \"delete\" button in front of the card you wish to delete."
							},
							{
								"subCat": [],
								"text": "How to link your bank account",
								"nextScreen": "3",
								"priority": "3",
								"active": true,
								"desc": "Go to wallet settings -> tap on \"Linked bank accounts\" -> tap on \"Add bank details\" -> Enter bank details -> Tap \"Save\""
							},
							{
								"active": false,
								"text": "How to delete linked bank account",
								"subCat": [],
								"nextScreen": "3",
								"desc": "Go to wallet settings -> tap on \"Linked bank accounts\" -> tap on \"delete\" button in front of the account you wish to delete."
							},
							{
								"text": "My query is not listed here"
							}
						]
					},
					{
						"active": false,
						"text": "My query is not listed here",
						"subCat": []
					}
				],
				"priority": "4"
			},
			{
				"active": false,
				"text": "Privacy and Security",
				"subCat": [
					{
						"active": false,
						"text": "What should I know about e-mail frauds",
						"subCat": [],
						"nextScreen": "3",
						"desc": "Be aware of any email message that requests personal data—such as passwords, or sends you to a web site that asks for such information. This practice is referred as Phishing, which is fraudulent communication designed to deceive consumers into divulging personal, financial or account information. These websites or emails may involve the illegal practice of “spoofing”, or forging a website or email address to resemble another, legitimate address and business. Remember to never enter your credentials over such websites or send personal information via an email. If you come across such websites or receive such emails, please contact Hike immediately."
					},
					{
						"active": false,
						"text": "What should I know about phone frauds",
						"subCat": [],
						"nextScreen": "3",
						"desc": "It’s important for consumers to know that Hike will not call customers to request their personal account information or any credit card, debit card or net banking details. Hike do not initiate outbound telemarketing calls. Consumers should not respond to any phone calls with requests for any such information and are advised to immediately report the situation to local law enforcement as well as the concerned financial institution."
					},
					{
						"active": false,
						"text": "What to do if I suspect fraud",
						"subCat": [],
						"nextScreen": "3",
						"desc": "If your credit card, debit card or bank account is involved, immediately contact the bank or financial institution that issued your card."
					},
					{
						"active": false,
						"text": "Is my credit/debit card information completely safe on Hike?",
						"subCat": [],
						"nextScreen": "3",
						"desc": "Millions of users are on Hike's platform. You can be rest assured that your online transactions at Hike are completely safe."
					},
					{
						"active": false,
						"text": "My query is not listed here",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Packets",
				"deepLinkId": "Packets_CS",
				"subCat": [
					{
						"subCat": [],
						"text": "What are packets",
						"nextScreen": "3",
						"priority": "1",
						"active": false,
						"desc": "Packets are a unique way of gifting/sending money to friends. You may send a packet to a single friend or in a group."
					},
					{
						"active": false,
						"text": "How do I send a packet",
						"subCat": [],
						"nextScreen": "3",
						"desc": "Just Tap on Packets, select the Theme and select a group or a contact, enter your Personalized Message, Amount and send a Packet. You can choose the number of people in the group who can receive it, and also whether you want to split the amount equally or randomly."
					},
					{
						"active": false,
						"text": "Not able to open the packet I received",
						"subCat": [],
						"nextScreen": "3",
						"desc": "When you receive a packet, it has to be redeemed within 48 hours. After 48 hours, the packet expires and you won't be able to redeem it. The remaining amount will be returned back to the sender."
					},
					{
						"active": true,
						"text": "My query is not listed here",
						"subCat": []
					}
				]
			},
			{
				"active": false,
				"text": "Supported devices",
				"subCat": [
					{
						"active": false,
						"text": "Supported devices and operating systems",
						"subCat": [],
						"nextScreen": "3",
						"desc": "Currently we support all devices with Android 4.4 and above. Hike Pay shall be available on iOS soon."
					},
					{
						"active": false,
						"text": "My query is not listed here",
						"subCat": []
					}
				],
				"priority": "1"
			},
			{
				"active": false,
				"text": "Others",
				"deepLinkId": "contact_us",
				"subCat": []
			}
		],
		"text": "Report a Hike Pay Issue",
		"theme": "Payments",
		"deepLinkId": "Wallet_Txn",
		"active": true,
		"type": "bug",
		"icon": "payIssue"
	},
	{
		"active": false,
		"text": "Reported issues",
		"subCat": [],
		"nextScreen": "5",
		"icon": "history"
	},
	{
		"subCat": [],
		"url": "http://www.hike.in/help/android",
		"text": "FAQs",
		"active": false,
		"type": "weburl",
		"icon": "faq"
	},
	{
		"subCat": [],
		"url": "http://www.twitter.com/hikestatus",
		"text": "System Health",
		"active": false,
		"type": "weburl",
		"icon": "system-health"
	},
	{
		"subCat": [],
		"url": "http://www.hike.in/terms/android",
		"text": "Terms and Conditions",
		"active": false,
		"type": "weburl",
		"icon": "tnc"
	},
	{
		"active": false,
		"text": "Rewards",
		"subCat": [
			{
				"active": false,
				"text": "Recharge Failed/Pending/Processing",
				"subCat": [
					{
						"active": false,
						"text": "Unable to recharge/failing repeatedly",
						"subCat": [
							{
								"text": "Please check that you are selecting the appropriate valid amount for the respective operator. If number is ported, make sure that you select circle/operator manually."
							},
							{
								"text": "Recharges also fail due to issues/load at the operator’s end. We suggest that you wait for some time and retry later"
							}
						]
					},
					{
						"active": false,
						"text": "Amount Deducted but recharge failed",
						"subCat": [
							{
								"text": "If the recharge fails and amount is deducted, hike will refund the entire amount of failed recharge back to your hike Wallet within 48 hrs. If you have not paid through hike wallet and it has been more than 48 hours since recharge failure, the bank will refund the amount within 3-10 working days in your bank account/card."
							}
						]
					},
					{
						"active": false,
						"text": "Status Shows Pending/Processing- Customer wants to cancel",
						"subCat": [
							{
								"text": "If you see the Status as Pending/Processing then you are allowed to cancel a transaction after 2 hrs from the last transaction’s attempt time."
							}
						]
					},
					{
						"active": true,
						"text": "Status shows Pending- when will I get recharge?",
						"subCat": [
							{
								"text": "If the recharge status shows ‘Pending’ then it might take 2 hrs for recharge to show as ‘Successful’. Please understand ‘Pending’ means Pending at operator’s end. We advise you not to initiate a 2nd transaction in this case."
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Recharge Successful but Service not received",
				"subCat": [
					{
						"active": false,
						"text": "Status shows success but recharge not received",
						"subCat": [
							{
								"text": "The bill payment takes 3 days to be updated at the operator’s end."
							},
							{
								"text": "In case of Recharge, please verify the plan whether you have received a special recharge benefit or not. If not, then please write to us at Support@hike.in."
							}
						]
					},
					{
						"active": true,
						"text": "Wrong Benefit Received",
						"subCat": [
							{
								"text": "Kindly confirm the benefit by referring to recharge plan for respective operator and circle. In case of incorrect benefit(s), please get in touch with the operator."
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Refund/Payment issue",
				"subCat": [
					{
						"active": false,
						"text": "Amount deducted but Recharge failed",
						"subCat": [
							{
								"text": "If the recharge fails and amount is deducted, hike refunds the entire amount of failed recharge back to your hike Wallet within 48 hrs. If you have not paid through hike wallet and it has been more than 48 hours since recharge failure, the bank will refund the amount within 3-10 working days in your bank account/card."
							}
						]
					},
					{
						"active": false,
						"text": "Refund not received",
						"subCat": [
							{
								"text": "All bank refunds are credited back to your bank account within 3-10 working days."
							},
							{
								"text": "If the amount is not credited in your account within this time frame then we request you to please check with your bank."
							},
							{
								"text": "If bank is unable to share you refund status then please contact us at Support@hike.in along with your latest bank statement from the date of transaction."
							}
						]
					}
				]
			},
			{
				"active": false,
				"text": "Other Issues",
				"subCat": [
					{
						"active": false,
						"text": "Wrong Benefit Received",
						"subCat": [
							{
								"text": "Kindly confirm the benefit by referring to recharge plan for respective operator and circle. In case of incorrect benefit(s), please get in touch with the operator."
							}
						]
					},
					{
						"active": true,
						"text": "Recharged wrong number by mistake",
						"subCat": [
							{
								"text": "Please contact your Operator for further help with reverse recharge."
							}
						]
					}
				]
			}
		]
	},
	{
		"active": false,
		"text": "Email CEO",
	 	"desc": "This is a dummy hardcoded description and later it will be replaced by the server side",
		"subCat": [],
		"nextScreen": "7"
	}
]




/* prod data.js */
// window.catData = [
// 	{
// 		"active": false,
// 		"type": "Bug",
// 		"text": "Report an Issue",
// 		"subCat": [
// 			{
// 				"active": false,
// 				"text": "Video Story",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Stuck in sending Video Story",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Stuck in sending Video Story"
// 					},
// 					{
// 						"active": false,
// 						"text": "Video Story upload failed",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Video Story upload failed"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Story posting",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Failed story post error",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Story post failed"
// 					},
// 					{
// 						"active": false,
// 						"text": "Stuck in story sending",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Stuck in story sending"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Story receiving",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Failed story received",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Failed story received"
// 					},
// 					{
// 						"active": false,
// 						"text": "When I tap on a story it doesn't open or disappears in a flash of a second",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Animation settings issue"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Story receiving others"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "New chat design",
// 				"subCat": []
// 			},
// 			{
// 				"active": false,
// 				"text": "Camera",
// 				"theme": "HomeScreen5.0",
// 				"subCat": [],
// 				"label": "Camera"
// 			},
// 			{
// 				"active": false,
// 				"text": "Live face filters",
// 				"theme": "HomeScreen5.0",
// 				"subCat": [],
// 				"label": "Live Filters"
// 			},
// 			{
// 				"active": false,
// 				"text": "Notifications",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm not getting any notification for a new message until I open the app.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I'm using a Samsung J5/J7 or similar device",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604867-Optimizing-message-receive-experience-for-your-Samsung-J5-J7-device",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Samsung S6, Galaxy S6 Edge Plus, Note Edge",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604827-Optimizing-message-receive-experience-for-your-Samsung-S6-Galaxy-S6-Edge-Plus-Note-Edge-device",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Xiaomi Phone",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"subCat": [],
// 										"type": "weburl",
// 										"url": "https://support.hike.in/hc/en-us/articles/230604807-Optimizing-message-receive-experience-on-my-Xiaomi-Phone-For-MIUI-7-",
// 										"text": "I'm using hike on Xiaomi MiUI 7"
// 									},
// 									{
// 										"active": false,
// 										"subCat": [],
// 										"type": "weburl",
// 										"url": "https://support.hike.in/hc/en-us/articles/230604727",
// 										"text": "I'm using hike on Xiaomi MiUi 5."
// 									},
// 									{
// 										"active": false,
// 										"subCat": [],
// 										"type": "weburl",
// 										"url": "https://support.hike.in/hc/en-us/articles/230604687-Optimizing-message-receive-experience-on-your-Xiaomi-Phone-MIUI-6-",
// 										"text": "I'm using hike on Xiaomi MiUi 6."
// 									},
// 									{
// 										"url": "http://support.hike.in/entries/98675887-How-do-I-know-my-MiUI-version-",
// 										"subCat": [],
// 										"type": "weburl",
// 										"active": false,
// 										"text": "I don't know my MiUI Version"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Asus Phone",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604767-Optimizing-message-receive-experience-experience-on-Asus",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using an Oppo device.",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604887-Optimizing-message-receive-experience-on-your-Oppo-phone",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Vivo Phone",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604907-Optimizing-message-receive-experience-on-your-Vivo-phone",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Lenovo Phone",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604787-Optimizing-message-receive-experience-on-your-Lenovo-phone",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Meizu Phone",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604747-Enable-notifications-for-MEIZU-Phones",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Huawei Phone",
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230604707-Optimizing-message-receive-experience-on-your-Huawei-Phones",
// 								"type": "weburl"
// 							},
// 							{
// 								"active": true,
// 								"text": "I have given necessary permissions to hike from phone settings and still not getting any notification for an incoming message.",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Push notification"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a rooted device.",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "If you have installed a Custom ROM, please share the OS version details on the next screen.",
// 										"theme": "CPR",
// 										"subCat": [],
// 										"label": "Notification Issue"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using Clean Master or similar apps on my mobile phone",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please share the details of the app on the next screen.",
// 										"theme": "CPR",
// 										"subCat": [],
// 										"label": "Push notification"
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": true,
// 						"text": "I'm facing issues with Notification Sounds",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I don't get any sound notification for an incoming message while listening to music.",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Are you listening to FM radio?",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Notification Sound"
// 									},
// 									{
// 										"active": true,
// 										"text": "Are you listening to a music player?",
// 										"subCat": [
// 											{
// 												"active": false,
// 												"text": "Please share music player details on the next screen.",
// 												"theme": "CPR",
// 												"subCat": [],
// 												"label": "Sound notification"
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "On receiving any new hike message the music playing in the background stops.",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Music Player Issue"
// 							},
// 							{
// 								"active": false,
// 								"text": "Phone goes into Silent mode after using hike for a while. I have to force kill the app to resume to its default sound settings",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Phone switched to silent mode"
// 							},
// 							{
// 								"active": false,
// 								"text": "There is a continuous sound playing in the background on sending a message.",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "It's the conversation tone.",
// 										"theme": "CPR",
// 										"subCat": [],
// 										"label": "Continuous notification"
// 									},
// 									{
// 										"active": true,
// 										"text": "It's the sound notification for the new message.",
// 										"theme": "CPR",
// 										"subCat": [],
// 										"label": "Continuous notification"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The phone doesn't vibrate on receiving a new hike message while Vibration settings are turned ON.",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Sound notification"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Sound notification"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I don’t want my friends to be notified when I re-install the app saying “xyz has joined hike. Say hi”",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "I don't want RUJ"
// 					},
// 					{
// 						"active": false,
// 						"text": "There are too many unnecessary notifications. Give us settings to disable some notifications",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "There are too many unnecessary notifications. Give us settings to disable some notifications"
// 					},
// 					{
// 						"active": false,
// 						"text": "The badge count for unread messages on hike logo is not in sync with the actual count",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Notifications"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Message delivery is Slow / Not working",
// 				"theme": "ChatExperience",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Sending messages to friends on hike",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Slowness from changing from ✓ to ✓✓",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Slow with ALL my friends",
// 										"label": "Messaging latency",
// 										"theme": "CPR",
// 										"logkey": "mqtt",
// 										"active": false
// 									},
// 									{
// 										"active": true,
// 										"text": "Slow with 1-2 friends in Specific",
// 										"subCat": [
// 											{
// 												"active": false,
// 												"text": "There is a very high chance that if your friend is on certain Android devices (Asus, Xiaomi, Lenovo, Huawei etc) that s/he will need to do something via the respective FAQ that we will share with you and then the problem should solve itself.",
// 												"subCat": [],
// 												"nextScreen": "0"
// 											},
// 											{
// 												"active": false,
// 												"text": "My Friend is using a Xiaomi Phone",
// 												"subCat": [
// 													{
// 														"active": false,
// 														"text": "With MiUI7",
// 														"subCat": [],
// 														"type": "weburl"
// 													},
// 													{
// 														"active": false,
// 														"text": "With MiUI6",
// 														"subCat": [],
// 														"type": "weburl"
// 													},
// 													{
// 														"active": false,
// 														"text": "With MiUI5",
// 														"subCat": [],
// 														"type": "weburl"
// 													},
// 													{
// 														"active": true,
// 														"text": "I don't know how to check the MiUI Version",
// 														"subCat": [],
// 														"type": "weburl"
// 													}
// 												]
// 											},
// 											{
// 												"active": false,
// 												"text": "My Friend is using a Lenovo Phone",
// 												"theme": "CPR",
// 												"subCat": [],
// 												"label": "Messaging latency"
// 											},
// 											{
// 												"active": false,
// 												"text": "My Friend is using a Huawei Phone",
// 												"subCat": [],
// 												"type": "weburl"
// 											},
// 											{
// 												"active": false,
// 												"text": "MY Friend is using an Asus Phone",
// 												"subCat": [],
// 												"type": "weburl"
// 											},
// 											{
// 												"active": false,
// 												"text": "My Friend is using a Samsung Phone",
// 												"subCat": [],
// 												"type": "weburl"
// 											},
// 											{
// 												"subCat": [],
// 												"text": "Others",
// 												"label": "Message slow from tick to double tick",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": false
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "My messages are stuck on clock",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "My messages are stuck on Clock while I'm on WiFi.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please tell us which WiFi network are you using on the next screen.",
// 												"label": "Messaging latency",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "My messages are stuck on Clock while I'm on Mobile Data (2G/3G/4G).",
// 										"subCat": [
// 											{
// 												"subCat": [
// 													{
// 														"text": "Go to device settings > Wireless and Network > Mobile Networks > Access Point Names > change the APN settings and then connect to hike, it should work."
// 													}
// 												],
// 												"text": "Please tell us which network operator services are you using on the next screen.",
// 												"label": "Message Latency",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": false
// 											},
// 											{
// 												"active": true,
// 												"text": "I'm using Reliance Jio sim",
// 												"subCat": [
// 													{
// 														"text": "There are known issues from Reliance end and we are trying to get this resolved with them soon."
// 													}
// 												]
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "My messages are stuck on Clock on both WiFi and Mobile Data (2G/3G/4G).",
// 										"subCat": [
// 											{
// 												"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
// 												"subCat": [],
// 												"type": "weburl",
// 												"active": false,
// 												"text": "Have you signed up on a different device using the same number? You can restore data from one device, reset the account and sign up on the device you wish to."
// 											},
// 											{
// 												"subCat": [],
// 												"text": "I'm not signed up on two devices simultaneously.",
// 												"label": "Connectivity Issue",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "My messages are stuck on single tick (✓)",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "My messages are stuck on single tick (✓) while I'm on WiFi",
// 										"subCat": [
// 											{
// 												"active": false,
// 												"text": "Please share the number with which this happens frequently on the next screen.",
// 												"subCat": []
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "My messages are stuck on single tick (✓) while I'm on Mobile Data (2G/3G/4G).",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please tell us which network operator services are you using on the next screen.",
// 												"label": "Connectivity",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": false
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "My messages are stuck on double tick (✓✓) but not delivered.",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "My messages are stuck on double tick (✓✓) with my contacts who are on iOS.",
// 										"label": "iOS double tick hack",
// 										"theme": "CPR",
// 										"logkey": "mqtt",
// 										"active": false
// 									},
// 									{
// 										"active": true,
// 										"text": "My messages are stuck on double tick (✓✓) with all my contacts.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share some contact details with whom you are facing this issue on the next screen.",
// 												"label": "Double tick but not delivered",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": false
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Slowness from changing from Clock to ✓",
// 								"label": "Message Latency",
// 								"theme": "CPR",
// 								"logkey": "mqtt",
// 								"active": true
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Slowness from changing from ✓✓ to ✓✓R",
// 								"label": "Message Latency",
// 								"theme": "CPR",
// 								"logkey": "mqtt",
// 								"active": false
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Sending messages to friends not on hike",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "My messages are not delivered to friends who are not on hike.",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the mobile number of the contact with whom you are facing this issue on the next screen.",
// 										"label": "Free SMS not delivered",
// 										"theme": "CPR",
// 										"logkey": "mqtt",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "My messages are stuck in Clock state and I'm facing this on WiFi.",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please tell us which WiFi network are you using on the next screen.",
// 										"label": "Connectivity",
// 										"theme": "CPR",
// 										"logkey": "mqtt",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "My messages are stuck in Clock state and I'm facing this on mobile data(2G/3G/4G).",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please tell us which network operator services are you using on the next screen.",
// 										"label": "Connectivity",
// 										"theme": "CPR",
// 										"logkey": "mqtt",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"subCat": [],
// 								"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
// 								"text": "Have you signed up with the same number on a different device? You can restore data from one device, reset the account and sign up on the device you wish to.",
// 								"logkey": "mqtt",
// 								"active": true,
// 								"type": "weburl"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm seeing 'No internet Connection' But my internet is working fine.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I'm facing this issue only on WiFi",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
// 										"subCat": [],
// 										"nextScreen": "0"
// 									},
// 									{
// 										"active": false,
// 										"text": "test.hike.in",
// 										"subCat": [],
// 										"url": "http://test.hike.in",
// 										"type": "weburl"
// 									},
// 									{
// 										"active": false,
// 										"text": "I've checked test.hike.in and Everything is working fine.",
// 										"theme": "CPR",
// 										"subCat": [
// 											{
// 												"active": false,
// 												"text": "I'm using a Home WiFi",
// 												"subCat": [
// 													{
// 														"subCat": [],
// 														"text": "Please share your WiFi Provider details on the next screen",
// 														"label": "Connectivity",
// 														"theme": "CPR",
// 														"logkey": "mqtt",
// 														"active": true
// 													}
// 												]
// 											},
// 											{
// 												"active": true,
// 												"text": "I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)",
// 												"subCat": [
// 													{
// 														"subCat": [],
// 														"text": "Please share your WiFi Provider details on the next screen",
// 														"label": "Connectivity",
// 														"theme": "CPR",
// 														"logkey": "mqtt",
// 														"active": true
// 													}
// 												]
// 											}
// 										],
// 										"label": "Connectivity"
// 									},
// 									{
// 										"active": false,
// 										"text": "I'm seeing an error in test.hike.in",
// 										"subCat": [
// 											{
// 												"active": false,
// 												"text": "I'm using a Home WiFi",
// 												"subCat": [
// 													{
// 														"subCat": [],
// 														"text": "Please share a screenshot of the error and your WiFi Provider details on the next screen",
// 														"label": "Connectivity",
// 														"theme": "CPR",
// 														"logkey": "mqtt",
// 														"active": true
// 													}
// 												]
// 											},
// 											{
// 												"active": true,
// 												"text": "I'm using a Public WiFi (Like Office WiFi, College WiFi etc.)",
// 												"subCat": [
// 													{
// 														"subCat": [],
// 														"text": "Please share a screenshot of the error and your WiFi Provider details on the next screen",
// 														"label": "Connectivity",
// 														"theme": "CPR",
// 														"logkey": "mqtt",
// 														"active": true
// 													}
// 												]
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm facing this issue only on Mobile Data",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
// 										"subCat": [],
// 										"nextScreen": "0"
// 									},
// 									{
// 										"active": false,
// 										"text": "http://test.hike.in",
// 										"subCat": [],
// 										"url": "http://test.hike.in",
// 										"type": "weburl"
// 									},
// 									{
// 										"active": false,
// 										"text": "I've checked test.hike.in and Everything is working fine.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share your Service Provide/Operator details on the next screen",
// 												"label": "Connectivity",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "I'm seeing an error in test.hike.in",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share a screenshot of the error and your Service Provider/Operator details on the next screen",
// 												"label": "Connectivity",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"text": "I'm using a Reliance Jio sim"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm seeing this on Mobile Mobile Data and WiFi",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please on test.hike.in and ensure that \"Everything is working fine\"",
// 										"subCat": [],
// 										"nextScreen": "0"
// 									},
// 									{
// 										"active": false,
// 										"text": "http://test.hike.in",
// 										"subCat": [],
// 										"url": "http://test.hike.in",
// 										"type": "weburl"
// 									},
// 									{
// 										"active": false,
// 										"text": "I've checked test.hike.in and Everything is working fine.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share your Service Provide/Operator details on the next screen",
// 												"label": "Connectivity",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "I'm seeing an error in test.hike.in",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share a screenshot of the error and your Service Provider/Operator details on the next screen",
// 												"label": "Connectivity",
// 												"theme": "CPR",
// 												"logkey": "mqtt",
// 												"active": true
// 											}
// 										]
// 									}
// 								],
// 								"nextScreen": "0"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Opening a chat in hike is slow",
// 						"subCat": []
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "File Transfer Issues",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Sending anything is very slow and takes a long time to upload.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I'm sending from a Wi-Fi connection",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "Sending media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm sending from my 2G mobile connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Sending media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Sending both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm sending from my 3G mobile connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Sending media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Sending both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm sending from my 4G mobile connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Sending media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Sending both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Sending is slow on both Wi-Fi and Mobile Data (2G/3G/4G)",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Sending media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Sending non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Sending both media and non-media documents is slow.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Downloading anything is very slow and takes a long time to download.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I'm downloading from a Wi-Fi connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Downloading media (photos & videos) is slow.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Downloading both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm downloading from my 2G mobile connection",
// 								"theme": "CPR",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Downloading media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								],
// 								"label": "File Transfer"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm downloading from my 3G mobile connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Downloading media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Downloading both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm downloading from my 4G mobile connection",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Downloading media (photos & videos) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Downloading both media and non-media documents is slow",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true,
// 												"type": "Bug"
// 											}
// 										],
// 										"label": "File Transfer"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Downloading is slow on both Wi-Fi and Mobile Data (2G/3G/4G)",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Downloading media (photos & videos) is slow.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									},
// 									{
// 										"active": true,
// 										"text": "Downloading non-media documents (PDF, ZIP, Word, Excel etc) is slow.",
// 										"subCat": [
// 											{
// 												"active": true,
// 												"text": "Please share the details on next screen.",
// 												"subCat": [],
// 												"logkey": "ft",
// 												"label": "File Transfer"
// 											}
// 										]
// 									},
// 									{
// 										"active": false,
// 										"text": "Downloading both media and non-media documents is slow.",
// 										"subCat": [
// 											{
// 												"subCat": [],
// 												"text": "Please share the details on next screen.",
// 												"label": "File Transfer",
// 												"theme": "CPR",
// 												"logkey": "ft",
// 												"active": true
// 											}
// 										]
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm getting an error when uploading.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'Could not upload the file. Check Network Settings'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'Unable to read file'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'No app found that can handle this action'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'Unsupported file'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'Max file size can be 100 MB'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Another error message comes which is not shown above appears.",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share a screenshot of the error message on the next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm getting an error when downloading.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "The error message when downloading is 'Could not download the file. Check Network Settings'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when downloading is 'This file no longer exists'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when downloading is 'Your phone does not have enough space to download the file'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when downloading is 'Please insert an SD Card'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "The error message when uploading is 'Max file size can be 100 MB'",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share the screenshot and other details on next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							},
// 							{
// 								"active": true,
// 								"text": "Another error message comes which is not shown above appears.",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "Please share a screenshot of the error message on the next screen.",
// 										"label": "File Transfer",
// 										"theme": "CPR",
// 										"logkey": "ft",
// 										"active": true
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"text": "Takes a lot of time to open the gallery after clicking it to choose a photo to upload.",
// 						"label": "Taking time to open gallery",
// 						"theme": "ChatExperience",
// 						"logkey": "ft",
// 						"active": false
// 					},
// 					{
// 						"active": false,
// 						"text": "The quality of the photo is poor and pixelated after I send it to my friend.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I shared the photo from inside chat thread > Attachments > Gallery",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "I choose 'Compressed' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									},
// 									{
// 										"active": false,
// 										"text": "I choose 'Normal' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									},
// 									{
// 										"active": false,
// 										"text": "I choose 'Original' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I shared the photo from inside chat thread > Attachments > Camera",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "I choose 'Compressed' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									},
// 									{
// 										"active": false,
// 										"text": "I choose 'Normal' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									},
// 									{
// 										"active": true,
// 										"text": "I choose 'Original' option",
// 										"theme": "ChatExperience",
// 										"subCat": [],
// 										"label": "Image shared is pixelated"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I shared the photo from outside hike > Phone photo gallery > Share via hike",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Image shared is pixelated"
// 							},
// 							{
// 								"subCat": [],
// 								"text": "The photos I receive from my friend get black or colour stripped out after I download it.",
// 								"label": "Image quality issue",
// 								"theme": "CPR",
// 								"logkey": "ft",
// 								"active": true
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Not able to download PDF",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "I am not able to download any PDF received on hike",
// 								"label": "I am not able to download any PDF received on hike",
// 								"theme": "CPR",
// 								"logkey": "ft",
// 								"active": true
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"text": "When I exit the app the file transfer stops in the background",
// 						"label": "When I exit the app the file transfer stops in the background",
// 						"theme": "CPR",
// 						"logkey": "ft",
// 						"active": false
// 					},
// 					{
// 						"subCat": [],
// 						"text": "Others",
// 						"label": "File Transfer",
// 						"theme": "CPR",
// 						"logkey": "ft",
// 						"active": false
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "I'm seeing duplicate chats on my home screen.",
// 				"theme": "CPR",
// 				"subCat": []
// 			},
// 			{
// 				"active": false,
// 				"text": "Typing issue",
// 				"theme": "ChatExperience",
// 				"subCat": [],
// 				"label": "typingissue"
// 			},
// 			{
// 				"active": false,
// 				"text": "Video Call",
// 				"theme": "VOIP&Video",
// 				"subCat": [
// 					{
// 						"text": "Please describe your issue in detail."
// 					}
// 				],
// 				"label": "video call"
// 			},
// 			{
// 				"active": false,
// 				"text": "Voice Call",
// 				"theme": "VOIP&Video",
// 				"subCat": [
// 					{
// 						"text": "Please describe your issue in detail."
// 					}
// 				],
// 				"label": "voice"
// 			},
// 			{
// 				"active": false,
// 				"text": "Link Sharing",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "It takes too long for the link preview to load",
// 						"theme": "DevX",
// 						"subCat": [],
// 						"label": "Link Previews take time to load"
// 					},
// 					{
// 						"active": false,
// 						"text": "The link previews do not load for me",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I typed the link in the text box",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "I am using 2G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 2G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using 3G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 3G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using 4G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 4G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using Wifi",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on Wifi"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I copied and pasted the link in the text box",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "I am using 2G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 2G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using 3G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 3G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using 4G",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on 4G"
// 									},
// 									{
// 										"active": false,
// 										"text": "I am using Wifi",
// 										"theme": "DevX",
// 										"subCat": [],
// 										"label": "Link previews not loading on Wifi"
// 									}
// 								]
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "All my chats have suddenly disappeared",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Go to device settings > apps > hike > force stop. Now open hike and let us know what happens",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I got my chats back",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Chats disappearing"
// 							},
// 							{
// 								"subCat": [],
// 								"text": "I'm still not not seeing my chats",
// 								"label": "Chats disappearing",
// 								"theme": "CPR",
// 								"logkey": "mqtt",
// 								"active": true
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Profile Photo",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm facing an issue while uploading the profile photo.It goes till 75% but never completes.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Please share a screenshot with us on the next screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Profile photo"
// 							}
// 						]
// 					},
// 					{
// 						"active": true,
// 						"text": "I'm not able to upload a profile photo while using hike on mobile data (2G/3G/4G)",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share a screenshot on the next screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Profile photo"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not able to upload a profile photo while using hike on WiFi.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please help us by sharing a screenshot on the next screen",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Profile photo"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "The profile photo quality is bad.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share screenshots of the original photo and the screenshot of the profile photo on hike.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Profile photo"
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/93478737-How-do-I-remove-My-Profile-Picture-",
// 						"text": "How do I remove my profile photo?",
// 						"label": "Remove DP",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/95378668-How-do-I-delete-a-Profile-Picture-that-I-posted-earlier-",
// 						"text": "How do I delete profile photo posted earlier?",
// 						"label": "Delete earlier DP",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/95378768-How-do-I-configure-Privacy-Settings-for-my-Profile-Picture-",
// 						"text": "How do I hide my profile photo from favorite contacts?",
// 						"label": "Hide DP",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Stickers",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm seeing 'Retry' option every time I download stickers.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please restart your phone and then try to download stickers.",
// 								"theme": "ExpressYourself",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Still facing this error after restarting? Please share a screenshot on the next screen.",
// 										"theme": "ExpressYourself",
// 										"subCat": [],
// 										"label": "Sticker retry"
// 									}
// 								],
// 								"label": "Sticker retry"
// 							}
// 						]
// 					},
// 					{
// 						"active": true,
// 						"text": "Sticker recommendation",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "My sticker recommendation is not working.",
// 								"theme": "ExpressYourself",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Turn off and then turn on your sticker recommendation in hike settings Or Force stop and restart hike.",
// 										"subCat": [
// 											{
// 												"active": true,
// 												"text": "Sticker recommendation still not working. Please share a screenshot of Sticker Settings (hike settings > chat settings > Sticker Settings) on the next screen.",
// 												"theme": "ExpressYourself",
// 												"subCat": [],
// 												"label": "Sticker recommendation"
// 											}
// 										]
// 									}
// 								],
// 								"label": "Sticker recommendation"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm getting wrong sticker recommendations for some words.",
// 								"theme": "ExpressYourself",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please share the exact words for which you get wrong sticker suggestions and add a screenshot on the next screen.",
// 										"theme": "ExpressYourself",
// 										"subCat": [],
// 										"label": "Sticker Recommendation"
// 									}
// 								],
// 								"label": "Sticker recommendation"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm seeing stickers in my phone gallery.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share a screenshot of the hike stickers you see in your phone gallery on the next screen.",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Sticker in gallery"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Sticker loads slowly when I recieve them in a chat",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "Sticker loads slowly when I recieve them in a chat"
// 					},
// 					{
// 						"active": false,
// 						"text": "Stickers are consuming memory, I'm running out of Storage Space on my phone",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "Stickers are consuming memory, I'm running out of Storage Space on my phone"
// 					},
// 					{
// 						"active": false,
// 						"text": "While scrolling stickers are sent accidently",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "While scrolling stickers are sent accidently"
// 					},
// 					{
// 						"active": false,
// 						"text": "My downloaded stickers are deleted and I need to redownload them.",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "My downloaded stickers are deleted and I need to redownload them."
// 					},
// 					{
// 						"active": false,
// 						"text": "I want to report an offensive sticker",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "I want to report an offensive sticker"
// 					},
// 					{
// 						"active": false,
// 						"text": "Stickers are too big in Chat conversation",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "Stickers are too big in Chat conversation"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Match Up!",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I don't have Match Up feature",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Why I am not able to get the Match Up feature in my hike account",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "Match Up feature is only available for Android OS version 4.4 and above.",
// 										"theme": "Growth",
// 										"subCat": [],
// 										"label": "Why I am not able to get the Match Up feature in my hike account"
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "My number is being displayed in the Match Up profile",
// 						"theme": "Growth",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "My number is being displayed to others on the match up profile",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "My number is being displayed to others on the match up profile"
// 							}
// 						],
// 						"label": "My number is being displayed in the Match Up profile"
// 					},
// 					{
// 						"active": false,
// 						"text": "Not able to change my profile pic",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I'm not able to change my profile picture on Match Up",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Unable to Change profile picture on Match Up"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Notification",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I get notifications saying I have a match but I don't see anything inside Match Up",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "I get notifications saying I have a match but I don't see anything inside Match Up"
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm getting notifications from my match even after blocking the person",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "I'm getting notifications from my match even after blocking the person"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Match-up is not suggesting any profiles for match",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Match-up is not suggesting any profiles for match"
// 					},
// 					{
// 						"active": false,
// 						"text": "Delete my Profile",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I cannot find how to delete my profile from Match Up",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "To delete your match up account, locate \"Match Up!\" on the conversation screen, long press the chat and select \"Block and Delete\". You may also block it by tapping on \"Block\", available under the three dot menu inside match up.",
// 										"theme": "Growth",
// 										"subCat": [],
// 										"label": "I cannot find how to delete my profile from Match Up"
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm getting notifications from match-up even after blocking it",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Block not working for match up"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Other Match Up related Issues"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Backup and Restore",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Not creating backup/ Getting an error: backup failed.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Have you installed any RAM killer or app optimizer like the Clean master on your mobile phone?",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please share the details of the app.",
// 										"theme": "CPR",
// 										"subCat": [],
// 										"label": "Backup/restore"
// 									}
// 								],
// 								"label": "BackUp/Restore"
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm not using any RAM killer",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Backup/Restore"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not able to restore my backup.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I'm getting an error 'Backup failed something went wrong'",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "Please confirm if you have installed Cleanmaster app on your device.",
// 										"theme": "HomeScreen5.0",
// 										"subCat": [],
// 										"label": "Backup Failed"
// 									}
// 								],
// 								"label": "Backup/Restore"
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/85325208-I-am-switching-to-a-new-android-phone-How-do-I-save-my-chat-backup-and-restore-in-new-phone-",
// 						"text": "I want to change my phone device, how do I take a backup?",
// 						"label": "Backup/Restore",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"active": true,
// 						"text": "I'm re-signing up to my hike account, but the backup restore option is not appearing.",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Backup/restore"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/88935677-How-do-I-restore-a-chat-backup-from-a-Backup-File-",
// 						"text": "How do I restore my backup file?",
// 						"label": "Backup/Restore",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"active": false,
// 						"text": "I deleted my hike account. I'm creating a new one. I had a backup file saved with me, but it is not getting restored as the backup restore option doesn't appear during sign up.",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Backup/restore"
// 					},
// 					{
// 						"active": false,
// 						"text": "I cannot delete my backup.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please add a screenshot on the next screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "BackUp/Restore"
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Hidden Chats",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm unable to hide/unhide a chat",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I'm unable to draw the pattern for accessing my hidden chats",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden chat"
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm unable to enter the PIN for my hidden chats",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden chat"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden chat"
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/40163340-How-do-I-unhide-a-Chat-",
// 						"text": "How do I unhide a hidden chat?",
// 						"label": "Hidden Chat",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"active": false,
// 						"text": "I can't share any pictures from the gallery and/or forward a picture to hidden chat.",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Hidden Chat"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-#hidden",
// 						"text": "I'm unable to find a hidden chat",
// 						"label": "Hidden Chat",
// 						"theme": "HomeScreen5.0",
// 						"active": true,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/40164720-How-do-I-know-if-I-ve-gotten-a-message-on-a-Hidden-Chat-",
// 						"text": "I'm not getting notification for new messages in a hidden chat",
// 						"label": "Hidden chat",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/43159734-How-do-I-change-my-password-",
// 						"text": "I forgot my hidden chat password. How do I enter into hidden mode?",
// 						"label": "Hidden chat",
// 						"theme": "HomeScreen5.0",
// 						"active": false,
// 						"type": "weburl"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Last seen",
// 				"theme": "ChatExperience",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "Please share the exact issue you are facing.",
// 						"subCat": []
// 					}
// 				],
// 				"label": "I'm not able to see last seen"
// 			},
// 			{
// 				"active": false,
// 				"text": "Hike is consuming too much space",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm referring to disk storage space",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Any screenshots to indicate or show how much storage is hike taking?",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "RAM consumption"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm referring to RAM usage",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Any screenshots to indicate or show how much RAM is hike taking?",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "RAM consumption"
// 							}
// 						]
// 					},
// 					{
// 						"active": true,
// 						"text": "Too many updates and it's consuming data",
// 						"theme": "CPR",
// 						"subCat": [],
// 						"label": "RAM consumption"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Hike is consuming too much Battery",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "Any screenshots to indicate or show how much battery hike is consuming?",
// 						"theme": "CPR",
// 						"subCat": [],
// 						"label": "Battery consumption"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Hike is consuming too much data in the background",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "Is this happening on mobile data OR WiFi",
// 						"subCat": [
// 							{
// 								"text": "Please share some screenshots"
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Delete Account",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "I'm unable to delete my hike account. I'm getting an error message.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share a screenshot on the next screen.",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Delete Acc"
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Natasha",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "Natasha bot is not sharing correct information. Please improve.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share your comments and screenshots on the next screen.",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Natasha"
// 							}
// 						]
// 					},
// 					{
// 						"url": "http://support.hike.in/entries/93169938-How-do-I-activate-Natasha-",
// 						"subCat": [],
// 						"type": "weburl",
// 						"text": "How to subscribe/unsubscribe to Natasha bot?"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "News",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm not getting News.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "News feature is only available for Android OS version 4.1 and above.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm seeing a blank screen on opening news.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Please share your comments and add screenshots on the next screen.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm getting a notification for a News from hike, but there's nothing inside the app.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share your comments and screenshots on the next screen.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News"
// 							}
// 						]
// 					},
// 					{
// 						"url": "http://support.hike.in/entries/95567027-How-do-I-subscribe-to-Hike-News",
// 						"subCat": [],
// 						"type": "weburl",
// 						"active": false,
// 						"text": "How do I subscribe/unsubscribe to news??"
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not able to scroll through the news properly.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share your comments and screenshots on the next screen.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "News is showing up in the incorrect category.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share your comments and screenshots on the next screen.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News Content Issue"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "News is not working",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Please share your comments and screenshots on the next screen.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "News is not working"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not getting News even after enabling it",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "You might have blocked news in the past. Please unblock and re-enable to start getting it. Follow these steps to Unblock: 3-Dot Menu(Available on the top-right corner) >> Settings >> Privacy >> Blocked List >> Select News >> Save.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I have enabled news but can't see it on the conversation screen"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Error in a news article",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "There is an error in the news article"
// 					},
// 					{
// 						"active": false,
// 						"text": "Full story redirects to other websites",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Full story button is redirecting me to other websites"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am getting Ads in news",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "I am getting Ads in news"
// 					},
// 					{
// 						"active": false,
// 						"text": "News is not getting updated",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "I'm not getting the latest news"
// 					},
// 					{
// 						"active": false,
// 						"text": "It only shows \"Fetching News\"",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "It only shows \"Fetching News\""
// 					},
// 					{
// 						"active": false,
// 						"text": "I do not get notifications for News",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "I do not get notifications for News"
// 					},
// 					{
// 						"active": false,
// 						"text": "Image quality is Poor",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "News image quality is poor"
// 					},
// 					{
// 						"active": false,
// 						"text": "Photos in news take time to load",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Photos in news take time to load"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Other news related issues"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Hike Daily",
// 				"theme": "Content",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I did not get hike daily today",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Hike Daily not received"
// 					},
// 					{
// 						"active": false,
// 						"text": "The fact you shared is not correct",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Discrepancy in the fact shared"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to share hike daily with my friends on hike",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to share hike daily on hike"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to share hike daily on other apps",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to share hike daily outside hike"
// 					},
// 					{
// 						"active": true,
// 						"text": "I am not able to forward hike daily",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to forward hike daily"
// 					},
// 					{
// 						"active": false,
// 						"text": "The background image does not load",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Background image doesn't load"
// 					},
// 					{
// 						"active": false,
// 						"text": "I wrote a caption while sharing a hike daily image but it does not appear",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Caption does not appear on the shared hike daily image"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Other hike daily related issues"
// 					}
// 				],
// 				"label": "Unable to share hike daily on hike"
// 			},
// 			{
// 				"active": false,
// 				"text": "Group Chats",
// 				"theme": "ChatExperience",
// 				"subCat": [],
// 				"label": "groupchat"
// 			},
// 			{
// 				"active": false,
// 				"text": "Walkie Talkie",
// 				"theme": "ChatExperience",
// 				"subCat": [
// 					{
// 						"text": "Please describe your issue in detail."
// 					}
// 				],
// 				"label": "walkietalkie"
// 			},
// 			{
// 				"active": false,
// 				"text": "Issues while Emailing Conversations",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "My hike is crashing while emailing the chats.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please remove the media in that chat and then try to email the Chat again.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Email conversation"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "For any further feedback on Email Conversation please leave your comments.",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Email conversation"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Contacts",
// 				"subCat": [
// 					{
// 						"subCat": [],
// 						"url": "http://support.hike.in/entries/95364737-I-m-not-able-to-see-a-contact-on-hike-",
// 						"text": "I'm not able to see the names of the contacts I have added to my address book recently.",
// 						"logkey": "ab",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "https://support.hike.in/hc/en-us/articles/230605907-I-cannot-see-my-contacts-on-hike-For-Xiaomi-phones-",
// 						"text": "My contacts are not syncing in hike contact list and I'm using a Xiaomi device.",
// 						"logkey": "ab",
// 						"active": false,
// 						"type": "weburl"
// 					},
// 					{
// 						"subCat": [],
// 						"url": "https://support.hike.in/hc/en-us/articles/230605987-I-cannot-see-my-contacts-on-hike-For-Lenovo-phones-",
// 						"text": "My contacts are not syncing in hike contact list and I'm using a Lenovo device.",
// 						"logkey": "ab",
// 						"active": true,
// 						"type": "weburl"
// 					},
// 					{
// 						"active": false,
// 						"text": "The above contact sync options don't help.",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "Please share the details on the next screen.",
// 								"label": "Contact sync",
// 								"theme": "CPR",
// 								"logkey": "ab",
// 								"active": false
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm receiving messages from blocked contacts.",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "Please share your number and the number from which you are receiving messages",
// 								"label": "Block contact issue",
// 								"theme": "CPR",
// 								"logkey": "ab",
// 								"active": true
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Stickey",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "I don't see Stickey under my hike account settings",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"subCat": [],
// 								"type": "weburl",
// 								"url": "https://support.hike.in/hc/en-us/articles/230606187-How-to-use-Stickey-in-Xiaomi-Phones-",
// 								"text": "I'm using a Xiaomi device."
// 							},
// 							{
// 								"subCat": [],
// 								"url": "https://support.hike.in/hc/en-us/articles/230606307-How-to-use-Stickey-in-Lenovo-Phones-",
// 								"text": "I'm using a Lenovo device",
// 								"label": "Stickey",
// 								"theme": "Growth",
// 								"active": false
// 							},
// 							{
// 								"active": false,
// 								"text": "I'm using a Motorola device with android version 5.1",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Stickey"
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm using an Intex Aqua device",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Stickey"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not able to enable hike stickey.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"subCat": [],
// 								"type": "weburl",
// 								"url": "https://support.hike.in/entries/96142448-How-to-enable-Stickey-",
// 								"text": "Please read this FAQ to know more."
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Stickey is not getting enabled for me. I have given all permissions to hike Stickey from phone settings and still not getting this enabled.",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Stickey"
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not happy with the hike stickey accessibility permissions required for using hike Stickey.",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Stickey"
// 					},
// 					{
// 						"active": false,
// 						"text": "Stickers are appearing like images when we are sharing them with friends via Stickey. They should look like stickers.",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Stickey"
// 					},
// 					{
// 						"active": false,
// 						"text": "Increase the number of stickers we can share via hike Stickey. 5 is too less.",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Stickey"
// 					},
// 					{
// 						"active": false,
// 						"text": "I need a sticker recommendation feature while using hike Stickey.",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "Stickey"
// 					},
// 					{
// 						"active": false,
// 						"text": "When I use Stickey it leaves a black circle.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share a screenshot on the next screen.",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Stickey"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Xiaomi device",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I am not able to use stickey on my Xiaomi device",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "I am not able to use stickey on my Xiaomi device"
// 							}
// 						]
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Hike Direct",
// 				"theme": "ChatExperience",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "My hike direct is not connecting",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share the number with which you are trying to connect on the next screen",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike direct not working"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "It's taking too much time to connect",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share the number with which you are trying to connect on the next screen",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike direct not working"
// 							}
// 						]
// 					}
// 				],
// 				"label": "hikedirect"
// 			},
// 			{
// 				"active": false,
// 				"text": "I'm getting an error while updating hike from Playstore",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "I'm seeing some error code on Playstore",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share the details of the error code in the next screen",
// 								"subCat": [],
// 								"label": "Update error"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"subCat": []
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "My hike account has been hacked",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Please let us know in details why you are suspecting that your hike account might be hacked on the next screen",
// 						"theme": "CPR",
// 						"subCat": [],
// 						"label": "Account hacked"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "7 Day Challenge",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I'm unable to recharge.",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please share the Operator Name (Airtel, Vodafone, idea, etc.) and State (Circle) in the next screen",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "7 day challenge"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "My hike Meter is not working.",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "My meter is stuck at 1 Minute",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "7 day challenge"
// 							},
// 							{
// 								"active": true,
// 								"text": "My meter was working fine, but it stopped working",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "7 day challenge"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "7 day challenge"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Just For Laughs",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I am not getting Just For Laughs",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Just For Laughs feature is only available on Hike App version 4.1 and above.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Request to enable JFL"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I am getting Just For Laughs even after blocking it",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Block not working for JFL"
// 					},
// 					{
// 						"active": false,
// 						"text": "Just For Laughs is not funny",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "JFL is not funny"
// 					},
// 					{
// 						"active": false,
// 						"text": "Report offensive content",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Offended by JFL content"
// 					},
// 					{
// 						"active": false,
// 						"text": "Image quality is Poor",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "JFL Image quality is poor"
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm not able to download Just For Laughs images",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to download JFL images"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to share Just For Laughs images with my friends on hike",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to share Just For Laughs images on hike"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to share Just For Laughs images on other apps",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to share Just For Laughs images outside hike"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to forward Just For Laughs images",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to forward Just For Laughs images"
// 					},
// 					{
// 						"active": false,
// 						"text": "The image is blank",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Blank Image"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am not able to post the image to my timeline",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Unable to post Just For Laughs images on timeline"
// 					},
// 					{
// 						"active": false,
// 						"text": "I wrote a caption while sharing a Just For Laughs image but it does not appear",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Caption does not appear on the shared image"
// 					},
// 					{
// 						"active": false,
// 						"text": "Scroll in Just For Laughs is not smooth",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Scroll is not smooth"
// 					},
// 					{
// 						"active": false,
// 						"text": "I do not land on the latest image",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "I do not land on the latest image"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Coupons",
// 				"theme": "Content",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "The coupon code shows invalid at the brand website",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please mention your comments and add screenshots on the next screen",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Invalid coupon code"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "The coupon was not accepted at the brand outlet",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please mention the brand name and details of the outlet you visited.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Coupon code not accepted at outlet"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm getting an error while downloading a coupon",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Please mention your comments and add screenshots on the next screen",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Error on downloading a coupon"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Other coupon related issues"
// 					}
// 				],
// 				"label": "Other coupon related issues"
// 			},
// 			{
// 				"active": false,
// 				"text": "SMS Issue",
// 				"theme": "ChatExperience",
// 				"subCat": [],
// 				"label": "sms"
// 			},
// 			{
// 				"active": false,
// 				"text": "Cricket",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I am not getting cricket updates",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Cricket feature is only available for Android OS version 4.0 and above.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Request to enable Cricket"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I do not have videos in cricket",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Videos in cricket are enabled for android users on hike app version 4.2.5 and above.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Wants Hotstar videos"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Cricket videos are not playing",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I have Google Chrome",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "The videos play only on Google Chrome version 45 and above. If you are using a lower version of Google Chrome, please update to the latest version from the Playstore.",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "Hotstar videos not playing on lower versions of Chrome"
// 									},
// 									{
// 										"active": true,
// 										"text": "I am using Google Chrome version 45 or above. Go to Chrome settings and clear data.",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "Hotstar videos not playing on required versions of Chrome"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "I do not have Google Chrome",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "Please download the latest version of Google Chrome from the Playstore.",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "Unable to play Hotstar videos without Chrome"
// 									}
// 								]
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Cricket fact disappears before I can read it",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Cricket fact disappears before I can read it"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am Stuck on the Cricket Fact screen",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please mention your comments and add screenshots on the next screen",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Getting stuck on Cricket fact screen"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "I'm seeing a blank screen on opening cricket",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "Please mention your comments and add screenshots on the next screen",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Blank screen in Cricket"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Scores are not updating/Delay in score updation",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Scores not updating/lagging"
// 					},
// 					{
// 						"active": false,
// 						"text": "I am getting Ads in Cricket",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Ads in cricket"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Other cricket related issues"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Block contact",
// 				"subCat": [
// 					{
// 						"active": true,
// 						"text": "I am receiving messages from a blocked contact",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "I am receiving messages from a blocked contact"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "hike caller",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "I do not have hike caller in my mobile",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "I do not have hike caller in my mobile"
// 					},
// 					{
// 						"active": false,
// 						"text": "I want to unlist/de-register my number from hike caller",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "I want to unlist/de-register my number from hike caller"
// 					},
// 					{
// 						"active": false,
// 						"text": "I want to change my name appearing on hike caller",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "I want to change my name appearing on hike caller"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Android N",
// 				"theme": "Growth",
// 				"subCat": [],
// 				"label": "Android N"
// 			},
// 			{
// 				"active": false,
// 				"text": "Other Issues",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Please be as detailed as possible such that we can understand it better.",
// 						"subCat": []
// 					}
// 				],
// 				"label": "Others"
// 			}
// 		],
// 		"icon": "issue"
// 	},
// 	{
// 		"subCat": [
// 			{
// 				"active": true,
// 				"text": "My Account",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Wallet settings",
// 						"deepLinkId": "Wallet_settings",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "How do I save debit or credit card details?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": true,
// 								"desc": "Go to wallet settings -> tap on \"Saved cards\" -> tap on \"Add a new card\" -> Enter card details -> Tap \"Save\""
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How do I delete a saved card?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "Go to wallet settings -> tap on \"Saved cards\" -> tap on \"delete\" button in front of the card you wish to delete."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How do I link my bank account?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "Go to wallet settings -> tap on \"Linked bank accounts\" -> tap on \"Add bank details\" -> Enter bank details -> Tap \"Save\""
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How do I delete a linked bank account?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "Go to wallet settings -> tap on \"Linked bank accounts\" -> tap on \"delete\" button in front of the account you wish to delete."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "UPI",
// 						"deepLinkId": "UPI_Onboard",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "What is UPI?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "“Unified Payment Interface” (UPI) is an instant payment system developed by the National Payments Corporation of India (NPCI), an RBI regulated entity. <br /> <br /> UPI is built over the IMPS infrastructure and allows you to transfer money between two bank accounts by using unique identifiers like mobile number or VPA (a unique payment address). You don't have to use account numbers/ IFSC codes. This makes it easy to send or receive money; just enter a receiver's mobile number/ VPA and send them money instantly into his bank account."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "What is the advantage of UPI?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "The main advantage of UPI is that you don't have to block your money by loading it into a wallet. You can simply link your bank account and then make payments without having to worry about topping up a wallet, wallet spend limits or loosing interest on your money."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How do I setup UPI with Hike Wallet?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "1. Go to settings on the wallet icon on the top left bar in the Wallet section <br /> 2. Verify your phone number <br /> 3. Select your bank <br /> 4. We will find if a bank account is linked with your phone number and provide you an option to link it."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "What is VPA?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "VPA is a Virtual Payment Address which uniquely links a user's identity with their bank account. For instance, the VPA for Hike customers is in the format xyz@ybl <br /> <br /> You can just share your VPA with any merchant and it can make a payment request to you. You can also send money to anyone by using just their VPA instead of their mobile number. There is no need to share private and confidential information like mobile number, bank account number/ IFSC code, etc."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How does Hike get my bank accounts from my phone number?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "This is a feature of the UPI payment platform (built by NPCI- an RBI regulated entity). The UPI platform retrieves the accounts details linked with your mobile number and sends it to us in a masked manner (Hike does not get the full account number). This exchange is done over secure banking networks and we don't store these details once you close the app or your session expires. No one can debit your bank account with your VPA or the masked bank account details."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Can I link multiple bank accounts with Wallet?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "You can link multiple bank account, however, at any point of time, only 1 bank account can be the defaults with your wallet. If money is transferred to you using your mobile number then it will be credited into your default account."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "What is UPI PIN?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "UPI PIN (Unified Payment Interface) is a 4-6 digit secret code issued by your bank when you sign up for mobile banking. This UPI PIN is used to authenticate all fund transfers made via mobile banking over the IMPS or UPI platform."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How do I Unlink UPI from Hike Wallet?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "1. Go to Wallet settings. <br /> 2. Tap on \"Unlink UPI\" and follow the steps. <br /> 3. Your bank account information is not persisted. You can link the bank account again anytime."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"subCat": [],
// 						"text": "Why is my account locked?",
// 						"nextScreen": "3",
// 						"priority": "3",
// 						"active": false,
// 						"desc": "For your safety and security, Hike may temporarily block your Hike account upon noticing any suspicious transactions or activity, or if your transactions are not as per Hike's terms & Conditions. If your account has been locked, please contact us for resolution."
// 					},
// 					{
// 						"subCat": [],
// 						"text": "Why am I on waitlist?",
// 						"nextScreen": "3",
// 						"priority": "2",
// 						"active": false,
// 						"desc": "Hike Wallet has just been launched. We are currently in the process of enabling access in a phased manner to ensure the best experience. Incase any of your friend sends you money via Hike Wallet you would be automatically taken off the waitlist, a link would be sent to activate your wallet"
// 					},
// 					{
// 						"subCat": [],
// 						"text": "I don't have Hike Wallet, how do I get one?",
// 						"nextScreen": "3",
// 						"priority": "2",
// 						"active": true,
// 						"desc": "Any of your friend needs to send you money via Hike Wallet, post which you would receive a link to activate the wallet or you can click on the wallet icon to get on the waitlist."
// 					},
// 					{
// 						"active": false,
// 						"text": "My query is not listed here",
// 						"subCat": [],
// 						"priority": "2"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Add / Send / Request Money",
// 				"theme": "Payments",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Add Money",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "How can I add money to my Hike wallet?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "1. You can add money to your Hike Wallet by selecting the Add Money option in your Hike Wallet. <br /> 2. You can choose to add money via UPI, Credit / Debit card or Net Banking. <br /> 3. Enter the amount you want to add and Tap on “Add Money”. <br /> 4. You will be redirected to a secure payment page where you need to provide payment details. <br /> 5. If you have any saved credit or debit card, you just need to enter CVV and proceed to the bank page. If you do not have any saved card you can choose to save a credit or debit card for faster payment next time. <br /> 6. Once you enter details on your bank’s page & complete the transaction, you will be redirected to Hike with money added in your Hike Wallet."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Are there any limitations to add money to Hike Wallet?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "You can add up to Rs. 20,000 in a calendar month to your Wallet."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "My money has been debited but it is not reflecting in Wallet, why?",
// 								"nextScreen": "3",
// 								"priority": "3",
// 								"active": true,
// 								"desc": "At Hike, your money is yours. If ever an amount is deducted from your bank/card but the payment or transaction is not successful, we will refund the amount to your card/bank the moment we get a confirmation from your bank. Refunds are credited to your bank/card usually in 3 – 5 working days, however, sometimes your bank can take upto 10 working days. If the amount is not credited in your account within this time frame, then we request you to please check with your bank. If your bank is unable to share your refund status, then please raise a ticket along with your latest bank statement from the date of transaction till date."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Send Money",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "How can I send money to someone using Hike?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "1. Open the Hike App & go to Wallet.  <br /> 2. Tap the Pay or Send icon. <br /> 3. Enter the phone number (You may select the recipient from your contact list). <br /> 4. Enter the amount you want to send. <br /> 5. Tap Send. <br /> 6. Select payment mode. <br /> 7.  Submit and the money would be sent to your contact."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "I have sent money to a friend who does not have Hike Wallet, how will he/she receive money?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "An SMS is sent to the receiver asking them to download Hike. Once they activate, a request from the receiver will be sent to you."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "What if the transfer fails?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "In case the transfer fails, the amount shall be refunded back to your Hike wallet."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How can I transfer money from Hike wallet to any VPA?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "1. Open the Hike App & go to Wallet. <br /> 2. Tap the Pay or Send icon. <br /> 3. Tap on VPA tab. <br /> 4. Select the VPA account (you may add a new VPA by tapping on \"\"Add new VPA\"\" <br /> 4. Enter the amount you want to send. <br /> 5. Tap Send. <br /> 6. Select payment mode. <br /> 7. Submit."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Request Money",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "How can I request for money?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "1. Open the Hike App & go to Wallet. <br /> 2. Go to \"\"Profile\"\" section. <br /> 3. Tap on \"\"Wallet\"\". <br /> 4. Tap on \"\"Request\"\" icon. <br /> 5. Enter either one of the following, Mobile Number, VPA or Account of the person from whom you want to request money. <br /> 6. Enter the amount you want to request. <br /> 7. You friend will receive a Request money notification."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "How much amount can I request?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": true,
// 								"desc": "You can add up to Rs. 20,000 in a calendar month to your Wallet."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "When will my request expire?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "The requests are valid for 30 days from the date of initiation."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Where can I see my request status?",
// 								"nextScreen": "3",
// 								"priority": "1",
// 								"active": false,
// 								"desc": "You may see the list of all successful and pending transactions in the transaction history. Just tap on \"Wallet\" icon and go to transaction history."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "My query is not listed here",
// 						"subCat": [],
// 						"priority": "2"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Recharge",
// 				"theme": "Payments",
// 				"deepLinkId": "Recharge_Txn",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Recharge Failed / Pending / Processing",
// 						"theme": "Payments",
// 						"deepLinkId": "Recharge_Status",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "Unable to recharge or your recharge is failing repeatedly",
// 								"nextScreen": "3",
// 								"priority": "3",
// 								"active": false,
// 								"desc": "Please check that you are selecting the valid amount for the respective operator. If your number is ported, make sure that you select circle/operator manually. Recharges also fail due to issues/load at the operator’s end. We suggest that you wait for some time and retry later."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Amount deducted but recharge failed",
// 								"nextScreen": "3",
// 								"priority": "3",
// 								"active": false,
// 								"desc": "If the recharge fails and the amount is deducted, Hike refunds the entire amount of failed recharge back to your Hike Wallet within 48 hrs. If you have not paid through Hike wallet and it has been more than 48 hours since recharge failure, the bank will refund the amount within 3-10 working days in your bank account/card."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Status shows pending - when will I get my recharge?",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": true,
// 								"desc": "If the recharge status shows ‘Pending’, just wait for 30 minutes more. ‘Pending’ means Pending at the operator’s end. We advise you not to initiate a second transaction in this case."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Recharge successful but service not received",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "Hike status shows success but bill payment/recharge not received",
// 								"nextScreen": "3",
// 								"priority": "3",
// 								"active": false,
// 								"desc": "The bill payment takes 3 days to be updated at the operator’s end. For recharges, please verify the plan whether you have received a special recharge benefit or not.  Please verify the status of the recharge with your service provider with the operator ID received."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Wrong benefit received",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "Kindly confirm the benefit by referring to recharge plan for respective operator and circle. In case of incorrect benefit(s), please get in touch with the operator."
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Recharged wrong number by mistake",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "Unfortunately none of the mobile operators allow reversal/ refund of a recharge. You may contact your Operator for further help with reverse recharge or questions."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						],
// 						"nextScreen": "0"
// 					},
// 					{
// 						"active": false,
// 						"text": "Refunds",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"text": "Refund not received",
// 								"nextScreen": "3",
// 								"priority": "2",
// 								"active": false,
// 								"desc": "All bank refunds are credited back to your bank account within 3-10 business days. If the amount is not credited in your account within this time frame then we request you to please check with your bank."
// 							},
// 							{
// 								"active": false,
// 								"text": "My query is not listed here",
// 								"subCat": [],
// 								"priority": "2"
// 							}
// 						],
// 						"nextScreen": "1"
// 					},
// 					{
// 						"active": false,
// 						"text": "My query is not listed here",
// 						"subCat": [],
// 						"priority": "2"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Privacy and Security",
// 				"subCat": [
// 					{
// 						"subCat": [],
// 						"text": "What should I know about e-mail frauds?",
// 						"nextScreen": "3",
// 						"priority": "1",
// 						"active": false,
// 						"desc": "\"Be aware of any email message that requests personal data—such as passwords, pins or direct you to a web site that asks for such information. This practice is referred as Phishing, which is fraudulent communication designed to deceive consumers into divulging personal, financial or account information. <br /> <br /> These websites or emails may involve the illegal practice of “spoofing,” or forging a website or email address to resemble another, legitimate address and business. Remember to never enter your credentials over such websites or send personal information via an email. If you come across such websites or receive such emails, please contact Hike immediately.\""
// 					},
// 					{
// 						"subCat": [],
// 						"text": "What should I know about phone frauds?",
// 						"nextScreen": "3",
// 						"priority": "1",
// 						"active": false,
// 						"desc": "It’s important for Hike users to know that Hike will not call users to request their personal account information or any credit card, debit card or net banking details or their PIN/ Hike passwords. Hike does not initiate outbound telemarketing calls. Users should not respond to any phone calls with requests for any such information and are advised to immediately report the situation to local law enforcement as well as the concerned financial institution."
// 					},
// 					{
// 						"subCat": [],
// 						"text": "What to do if I suspect fraud?",
// 						"nextScreen": "3",
// 						"priority": "1",
// 						"active": false,
// 						"desc": "If your credit card, debit card or bank account is involved, immediately contact the bank or financial institution that issued your card and block your card. Also notify your bank of any fraudulent transactions, Hike will cooperate fully on any investigation by your bank."
// 					},
// 					{
// 						"subCat": [],
// 						"text": "Is my credit or debit card information completely safe on Hike?",
// 						"nextScreen": "3",
// 						"priority": "2",
// 						"active": false,
// 						"desc": "Millions of users are on Hike's platform. You can be rest assured that your online transactions at Hike are completely safe. You can easily remove your cards from Hike app if you want to."
// 					},
// 					{
// 						"active": false,
// 						"text": "My query is not listed here",
// 						"subCat": []
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Supported devices",
// 				"subCat": [
// 					{
// 						"subCat": [],
// 						"text": "Supported devices and operating systems",
// 						"nextScreen": "3",
// 						"priority": "1",
// 						"active": true,
// 						"desc": "Currently we support all devices with Android 4.4 and above. Hike Wallet shall be available on iOS soon."
// 					},
// 					{
// 						"active": false,
// 						"text": "My query is not listed here",
// 						"subCat": [],
// 						"priority": "2"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Others",
// 				"deepLinkId": "contact_us",
// 				"subCat": [],
// 				"priority": "2"
// 			}
// 		],
// 		"text": "Report a Wallet Issue",
// 		"theme": "Payments",
// 		"deepLinkId": "Wallet_Txn",
// 		"active": true,
// 		"type": "bug",
// 		"icon": "payIssue"
// 	},
// 	{
// 		"active": false,
// 		"text": "Reported issues",
// 		"subCat": [],
// 		"nextScreen": "5",
// 		"icon": "history"
// 	},
// 	{
// 		"active": false,
// 		"type": "Feature",
// 		"text": "Suggest Something New",
// 		"subCat": [
// 			{
// 				"active": false,
// 				"text": "Suggest a new feature",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Friends",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Friends"
// 					},
// 					{
// 						"active": false,
// 						"text": "Stories tab",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "My story tab"
// 					},
// 					{
// 						"active": false,
// 						"text": "My profile tab",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "My profile tab"
// 					},
// 					{
// 						"active": false,
// 						"text": "Profile",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Profile"
// 					},
// 					{
// 						"active": false,
// 						"text": "New features for Conversation screen",
// 						"theme": "ChatExperience",
// 						"subCat": []
// 					},
// 					{
// 						"active": false,
// 						"text": "Custom fonts",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I want to use my system font. Hike doesn't support that!",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Custom fonts"
// 							},
// 							{
// 								"active": false,
// 								"text": "I would like to change the font size while using hike.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Custom fonts"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Pop-up notifications/Quick Reply",
// 						"theme": "ChatExperience",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want a quick reply window to answer to an incoming message without opening the app.",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Pop up notification"
// 							}
// 						],
// 						"label": "quickreply"
// 					},
// 					{
// 						"active": false,
// 						"text": "Night Mode",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want a dark chat theme which is less bright in dim light especially for chatting in the night time.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Night mode"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to reduce the brightness of the screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Night mode"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want the font color, icon color etc. to be changed when enabling night mode.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Night mode"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Turn Off Read Notifications",
// 						"theme": "ChatExperience",
// 						"subCat": [],
// 						"label": "Read notification"
// 					},
// 					{
// 						"active": false,
// 						"text": "Widgets",
// 						"theme": "Growth",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want hike app widget on my phone home screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Widgets"
// 							}
// 						],
// 						"label": "Growth"
// 					},
// 					{
// 						"active": false,
// 						"text": "Change Number",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to change my registered mobile number without loosing any of my Groups and Chats",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Change Number"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to use two mobile numbers on hike at the same time.",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Change Number"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "CPR",
// 								"subCat": [],
// 								"label": "Change Number"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Online indicator",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "On hike home screen",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Online Green dot"
// 							},
// 							{
// 								"active": false,
// 								"text": "On New Compose screen",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Online Green dot"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Additional settings on hike home screen",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Network usage info",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Network usage info"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hide the message content in notification bar",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hide the message content in notification bar"
// 							},
// 							{
// 								"active": false,
// 								"text": "Custom ringtone for hike Voice Calls",
// 								"theme": "VOIP&Video",
// 								"subCat": [],
// 								"label": "Custom ringtone for hike Voice Calls"
// 							},
// 							{
// 								"active": false,
// 								"text": "Mute a one-on-one chat",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Mute a one-on-one chat"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "An unread message counter on hike icon would be great.",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Message Counter on icon"
// 					},
// 					{
// 						"active": false,
// 						"text": "Multi-screen",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want hike support for the multi-screen feature in my phone.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Multi-Screen"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Web/PC app",
// 						"theme": "CPR",
// 						"subCat": [],
// 						"label": "Web Client"
// 					},
// 					{
// 						"active": false,
// 						"text": "Custom Notification Tones",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to designate different notification tones to my one-on-one and group chats.",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Custom Notification"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want different notification tones for Group Chats and one-on-one chats.",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Custom Notification"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Schedule Messages",
// 						"theme": "ChatExperience",
// 						"subCat": [],
// 						"label": "Schedule Messages"
// 					},
// 					{
// 						"active": false,
// 						"text": "Gesture based swipes",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Gestures"
// 					},
// 					{
// 						"active": false,
// 						"text": "Chat Heads",
// 						"theme": "ChatExperience",
// 						"subCat": [],
// 						"label": "Chat Heads"
// 					},
// 					{
// 						"active": false,
// 						"text": "Hide content previews in notifications",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Hide preview in notification"
// 					},
// 					{
// 						"active": false,
// 						"text": "Birthday reminders",
// 						"theme": "Growth",
// 						"subCat": [],
// 						"label": "Birthday reminder"
// 					},
// 					{
// 						"active": false,
// 						"text": "GIF support",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "GIF"
// 					},
// 					{
// 						"active": false,
// 						"text": "Live Sports scores",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Kabaddi",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want Live Kabaddi score updates"
// 							},
// 							{
// 								"active": false,
// 								"text": "Football",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want Live Football score updates"
// 							},
// 							{
// 								"active": false,
// 								"text": "Tennis",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want Live Tennis score updates"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hockey",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want Live Hockey score updates"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want Live updates for other sports"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Animated Stickers",
// 						"theme": "ExpressYourself",
// 						"subCat": [],
// 						"label": "Animated Sticker"
// 					},
// 					{
// 						"active": false,
// 						"text": "Custom LED colour for notifications",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to set different LED colour for Group and one-one-chat",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "different LED colour for Group and one-one-chat"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to set different LED colours per chat",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Different LED colours per chat"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Home Screen Theme",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to apply themes to my hike home screen",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Home Screen Theme"
// 							},
// 							{
// 								"active": true,
// 								"text": "I want to customise the colour of the hike logo",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Home Screen Theme"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Home Screen Theme"
// 							}
// 						]
// 					},
// 					{
// 						"text": "Nudges"
// 					},
// 					{
// 						"active": false,
// 						"text": "Horoscope",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Horoscope"
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"subCat": [],
// 						"label": "Others"
// 					},
// 					{
// 						"active": false,
// 						"text": "hike Settings",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "hike Settings"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Improve an existing feature",
// 				"subCat": [
// 					{
// 						"active": false,
// 						"text": "Chat thread",
// 						"theme": "ChatExperience",
// 						"subCat": [],
// 						"label": "ChatExperience"
// 					},
// 					{
// 						"active": false,
// 						"text": "Video Story",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Video Story"
// 					},
// 					{
// 						"active": false,
// 						"text": "My Story",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Views on My Story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Views"
// 							},
// 							{
// 								"active": false,
// 								"text": "Time stamp when My Story was viewed by a Friend",
// 								"subCat": []
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Stories received from Friends",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Story counter on top right corner of a story",
// 								"subCat": []
// 							},
// 							{
// 								"text": "Sequence of Stories while viewing all stories from a friend"
// 							},
// 							{
// 								"text": "Chat via Story for commenting on a story"
// 							}
// 						],
// 						"label": "Stories received from Friends"
// 					},
// 					{
// 						"active": false,
// 						"text": "Multiple Likes on a Story",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Multiple Likes"
// 					},
// 					{
// 						"active": true,
// 						"text": "Likes on a Story",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Likes"
// 					},
// 					{
// 						"active": false,
// 						"text": "Text Story",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Text Story"
// 					},
// 					{
// 						"active": false,
// 						"text": "Live Face filter suggestions",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Live face filter suggestions"
// 					},
// 					{
// 						"active": false,
// 						"text": "Hike Camera experience",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Capturing a Story using hike Camera",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Image capturing hike Camera"
// 							},
// 							{
// 								"active": false,
// 								"text": "Sharing a gallery image as a story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Sharing a gallery image as a story"
// 							},
// 							{
// 								"active": false,
// 								"text": "Image editing for posting a story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Image editing for posting a story"
// 							},
// 							{
// 								"active": false,
// 								"text": "Adding filters on a story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Filters on story"
// 							},
// 							{
// 								"active": false,
// 								"text": "Adding text on a story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Text on story"
// 							},
// 							{
// 								"active": false,
// 								"text": "Adding stickers on a story",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Stickers on story"
// 							}
// 						],
// 						"label": "Hike Camera"
// 					},
// 					{
// 						"active": false,
// 						"text": "Three tab design",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Bottom tab for navigating between tabs",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Three tab design"
// 							},
// 							{
// 								"active": true,
// 								"text": "Blue dot notifications on bottom tab",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Blue dot notifications"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "News",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Add more Languages",
// 								"subCat": [
// 									{
// 										"active": true,
// 										"text": "Tamil",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Tamil"
// 									},
// 									{
// 										"active": false,
// 										"text": "Marathi",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Marathi"
// 									},
// 									{
// 										"active": false,
// 										"text": "Kannada",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Kannada"
// 									},
// 									{
// 										"active": false,
// 										"text": "Telugu",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Telugu"
// 									},
// 									{
// 										"active": false,
// 										"text": "Gujarati",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Gujarati"
// 									},
// 									{
// 										"active": false,
// 										"text": "Malayalam",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Malayalam"
// 									},
// 									{
// 										"active": false,
// 										"text": "Urdu",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Urdu"
// 									},
// 									{
// 										"active": false,
// 										"text": "Bengali",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want to get news in Bengali"
// 									},
// 									{
// 										"active": false,
// 										"text": "Others",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "Add more languages"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Add more categories",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Employment/Jobs",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want a separate category for Employment/Jobs related news"
// 									},
// 									{
// 										"active": false,
// 										"text": "Weather Forecast",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want a separate category for Weather updates"
// 									},
// 									{
// 										"active": false,
// 										"text": "BSE/NSE",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "BSE/NSE News"
// 									},
// 									{
// 										"active": false,
// 										"text": "Automobiles",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want a separate category for Automobiles related news"
// 									},
// 									{
// 										"active": false,
// 										"text": "Start-Ups",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "I want a separate category for Start-Ups related news"
// 									},
// 									{
// 										"active": true,
// 										"text": "Others",
// 										"theme": "Content",
// 										"subCat": [],
// 										"label": "Other categories requests in news"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Bookmark News articles",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Bookmark News articles"
// 							},
// 							{
// 								"active": true,
// 								"text": "Customized news categories",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Customized news categories"
// 							},
// 							{
// 								"active": false,
// 								"text": "Add refresh button in News",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Add refresh button in News"
// 							},
// 							{
// 								"active": false,
// 								"text": "Add News widget",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Add News widget"
// 							},
// 							{
// 								"active": false,
// 								"text": "Option to mark News as Read/Unread",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Mark News Read Unread"
// 							},
// 							{
// 								"active": false,
// 								"text": "Add 'Go to top' button",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Go to Top Button"
// 							},
// 							{
// 								"active": false,
// 								"text": "RSS Feed",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "RSS Feed for News"
// 							},
// 							{
// 								"active": false,
// 								"text": "Add Search option in News",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Search for News"
// 							},
// 							{
// 								"active": false,
// 								"text": "Increase font Size",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Increase News Font Size"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to share news on other platforms like facebook, email etc.",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Share News across Platforms"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want an option to save news articles to be able to read offline",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Save News to read offline"
// 							},
// 							{
// 								"active": false,
// 								"text": "Add a button to like news articles",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Add a button to like news articles"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want Night mode in News",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Night Mode in News"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to decide when I want news notifications",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want to decide when I want news notifications"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Other News Enhancements"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Hike Daily",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to use hike daily as a full screen wallpaper",
// 								"subCat": []
// 							},
// 							{
// 								"text": "I want an option to copy hike daily text"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want hike daily in Hindi",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want hike daily in Hindi"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want hike daily quotes from Indians",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want hike daily quotes from Indians"
// 							},
// 							{
// 								"active": false,
// 								"text": "Send more quotes",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "I want to get more quotes daily"
// 							},
// 							{
// 								"active": true,
// 								"text": "Others",
// 								"subCat": []
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Chat Themes",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I want more Chat Themes",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "More Chat Themes"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Stickers",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Custom Stickers",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Custom stickers"
// 							},
// 							{
// 								"active": false,
// 								"text": "Sticker Shop",
// 								"subCat": [
// 									{
// 										"active": false,
// 										"text": "Please add categories / Tabs in sticker Shop",
// 										"theme": "ExpressYourself",
// 										"subCat": [],
// 										"label": "Please add cateogries in Sticker Shop"
// 									},
// 									{
// 										"active": true,
// 										"text": "Preview a Sticker Pack before downloading",
// 										"theme": "ExpressYourself",
// 										"subCat": [],
// 										"label": "Preview a Sticker Pack before downloading"
// 									},
// 									{
// 										"text": ""
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Preview sticker before sharing",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Preview sticker"
// 							},
// 							{
// 								"active": false,
// 								"text": "Reduce the size of Sticker",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Reduce the size of Sticker"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to create assorted sticker pack of my favorite sticker",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "I want to create assorted sticker pack of my favorite sticker"
// 							},
// 							{
// 								"active": false,
// 								"text": "Clear Recent Stickers",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Clear Recent Stickers"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to have different recent stickers for different contacts",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "I want to have different recent stickers for different contacts"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to know the sticker pack of a newly recieved sticker",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "I want to know the sticker pack of a newly recieved sticker"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want stickers with sound",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "I want stickers with sound"
// 							},
// 							{
// 								"active": false,
// 								"text": "Recent stickers in each category should appear first",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Recent stickers in each category should appear first"
// 							},
// 							{
// 								"active": false,
// 								"text": "I can't find the recently downloaded sticker pack in my sticker pallette easily",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "I can't find the recently downloaded sticker pack in my sticker pallette easily"
// 							},
// 							{
// 								"active": false,
// 								"text": "On clicking the sticker button I should see the last used sticker pack instead of recents.",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "On clicking the sticker button I should see the last used sticker pack instead of recents."
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "ExpressYourself",
// 								"subCat": [],
// 								"label": "Other Sticker Enhancements"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Match Up!",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I should be able to see who has liked my profile",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "I should be able to see who has liked my profile"
// 							},
// 							{
// 								"active": false,
// 								"text": "I should be able to match with more than 10 people",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Increase the limit of matches"
// 							},
// 							{
// 								"active": false,
// 								"text": "Match Up suggestions should be on the basis of location",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Location Based Match Up Suggestion"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Other Match Up related requests"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Hike Direct",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want to use Hike direct for GC",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike Direct for GC"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hike direct radius should be more than 100 meters",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike Direct Enchancement"
// 							},
// 							{
// 								"active": false,
// 								"text": "Download the Hike Direct files directly to SD Card.",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike Direct Enchancement"
// 							},
// 							{
// 								"subCat": [],
// 								"text": "hike direct not working.",
// 								"label": "Hike Direct not working",
// 								"theme": "ChatExperience",
// 								"active": false,
// 								"type": "Bug"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Hike Direct Enchancement"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Just For Laughs",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "JFL Enhancement requests"
// 					},
// 					{
// 						"active": false,
// 						"text": "Search",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Please add a feature to search for messages between particular dates.",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Search Between Dates"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want a universal search button to search keywords on hike conversation list screen.",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Universal Search"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Hidden Mode",
// 						"subCat": [
// 							{
// 								"active": true,
// 								"text": "I want to recover my hidden chats forgotten password without deleting my chats",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden mode"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hide the timeline posts for contacts that are hidden",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden Mode Enhancements"
// 							},
// 							{
// 								"active": false,
// 								"text": "Sharing media from gallery directly to a hidden chat",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden Mode Enhancements"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hiding the hidden chat notification",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden Mode Enhancements"
// 							},
// 							{
// 								"active": false,
// 								"text": "Hiding media shared in a hidden chat",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden Mode Enhancements"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Hidden Mode Enhancements"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Message Backup",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "I want Google Drive Backup",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Backup"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Backup"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "File Sharing",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Image crop option",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Image Crop"
// 							},
// 							{
// 								"active": true,
// 								"text": "Adding a caption to an image",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "File Transfer"
// 							},
// 							{
// 								"active": false,
// 								"text": "Rotating an image while sharing",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Rotate Image"
// 							},
// 							{
// 								"active": false,
// 								"text": "I want to share my installed Apps as APKs via hike",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "I want to share my installed Apps as APKs via hike"
// 							},
// 							{
// 								"active": false,
// 								"text": "Voice Messages/Walkie-Talkie",
// 								"subCat": [
// 									{
// 										"subCat": [],
// 										"text": "I'm unable to send Voice Messages",
// 										"label": "I'm unable to send Voice Messages",
// 										"theme": "ChatExperience",
// 										"active": false,
// 										"type": "Bug"
// 									},
// 									{
// 										"subCat": [],
// 										"text": "My Voice Message volume is too low",
// 										"label": "Walkie Talkie Issue",
// 										"theme": "ChatExperience",
// 										"active": false,
// 										"type": "Bug"
// 									}
// 								]
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Other File Sharing Enchancements"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Group Chats",
// 						"subCat": []
// 					},
// 					{
// 						"active": false,
// 						"text": "Profile Photo",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Profile Photo"
// 					},
// 					{
// 						"active": false,
// 						"text": "Broadcast",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Broadcast"
// 					},
// 					{
// 						"active": false,
// 						"text": "Cricket",
// 						"theme": "Content",
// 						"subCat": [],
// 						"label": "Cricket"
// 					},
// 					{
// 						"active": false,
// 						"text": "Coupons",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Add more coupons",
// 								"theme": "Content",
// 								"subCat": [],
// 								"label": "Add more coupons"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Profile Enhancement",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Save profile picture of friends while visiting their profile",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Save profile picture of friends while visiting their profile"
// 							},
// 							{
// 								"active": false,
// 								"text": "Profile picture privacy similar to last seen privacy(sharing with everyone, favourites, my contacts and none)",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "Profile picture privacy similar to last seen privacy(sharing with everyone, favourites, my contacts and none)"
// 							},
// 							{
// 								"active": true,
// 								"text": "More photo filters",
// 								"theme": "HomeScreen5.0",
// 								"subCat": [],
// 								"label": "More photo filters"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "hike caller",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Add the option to \"Mark Spam and block\"",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Add the option to \"Mark Spam and block\" in hike Caller"
// 							},
// 							{
// 								"active": false,
// 								"text": "Update my Hike Caller Details",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Update my Hike Caller Details"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Natasha",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Add more languages",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Add more languages for Natasha"
// 							},
// 							{
// 								"active": false,
// 								"text": "Voice response from Natasha like Siri, Google voice etc.",
// 								"theme": "Growth",
// 								"subCat": [],
// 								"label": "Voice response from Natasha like Siri etc."
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Block Contact",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"url": "http://support.hike.in/entries/21599545-Annoyed-by-someone-Block-Unblock-the-contact",
// 								"text": "I want to unblock a contact",
// 								"theme": "ChatExperience",
// 								"active": false,
// 								"type": "weburl"
// 							},
// 							{
// 								"subCat": [],
// 								"text": "Block contact for a Particular time like 1 hour or 1 day",
// 								"label": "Block contact for a Particular time like 1 hour or 1 day",
// 								"theme": "ChatExperience",
// 								"active": false,
// 								"type": "Feature"
// 							},
// 							{
// 								"active": false,
// 								"text": "Others",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Other Block Contact Enhacement"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Last Seen",
// 						"subCat": [
// 							{
// 								"subCat": [],
// 								"url": "http://support.hike.in/entries/93467388-How-do-I-configure-Privacy-Settings-for-Last-Seen-",
// 								"text": "How to turn OFF last seen",
// 								"theme": "ChatExperience",
// 								"active": false,
// 								"type": "weburl"
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm not able to see last seen",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Last seen"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Nudge",
// 						"subCat": [
// 							{
// 								"active": false,
// 								"text": "Remove Nudge",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "Remove Nudge"
// 							},
// 							{
// 								"active": true,
// 								"text": "I'm unable to nudge",
// 								"theme": "ChatExperience",
// 								"subCat": [],
// 								"label": "I'm unable to nudge"
// 							}
// 						]
// 					},
// 					{
// 						"active": false,
// 						"text": "Others",
// 						"subCat": [],
// 						"label": "Other Feature Enhacements"
// 					},
// 					{
// 						"active": false,
// 						"text": "Camera Shy",
// 						"theme": "HomeScreen5.0",
// 						"subCat": [],
// 						"label": "Camera Shy"
// 					}
// 				]
// 			},
// 			{
// 				"active": false,
// 				"text": "Suggest a new Sticker",
// 				"theme": "ExpressYourself",
// 				"subCat": [],
// 				"label": "New Sticker Suggestion"
// 			},
// 			{
// 				"text": "Suggest a new feature for Wallet"
// 			}
// 		],
// 		"icon": "feature"
// 	},
// 	{
// 		"subCat": [],
// 		"url": "http://www.twitter.com/hikestatus",
// 		"text": "System Health",
// 		"active": false,
// 		"type": "weburl",
// 		"icon": "system-health"
// 	},
// 	{
// 		"subCat": [],
// 		"url": "https://support.hike.in/hc/en-us/articles/230607647-The-Terms-Conditions",
// 		"text": "Terms and Conditions",
// 		"active": false,
// 		"type": "weburl",
// 		"icon": "tnc"
// 	},
// 	{
// 		"subCat": [],
// 		"url": "https://support.hike.in/hc/",
// 		"text": "FAQ",
// 		"active": false,
// 		"type": "weburl",
// 		"icon": "faq"
// 	}
// ]