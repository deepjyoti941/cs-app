! function(window, platformSdk) {

    'use strict';

    var ALLOWED_FILE_SIZE = 0,
        TYPE_WEBURL = 'WEBURL';

    var microAppVersion = 'V0.13';

    var basePath, parentTopic, screenType = '',
        deviceName = '',
        fileUploadList = [],
        pastIssueArrayList = [],
        conversationArrayList = [],
        isFromDeepLink = false,
        isFromCbot = false,
        isFromReportedIssueDeeplink = false,
        isEmptyPastIssueResponse = false,
        isEmptyIssueConversationResponse = false,
        isLastLeafNode = false,
        lastLeafNode = {},
        isFromNotification = false,
        latestPullDataArray = [],
        deepLinkedData = {},
        appInitData = {},
        currentIssue,
        uploadCount = 0,
        pastIssuePageSize,
        conversationPageSize,
        serverUrls,
        appName,
        themeSelected,
        prioritySelected,
        allowedFileTypes,
        selectedListCategory,
        deepLinkBreadCrub = '',
        breadCrumb = {
            text: '',
            list: []
        },
        feedBackData = {
            appVersion: '',
            summary: '',
            issuetype: {
                name: ''
            },
            sub_category: '',
            description: ''
        },
        analyticsLogObj = {
            "uk": "help",
            "k": "act_help",
            "p": "microapp",
            "c": "",
            "o": "",
            "fa": "",
            "g": "",
            "s": "",
            "v": "",
            "f": "",
            "ra": ""
        },
        pastIssueObj = {
            active: false,
            nextScreen: 5,
            subCat: [],
            text: 'Reported issues',
            type: 'pastIssue'
        };


    var endSearch = true;

    var analyticsLog = {};

    var categoryListElem = document.getElementById('category-list'),
        categoryTitle = document.getElementById('category-title'),
        mainTitle = document.getElementById('main-title'),
        backIcon = document.getElementById('back-icon'),
        searchIcon = document.getElementById('search-icon'),
        backNav = document.getElementById('back-nav'),
        mainCard = document.getElementById('main-section'),
        lastSection = document.getElementById('last-section'),
        thankSection = document.getElementById('thank-section'),
        answerSection = document.getElementById('answers-section'),
        questionPart = document.getElementById('question-part'),
        answerPart = document.getElementById('answer-part'),
        heroImg = document.getElementById('hero-section'),
        searchFieldContainer = document.getElementById('search-field-container'),
        searchField = document.getElementById('search-field'),
        submitBtn = document.getElementById('submit-btn'),
        unhappyBtn = document.getElementById('unhappy-btn'),
        happyBtn = document.getElementById('happy-btn'),
        attachContainer = document.getElementById('attachment-container'),
        homeBtn = document.getElementById('home-btn'),
        feedbackText = document.getElementById('feedback-text'),
        noNetworkStrip = document.getElementById('network-status'),
        pastIssueSection = document.getElementById('past-issue-section'),
        pastIssueList = document.getElementById('past-issue-list'),
        pastIssueDetails = document.getElementById('past-issue-details'),
        issueConversationList = document.getElementById('issue-conversation-list'),
        newMessage = document.getElementById('new-message'),
        submitMessageBtn = document.getElementById('submit-message-btn'),
        loadingBar = document.getElementById('loading-bar'),
        smallLoadingBar = document.getElementById('small-loading-bar'),
        convSmallLoadingBar = document.getElementById('conv-small-loading-bar'),
        ticketMenu = document.getElementById('ticket-menu'),
        lastConversationSpace = document.getElementById("last-conversation-space"),
        messageSection = document.getElementById('message-section'),
        resolvedSection = document.getElementById('resolved-section'),
        satisfactionSection = document.getElementById('satisfaction-section'),
        resolvedBtn = document.getElementById('resolved-btn'),
        rainyCloud = document.getElementById('rainy-cloud'),
        thanksSectionTicketId = document.getElementById('thank-section-ticket-id'),
        noContentSection = document.getElementById('no-content-section'),
        pastIssueSectionFromCbot = document.getElementById('past-issue-section-from-cbot'),
        thankYouText = document.getElementById('thank-you-text'),
        noContent = document.getElementById('no-content'),
        replyAttachment = document.getElementById('reply-attachment');


    // array filter support for older browser/phone
    if (!Array.prototype.filter) {
        Array.prototype.filter = function(fun /*, thisp*/ ) {
            "use strict";

            if (this == null)
                throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun != "function")
                throw new TypeError();

            var res = [];
            var thisp = arguments[1];
            for (var i = 0; i < len; i++) {
                if (i in t) {
                    var val = t[i]; // in case fun mutates this
                    if (fun.call(thisp, val, i, t))
                        res.push(val);
                }
            }

            return res;
        };
    }

    /**
     * [addSubCategories function to add new categories]
     * @param {[type]} listItem [array objects]
     * @param {[type]} title    [string]
     */
    var addSubCategories = function(listItem, title) {
        console.log(listItem);

        console.log("3. in addSubCategories");
        // console.log(listItem);
        if (!isFromDeepLink) hideLoader();
        var categoryList = listItem.subCat;
        lastSection.style.display = 'none';
        thankSection.style.display = 'none';
        answerSection.style.display = 'none';
        if (!isFromReportedIssueDeeplink) pastIssueSection.style.display = 'none';
        noContentSection.style.display = 'none';


        screenType = 'mainSection';

        if (title) {
            categoryTitle.innerText = title;
            if (breadCrumb.text) {
                breadCrumb.text = breadCrumb.text + '=>' + title;
            } else
                breadCrumb.text = title;
        }

        if (!categoryList || !categoryList.length) {

            var attachments = attachContainer.querySelectorAll('.attachment');
            var classList;
            for (var i = 0; i < attachments.length; i++) {
                classList = attachments[i].classList;
                if (classList.contains('uploading') || classList.contains('uploaded')) {
                    classList.remove('uploaded');
                    classList.remove('uploading');
                    attachments[i].querySelector('img').src = basePath + 'assets/images/plus.png';
                }
                if (i === 0)
                    classList.remove('disabled');
                else
                    classList.add('disabled');
            }
            fileUploadList = [];
            uploadCount = 0;
            feedbackText.value = '';
            feedBackData.description = '';
            feedBackData.label = listItem.label;
            feedBackData.theme = listItem.theme;
            feedBackData.priority = listItem.priority;
            feedBackData.logkey = listItem.logkey;

            lastSection.classList.remove('backward');

            if (listItem.nextScreen === '5' || listItem.nextScreen === 5) {
                screenType = 'pastIssueSection';
                showPastIssueSection(listItem);
                pastIssueObj = listItem;
            }

            if (listItem.nextScreen === undefined || listItem.nextScreen === 1 || listItem.nextScreen === '1') {
                console.log('show showLastSection');
                showLastSection(listItem);
            }

            if (listItem.nextScreen === 2 || listItem.nextScreen === '2') {
                console.log('show submitFeedback');
                submitFeedback(undefined, feedBackData);
            }

            if (listItem.nextScreen === 3 || listItem.nextScreen === '3') {
                console.log('show answer section with unhappy button');
                showAnswerSection(listItem);
            }

            if (listItem.nextScreen === '6' || listItem.nextScreen === 6) {
                showIssueDetailsSection(listItem)
            }

        } else {
            updateListInHtml(categoryList);
            breadCrumb.list.push(categoryList);
        }
    };

    /**
     * [showIssueDetailsSection function to display issue details conversations]
     * @param  {[type]} listItem [array objects]
     * @return {[type]}          [description]
     */
    var showIssueDetailsSection = function(listItem) {
        console.log(listItem);
        checkConnection();
        hideMaiLink();
        selectedListCategory = listItem;
        lastSection.style.display = 'none';
        mainCard.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'none';
        //   messageSection.style.display = 'none';
        pastIssueDetails.style.display = 'block';
        platformSdk.bridge.allowBackPress('true');
        //   backNav.style.display = 'block';
        screenType = 'pastIssueDetails';

    }

    /**
     * [showNoInternetCloud function to display/hide offline internet connection]
     * @return {[type]} [description]
     */
    var showNoInternetCloud = function() {
        lastSection.style.display = 'none';
        thankSection.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'none';
        pastIssueDetails.style.display = 'none';
        mainCard.style.display = 'none';
        screenType = 'noInternetScreen';
    }

    /**
     * [showissueListPage shows issue list section]
     * @return {[type]} [description]
     */
    var showissueListPage = function() {
        searchIcon.style.display = 'none';
        lastSection.style.display = 'none';
        thankSection.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'block';
        pastIssueDetails.style.display = 'none';
        // mainCard.style.display = 'none';
        screenType = 'pastIssueSection';
        // backNav.style.display = 'block';

    };

    /**
     * [showMainSection shows main/home section]
     * @return {[type]} [description]
     */
    var showMainSection = function() {
        isLastLeafNode = false;
        lastLeafNode = {};
        if (isFromNotification) {
            platformSdk.bridge.allowBackPress('false');
            platformSdk.bridge.allowUpPress('false');
        }

        console.log(screenType);
        searchIcon.style.display = 'inline-block';
        lastSection.style.display = 'none';
        thankSection.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'none';
        pastIssueDetails.style.display = 'none';
        noContentSection.style.display = 'none';
        mainCard.style.display = 'block';
        rainyCloud.style.display = 'none';
        screenType = 'mainSection';

        // backNav.style.display = 'none';

    };

    var showLoader = function() {
        // console.log('showLoader called');
        loadingBar.style.display = 'block';
    }

    var hideLoader = function() {
        console.log('hide loader');
        loadingBar.style.display = 'none';
    }

    var showSmallLoader = function() {
        smallLoadingBar.style.display = 'block';
    }

    var hideSmallLoader = function() {
        smallLoadingBar.style.display = 'none';
    }

    var showConvSmallLoader = function() {
        convSmallLoadingBar.style.display = 'block';
    }

    var hideConvSmallLoader = function() {
        convSmallLoadingBar.style.display = 'none';
    }

    setTimeout(function() {
        checkConnection();
    }, 500);

    /**
     * [showNoNetworkStrip shows network strip when no internet]
     * @param  {[type]} offline [integer]
     * @return {[type]}         [description]
     */
    var showNoNetworkStrip = function(offline) {
        var timer = undefined;
        if (!offline) {
            if (!timer) {
                hideLoader();
                if (screenType === 'pastIssueSection') {
                    rainyCloud.style.display = 'block';
                    pastIssueSection.style.display = 'none';
                }
                console.log(screenType);
                //submitBtn.setAttribute('disabled', true);
                submitBtn.classList.add('disabled');
                submitMessageBtn.classList.add('disabled');
                attachContainer.classList.add('offline');
                noNetworkStrip.style.display = 'block';
                submitBtn.classList.remove('progress');

                timer = setTimeout(function() {
                    checkConnection();

                }, 500);
            }

        } else {
            //submitBtn.removeAttribute('disabled');
            submitBtn.classList.remove('disabled');
            attachContainer.classList.remove('offline');
            noNetworkStrip.style.display = 'none';
            rainyCloud.style.display = 'none';

            clearTimeout(timer);
            timer = undefined;
        }

    };

    /**
     * [checkConnection checks device network connection and shows network strip]
     * @return {[type]} [description]
     */
    var checkConnection = function() {
        platformSdk.nativeReq({
            ctx: this,
            fn: 'checkConnection',
            success: function(netType) {
                console.log(netType);
                showNoNetworkStrip(parseInt(netType) > 0);
            }
        });
    };


    /**
     * [showAnswerSection shows answersection of a leaf node]
     * @param  {[type]} listItem [array objects]
     * @return {[type]}          [description]
     */
    var showAnswerSection = function(listItem) {
        isLastLeafNode = true;
        lastLeafNode = listItem,
            console.log(listItem);
        checkConnection();
        hideMaiLink();

        selectedListCategory = listItem;
        answerPart.innerHTML = listItem.desc;
        lastSection.style.display = 'none';
        mainCard.style.display = 'none';
        // backNav.classList.remove('back-nav-visible');
        // mainCard.classList.remove('back-nav-visible');
        answerSection.style.display = 'block';
        pastIssueSection.style.display = 'none';
        noContentSection.style.display = 'none';
        searchIcon.style.display = 'none';
        screenType = 'answerSection';
        questionPart.innerHTML = listItem.text;

    }

    var showPastIssueSection = function(listItem) {
        console.log(listItem);
        console.log(breadCrumb.text);
        checkConnection();
        hideMaiLink();
        selectedListCategory = listItem;
        lastSection.style.display = 'none';
        mainCard.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'block';
        noContentSection.style.display = 'none';

        pastIssueDetails.style.display = 'none';
        searchIcon.style.display = 'none';
        //   platformSdk.bridge.allowBackPress('true');
        //   backNav.style.display = 'block';
        screenType = 'pastIssueSection';

    }

    var showLastSection = function(listItem) {
        checkConnection();

        hideMaiLink();

        lastSection.style.display = 'block';
        searchIcon.style.display = 'none';
        mainCard.style.display = 'none';
        thankSection.style.display = 'none';
        answerSection.style.display = 'none';
        pastIssueSection.style.display = 'none';
        noContentSection.style.display = 'none';



        if (listItem) {
            feedbackText.style.display = 'block';
            attachContainer.style.display = 'block';
            if (listItem.feedbackScreen === 0) {
                attachContainer.style.display = 'none';
            }
            if (listItem.feedbackScreen === 1) {
                feedbackText.style.display = 'none';
            }
        }
        screenType = 'lastSection';
    };

    var showThankSection = function(ticketId, feedBackData) {
        console.log(feedBackData);
        // var thankYouNote = platformSdk.appData.helperData.copies.thankYouText
        // thankYouNote = thankYouNote.replace('#ticketId#', ticketId)
        // console.log(thankYouNote);
        var thankYouClasses = thankSection.getElementsByClassName('thank-note');
        console.log(thankYouClasses);
        if (feedBackData.theme && feedBackData.theme == 'Payments') {
            thankYouClasses[0].style.display = 'block';
            thankYouClasses[1].style.display = 'none';
        } else {
            thankYouClasses[0].style.display = 'none';
            thankYouClasses[1].style.display = 'block';
        }
        thanksSectionTicketId.innerHTML = ticketId;
        lastSection.style.display = 'none';
        mainCard.style.display = 'none';
        backNav.classList.remove('back-nav-visible');
        mainCard.classList.remove('back-nav-visible');
        thankSection.style.display = 'block';
        screenType = 'thankSection';

        platformSdk.bridge.allowBackPress('false');
        platformSdk.bridge.allowUpPress('false');
    };

    function ObjectLength_Modern(object) {
        return Object.keys(object).length;
    }

    function ObjectLength_Legacy(object) {
        var length = 0;
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                ++length;
            }
        }
        return length;
    }

    var updateListInHtml = function(categoryList, search) {
        console.log(categoryList);
        if (!categoryList) {
            // getCachedData();
            // getDataFromCache('deepLinkPathObj');
            backNav.classList.remove('back-nav-visible');
            showMainSection()
            getCachedData(function(cachedData) {
                if (Object.keys(cachedData).length) {
                    addSubCategories({
                        subCat: cachedData
                    }, 'how can we help?');
                    return;
                } else {
                    addSubCategories({
                        subCat: window.catData
                    }, 'how can we help?');
                }
            });

        }
        console.log('4. inside updateListInHtml');
        var catLen = categoryList.length,
            i = 0;

        var docFrag = document.createDocumentFragment(),
            li, txt;

        if (search && catLen === 0) {
            li = document.createElement('li');
            li.classList.add('category');
            li.classList.add('no-search-result');

            txt = document.createTextNode('No match found');

            li.appendChild(txt);
            docFrag.appendChild(li);
        }

        for (i = 0; i < catLen; i++) {
            var cat = categoryList[i];
            console.log(cat);

            li = document.createElement('li');
            li.classList.add('category');
            li.classList.add('clearfix');

            if (search) {
                li.setAttribute('data-index', cat.index);
            } else {
                li.setAttribute('data-index', i);
                li.classList.add('animate');
            }

            if (cat.type) {
                if (cat.type.toUpperCase() === TYPE_WEBURL && cat.url) {
                    li.setAttribute('data-weburl', cat.url);
                } else {
                    li.setAttribute('data-type', cat.type);
                }
            }

            console.log(platformSdk.appData.notifData);
            var ObjectLength = Object.keys ? ObjectLength_Modern : ObjectLength_Legacy;
            var issueCount = ObjectLength(platformSdk.appData.helperData.tempNotifData)
            if (issueCount > 0) {
                var spanCounter = document.createElement('span');
                spanCounter.classList.add('span-counter');
                spanCounter.setAttribute("id", 'issue-counter');
                spanCounter.innerText = issueCount;
            }

            if (cat.nextScreen === "5") {
                li.setAttribute('data-next-screen', cat.nextScreen);
            }
            if (cat.subCategoryName) {
                li.setAttribute('data-sub-type', cat.subCategoryName);
            }

            if (cat.icon) {
                var imgDiv = document.createElement('div');
                imgDiv.classList.add('cat-type-icon-container');
                var img = document.createElement('img');
                img.src = basePath + 'assets/images/' + cat.icon + '.png';
                img.classList.add('cat-type-icon');
                imgDiv.appendChild(img);
                li.appendChild(imgDiv);
            }

            //hack to keep special characters intact while creating text nodes.
            //create a dummy element add the text as inner HTML to it  and read this back in your variable
            var dummyDiv = document.createElement('div');
            dummyDiv.innerHTML = cat.text;

            cat.text = dummyDiv.innerText;

            var textSpan = document.createElement('span');
            textSpan.innerHTML = cat.text;

            li.appendChild(textSpan);

            if (cat.nextScreen === "5" && issueCount > 0) textSpan.appendChild(spanCounter);

            docFrag.appendChild(li);

            console.log(screenType);
            var spanCounter = document.getElementById("issue-counter");
            console.log(spanCounter);
            if (spanCounter) {
                console.log('#####################hide spanCounter ########');
                spanCounter.innerText = 0;
                spanCounter.style.display = 'none';
            }
        }

        categoryListElem.innerHTML = '';
        categoryListElem.appendChild(docFrag);

    };

    var getFormatedDate = function(string) {

        var today = new Date();
        var issueDate = new Date(string);
        var months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        //   if (today.getDate()===issueDate.getDate()) return "Today at "+getFormatedTime(string);
        if (today.getDate() === issueDate.getDate()) return getFormatedTime(string);

        else return months[issueDate.getMonth()] + ' ' + issueDate.getDate() + ', ' + issueDate.getFullYear();

    }

    var getFormatedTime = function(string) {
        var date = new Date(string);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ampm;
        return strTime;
    }

    var createUlElement = function(array) {
        // console.log('$$$$$$$$$$$$$ clear prev list $$$$$$$$$$$$');
        // pastIssueList.innerHTML = '';
        for (var i = 0; i < array.length; i++) {
            console.log(array[i]);
            // issueListTemplate(array[i]))
            var issueObj = array[i];
            var additionalInfoContainer = document.createElement('div');
            var subSpan = document.createElement('span');
            var statusSpan = document.createElement('span');
            var issueStatusDiv = document.createElement('div');
            var timstampDiv = document.createElement('div');

            var attachmentsDiv = document.createElement('div');
            var attachmentsMetaData = document.createElement('div');
            var attachmentsIcon = document.createElement('span');
            var attachmentsCount = document.createElement('span');
            var attachmentsUl = document.createElement('ul');


            attachmentsUl.classList.add('attachments-list');
            // additionalInfoContainer.style.width = '260px';
            var ticketIdSpan = document.createElement('span');
            var createdAtSpan = document.createElement('span');
            // var issueStatus = document.createElement('span');

            additionalInfoContainer.classList.add('past-issue-additional-container');
            issueStatusDiv.classList.add('issue-status-div');
            subSpan.classList.add('subject');
            createdAtSpan.classList.add('creation-time');
            createdAtSpan.classList.add('vertical-span');
            ticketIdSpan.classList.add('ticket-id');
            // issueStatus.classList.add('issue-status');
            statusSpan.classList.add('fixed-status-button');
            statusSpan.classList.add('vertical-span');

            subSpan.innerHTML = array[i].sub;
            ticketIdSpan.innerHTML = 'Ticket ID: #' + array[i].id;
            createdAtSpan.innerHTML = getFormatedDate(array[i].created_at);
            if (array[i].newNotif) subSpan.classList.add('new-notif');

            if (array[i].status == "Closed") {
                statusSpan.innerHTML = 'RESOLVED'
                statusSpan.classList.add('closed-status');
                // ticketIdSpan.style.opacity = '.5';

                // console.log('apply resolved style');
                // issueStatus.classList.add('resolved');
            } else if (array[i].status == "Resolved") {
                statusSpan.innerHTML = 'RESOLVED'
                statusSpan.classList.add('closed-status');
                // ticketIdSpan.style.opacity = '.5';
            } else {
                statusSpan.innerHTML = 'OPEN'
                statusSpan.classList.add('open-status');
                statusSpan.style['margin-left'] = '30px';
            }

            additionalInfoContainer.appendChild(subSpan);
            timstampDiv.appendChild(ticketIdSpan);
            timstampDiv.appendChild(createdAtSpan);

            // additionalInfoContainer.appendChild(ticketIdSpan);
            // additionalInfoContainer.appendChild(createdAtSpan);


            issueStatusDiv.appendChild(statusSpan);
            // issueStatusDiv.appendChild(createdAtSpan);
            // additionalInfoContainer.appendChild(createdAtSpan);


            var item = document.createElement('li');
            item.classList.add('category');
            // item.classList.add('clearfix');
            item.classList.add('issue-item');
            item.setAttribute('data-index', array[i].id);

            item.appendChild(additionalInfoContainer);
            item.appendChild(issueStatusDiv);
            item.appendChild(timstampDiv);
            // item.appendChild(attachmentsDiv);


            pastIssueList.appendChild(item);

            (function(value) {

                item.addEventListener("click", function(e) {
                    console.log(e);
                    currentIssue = value;
                    getIssueDetails(value);
                }, false);
            })(array[i]);

        }
    }

    var createIssueConversionTree = function(issueDetails, conversations) {
        hideLoader();
        hideConvSmallLoader();
        console.log(issueDetails);
        console.log(breadCrumb.text);
        searchIcon.style.display = 'none';

        for (var i = conversations.length; i--;) {
            var item = document.createElement('li');
            item.classList.add('conv-class');

            var convListContainer = document.createElement('div')
            var convPostDetailsDiv = document.createElement('div');
            var convDetailsDiv = document.createElement('div');
            var convText = document.createElement('P');
            convText.style['line-height'] = '1';
            convPostDetailsDiv.classList.add('conversation-meta-data');
            convDetailsDiv.classList.add('conversation-description');
            convListContainer.classList.add('conversation-list-container');
            var profilePicSpan = document.createElement('span')
            profilePicSpan.classList.add('profile-circle');


            convPostDetailsDiv.appendChild(profilePicSpan);

            var userSpan = document.createElement('span');
            var agentSpan = document.createElement('span');
            var timeStampSpan = document.createElement('span');
            userSpan.classList.add('conversation-post-details')
            agentSpan.classList.add('conversation-post-details')

            userSpan.innerHTML = 'Me';
            agentSpan.innerHTML = 'Customer Support';
            timeStampSpan.classList.add('conversation-timestamp');
            // timeStampSpan.innerHTML = ' '+getFormatedDate(conversations[i].ts)+' at '+ getFormatedTime(conversations[i].ts);
            timeStampSpan.innerHTML = ' ' + getFormatedDate(conversations[i].ts);

            console.log(userSpan);

            console.log(conversations[i]);
            // left-conv style for user
            // right-conv style is for agent
            if (conversations[i].sent) {
                convPostDetailsDiv.appendChild(userSpan);
                convPostDetailsDiv.appendChild(timeStampSpan);
                item.classList.add('left-conv');
                //  profilePicSpan.style['padding-left'] = '10px';
                profilePicSpan.innerHTML = deviceName;
                // timeStampSpan.style['margin-left'] = '155px';
                // item.appendChild(userSpan)
            } else {
                convPostDetailsDiv.appendChild(agentSpan);
                convPostDetailsDiv.appendChild(timeStampSpan);
                profilePicSpan.innerHTML = 'CS';
                // timeStampSpan.style['margin-left'] = '60px';
                item.classList.add('right-conv');
            }

            // item.innerHTML = conversations[i].text;

            // conversations[i].text.replace(/(\r\n|\n|\r)/gm, "<br>");
            var text = document.createTextNode(conversations[i].text);
            convText.appendChild(text)
            convDetailsDiv.appendChild(convText);
            //  convDetailsDiv.innerHTML = conversations[i].text;
            console.log(conversations[i].text);

            if (conversations[i].attachments) {
                for (var j = 0; j < conversations[i].attachments.length; j++) {
                    console.log(conversations[i].attachments[j]);
                    var img = document.createElement('img');
                    img.width = 200;
                    img.height = 300;
                    img.src = '';
                    img.src = conversations[i].attachments[j];
                    convDetailsDiv.appendChild(img)
                }
            }

            convListContainer.appendChild(convPostDetailsDiv);
            convListContainer.appendChild(convDetailsDiv);

            item.appendChild(convListContainer);

            issueConversationList.appendChild(item);
        }


        if (issueDetails && issueDetails.type == 'agentReply') {
            console.log('***append agent notification***');
            addAgentMessage(issueDetails);
        }

        setTimeout(function() {
            lastConversationSpace.scrollIntoView({
                block: "end",
                behavior: "smooth"
            });
        }, 300)
    }

    var getIssueDetails = function(issueDetails) {

        // analytics for issue is clicked
        analyticsLogObj['c'] = 'help_past_issues';
        analyticsLogObj['o'] = 'past_issues_screen';
        analyticsLogObj['fa'] = 'click_past_issue_card';
        analyticsLogObj['g'] = 1;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = ''; // count for open issues
        analyticsLogObj['ra'] = ''; // count for closed issues
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = issueDetails.id;

        console.log('analytics for issue is clicked');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj))

        console.log(issueDetails);
        console.log(conversationPageSize);
        issueConversationList.innerHTML = '';
        conversationPageSize = 1;
        conversationArrayList = [];
        showLoader();
        var container = document.getElementById('title-search-icon-container');
        ticketMenu.innerHTML = '';
        categoryTitle.innerText = '';
        var menuIssueDetailsDiv = document.createElement('div');
        var menuStatusDetailsDiv = document.createElement('div');

        menuIssueDetailsDiv.style.display = 'inline-block';
        menuIssueDetailsDiv.style['padding-top'] = '12px';
        menuStatusDetailsDiv.style.display = 'inline-block';
        menuStatusDetailsDiv.style.float = 'right';
        menuStatusDetailsDiv.style['padding-top'] = '12px';
        var createdAtContent = document.createElement('span');
        var ticketIdContent = document.createElement('span');
        var issueStatus = document.createElement('span');
        issueStatus.classList.add('fixed-status-button')
        createdAtContent.classList.add('last');
        // issueStatus.classList.add('issue-status');

        if (issueDetails.status == "Resolved") {
            issueStatus.innerHTML = 'RESOLVED';
            issueStatus.classList.add('closed-status');
            resolvedSection.style['margin-bottom'] = '0px';
            resolvedBtn.style.display = 'block';
            // hide the message section and show resolved section
            resolvedSection.style.display = 'block'
            resolvedSection.style['background-color'] = '#fff'
            messageSection.style.display = 'none';

        } else if (issueDetails.status == "Closed") {
            issueStatus.innerHTML = 'RESOLVED';
            issueStatus.classList.add('closed-status');
            issueConversationList.style['margin-bottom'] = '13%';
            // resolvedSection.style['margin-bottom'] = '50px';
            resolvedBtn.style.display = 'none';
            // hide the message section and show resolved section
            resolvedSection.style.display = 'block'

            messageSection.style.display = 'none';
        } else {
            issueStatus.innerHTML = 'OPEN';
            issueStatus.classList.add('open-status');
            messageSection.style.display = 'block';
            resolvedSection.style.display = 'none';
            issueConversationList.style['margin-bottom'] = '27%';

        }

        ticketIdContent.innerHTML = 'Ticket ID #' + issueDetails.id;
        ticketIdContent.classList.add('menu-ticket-id');
        createdAtContent.innerHTML = 'Created on ' + getFormatedDate(issueDetails.created_at);
        createdAtContent.classList.add('menu-created-time');
        categoryTitle.setAttribute('data-index', issueDetails.id);
        // categoryTitle.innerText = issueDetails.desc.replace(/(\r\n|\n|\r)/gm,"");
        categoryTitle.innerText = issueDetails.sub.replace(/(\r\n|\n|\r)/gm, "");


        menuIssueDetailsDiv.appendChild(ticketIdContent);
        menuIssueDetailsDiv.appendChild(createdAtContent)
        menuStatusDetailsDiv.appendChild(issueStatus);

        ticketMenu.appendChild(menuIssueDetailsDiv);
        ticketMenu.appendChild(menuStatusDetailsDiv);

        console.log(issueDetails);
        issueDetails.nextScreen = 6;
        addSubCategories(issueDetails)
        // issueConversationList.innerHTML = '';

        getConversationList(null, issueDetails, conversationPageSize, getConversationListCallback)
    };

    var gotoNextCategory = function(target) {
        console.log('5. inside gotoNextCategory');
        endSearch = true;

        if (!target) return;


        var idx = target.getAttribute('data-index');
        var webUrl = target.getAttribute('data-weburl');
        var catType = target.getAttribute('data-type');
        var subCatType = target.getAttribute('data-sub-type');

        var targetText = target.querySelector('span').innerText;

        var currentCategoryList = breadCrumb.list[breadCrumb.list.length - 1];
        var listItem = currentCategoryList[idx];

        var nextScreen = parseInt(listItem.nextScreen);

        if ((!listItem.subCat || !listItem.subCat.length) && (nextScreen === 0)) {
            return;
        }

        if (target.classList.contains('no-search-result')) {
            return;
        }

        if (catType) {
            feedBackData.issuetype.name = catType;
            parentTopic = targetText;
        }
        if (breadCrumb.list.length === 2) {
            if (subCatType)
                feedBackData.sub_category = subCatType;
            else
                feedBackData.sub_category = targetText;
        }

        analyticsLog = {
            mapp_vs: microAppVersion,
            ek: 'topic_selected',
            topic_selected: targetText,
            level: breadCrumb.list.length - 1
        };


        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);


        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['d'] = '';
        analyticsLogObj['ra'] = '';
        analyticsLogObj['fa'] = 'screen_open';
        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = '';
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = ''

        // PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj))
        console.log('analytics for category clicked');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        if (webUrl) {
            platformSdk.openFullPage(targetText, webUrl);
            return;
        }

        hideSearchField();

        backNav.classList.add('back-nav-visible')
        mainCard.classList.add('back-nav-visible')
        platformSdk.bridge.allowBackPress('true');
        platformSdk.bridge.allowUpPress('true');

        categoryListElem.classList.remove('backward');
        categoryListElem.classList.add('forward');

        heroImg.style.display = 'none';
        mainTitle.style.display = 'none';
        console.log(targetText);
        console.log(listItem);
        // if (listItem.theme) themeSelected = listItem.theme;
        addSubCategories(listItem, targetText);

        document.body.scrollTop = 0;
    };

    var gotoPrevCategory = function(e) {
        hideLoader();
        console.log('prev button clicked');
        console.log(screenType);
        console.log('isFromCbot', isFromCbot);
        console.log('isFromReportedIssueDeeplink', isFromReportedIssueDeeplink);
        endSearch = true;
        isEmptyPastIssueResponse = false;
        isEmptyIssueConversationResponse = false;
        if (screenType === 'mainSection' && isFromReportedIssueDeeplink) {
            PlatformBridge.closeWebView();
            return;
        }
        if (backNav.classList.contains('searching')) {
            hideSearchField();
            updateListInHtml(breadCrumb.list[breadCrumb.list.length - 1]);
            return;
        }
        categoryListElem.classList.remove('forward');
        categoryListElem.classList.add('backward');

        if (breadCrumb.list.length === 1 && screenType === 'mainSection') {
            backNav.style.display = 'none'
            return;
        }

        if (screenType === 'thankSection') {
            showLastSection();
            return;
        }
        if (screenType === 'lastSection' && isLastLeafNode) {
            answerSection.classList.add('backward');
            showAnswerSection(lastLeafNode);
            return;
        }

        if (screenType === 'lastSection') {
            lastSection.classList.add('backward');
            showMainSection();
            showPrevData();
            return;
        }

        if (screenType === 'mainSection') {
            pastIssuePageSize = 1;
            // backNav.style.display = 'none';
            showMainSection();
            breadCrumb.list.pop();
            showPrevData();
            return;
        }

        if (screenType === 'answerSection') {
            answerSection.classList.add('backward');
            //   showAnswerSection();
            showMainSection();
            showPrevData();
            return;
        }

        if (screenType === 'pastIssueSection') {
            //   pastIssueSection.classList.add('backward');
            //   backNav.style.display = 'none';
            showMainSection();
            showPrevData();
            return;
        }


        if (platformSdk.appData.sourceName == "bot_notif") {
            showPastIssueSection(pastIssueObj)
            categoryTitle.innerText = pastIssueObj.text;
            backNav.style.display = 'block';
            pastIssuePageSize = 1;
            getPastIssues(null, pastIssuePageSize, getPastIssueList);
        }

        if (screenType === 'pastIssueDetails' && isFromCbot) {
            console.log('handle for cbot packet');
            showissueListPage();
            // platformSdk.bridge.allowBackPress('false');
            // platformSdk.bridge.allowUpPress('false');
            categoryTitle.innerText = pastIssueObj.text;

        } else {
            pastIssueList.classList.add('backward');
            conversationPageSize = 1;
            pastIssuePageSize = 1;
            console.log(screenType);
            console.log(breadCrumb.text.substring(0, breadCrumb.text.lastIndexOf('=>')));
            categoryTitle.innerText = pastIssueObj.text;
            showissueListPage();
            // showPrevData();
            return;
        }



    };

    var showPrevData = function() {
        console.log(screenType);
        console.log(breadCrumb.text);
        var nodeLength = breadCrumb.text.split('=>').length;
        console.log(nodeLength);
        // if (nodeLength == 1 || nodeLength == 2) {
        //     themeSelected = '';
        //     prioritySelected = '';
        // }
        breadCrumb.text = breadCrumb.text.substring(0, breadCrumb.text.lastIndexOf('=>'));
        if (breadCrumb.text.indexOf('=>') == -1 && isFromCbot) backNav.style.display = 'none';
        if (breadCrumb.text.indexOf('=>') == -1) {
            console.log('i am inside d');
            console.log(screenType);
            if (screenType == 'mainSection' && isFromReportedIssueDeeplink) {
                PlatformBridge.closeWebView();
            }
            if (screenType == 'pastIssueSection') {
                platformSdk.bridge.allowBackPress('true');
                platformSdk.bridge.allowUpPress('true');
            } else {
                platformSdk.bridge.allowBackPress('false');
                platformSdk.bridge.allowUpPress('false');
            }

            heroImg.style.display = 'block';
            mainTitle.style.display = 'block';
            if (screenType != "pastIssueSection") {
                backNav.classList.remove('back-nav-visible');

            }

            if (screenType == "pastIssueSection") {
                categoryTitle.innerText = "Reported issues";
            }
            mainCard.classList.remove('back-nav-visible');
        } else {
            console.log('i am inside e');
            categoryTitle.innerText = breadCrumb.text.substring(breadCrumb.text.lastIndexOf('=>') + 2);
        }

        console.log(breadCrumb.list);
        console.log(screenType);
        console.log('isFromDeepLink:', isFromDeepLink);

        if (breadCrumb.list.length == 2 && isFromDeepLink) {
            console.log('*************exit the app to wallet**********');
            platformSdk.bridge.allowBackPress('false');
            platformSdk.bridge.allowUpPress('false');
        }
        console.log(breadCrumb.list[breadCrumb.list.length - 1]);
        if (!breadCrumb.list[breadCrumb.list.length - 1]) backNav.style.display = 'none';

        updateListInHtml(breadCrumb.list[breadCrumb.list.length - 1]);
    };

    var isHikePayIssue = function(breadCrumb) {
        return breadCrumb.text.indexOf("Report a Hike Pay Issue") != -1 ? 1 : 0
    }

    var getFormattedText = function(node_name) {
        node_name = node_name.toLowerCase();
        node_name = node_name.replace(/[^\w\s]/gi, ' '); // convert al special charaters to space
        node_name = node_name.replace(/[^A-Z0-9]+/ig, "_"); // convert all spaces with _
        return node_name;
    }

    var submitFeedback = function(ele, feedBack) {
        console.log('inside submitFeedback');
        console.log(feedBack);
        console.log(breadCrumb);

        feedBack.summary = breadCrumb.text.substring(breadCrumb.text.indexOf('=>') + 2);
        feedBack.summary = appName ? appName + '=>' + feedBack.summary : feedBack.summary;

        analyticsLog = {
            mapp_vs: microAppVersion,
            ek: 'submit_click',
            topic_path: feedBack.summary,
            file_count: getFIleUploadCount,
            desc_text: feedBackData.description ? 'yes' : 'no'
        };


        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);


        if (feedBack.summary.indexOf("Report an Issue") != -1) {
            if (!feedBack.logkey) {
                feedBack.logkey = "";
            }
            try {
                platformSdk.nativeReq({
                    fn: 'getLogsToSend',
                    ctx: this,
                    data: feedBack.logkey,
                    success: function(filePath) {

                        filePath = decodeURIComponent(filePath);
                        console.log(filePath);
                        if (!filePath) {
                            submitFinal(ele, feedBack);
                            return;
                        }
                        var uploadFileData = {
                            filePath: filePath,
                            uploadUrl: platformSdk.appData.helperData.serverUrls.uploadFile,
                            doCompress: true
                        };
                        console.log('upload file req');
                        platformSdk.nativeReq({
                            ctx: self,
                            fn: 'uploadFile',
                            data: platformSdk.utils.validateStringifyJson(uploadFileData),
                            success: function(fileUrl) {
                                fileUrl = decodeURIComponent(fileUrl);
                                console.log('upload file res');
                                console.log(fileUrl);
                                if (fileUrl.indexOf("://") != -1) {
                                    feedBack.description += '\n';
                                    feedBack.description += fileUrl;
                                }
                                submitFinal(ele, feedBack);
                            }
                        });

                    }
                });
            } catch (e) {
                submitFinal(ele, feedBack);
            }
        } else {
            submitFinal(ele, feedBack);
        }

    };
    var submitFinal = function(ele, feedBack) {

        console.log(serverUrls.createIssue);
        console.log(feedBack);
        // console.log(deepLinkBreadCrub);
        if (deepLinkBreadCrub) {
            console.log('attach deep lined bredcrub');
            var breadCrumbString = deepLinkBreadCrub + '=>' + feedBack.summary;
            // console.log(breadCrumbString);
            // remove duplicates strings
            var uniqueBreadCrumbString = breadCrumbString.split('=>').filter(function(item, i, allItems) {
                return i == allItems.indexOf(item);
            }).join('=>');
            console.log(uniqueBreadCrumbString);
            feedBack.summary = uniqueBreadCrumbString
        }
        // anlytics for final submit
        var targetText = categoryTitle.innerHTML;
        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['fa'] = 'issue_create_final_submit_btn_clicked';
        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
        analyticsLogObj['s'] = feedBack.description ? 1 : 0;
        analyticsLogObj['v'] = feedBack.attachments ? feedBack.attachments.length : 0;
        analyticsLogObj['d'] = feedBack.summary;

        console.log('anlytics for final submit');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        //txnId

        if (!feedBack.description || !feedBack.description.match(/[a-z]/i)) {
            submitBtn.classList.remove('progress');
            platformSdk.ui.showToast('Please enter description to raise a issue');
            return;
        }
        if (isFromDeepLink) {

            platformSdk.appData.extra_data = typeof(platformSdk.appData.extra_data) == "object" ? platformSdk.appData.extra_data : JSON.parse(platformSdk.appData.extra_data);
            feedBack.txnId = platformSdk.appData.extra_data.data.transactionId ? platformSdk.appData.extra_data.data.transactionId : '';
            feedBack.deepLinkId = platformSdk.appData.extra_data.deepLinkId ? platformSdk.appData.extra_data.deepLinkId : '';
        }
        delete feedBack.logkey;
        var data = JSON.stringify({
            url: serverUrls.createIssue,
            params: feedBack
        });



        console.log(isFromDeepLink);
        console.log(platformSdk.appData.extra_data);
        console.log('create issue req');
        console.log(JSON.parse(data));

        platformSdk.nativeReq({
            fn: 'doPostRequest',
            ctx: this,
            data: data,
            success: function(res) {
                res = decodeURIComponent(res);
                try {
                    res = JSON.parse(res);
                    var response = JSON.parse(res.response);
                    console.log('create issue res');
                    console.log(response);

                    if (ele) {
                        ele.classList.remove('progress');
                    }
                    if (response.stat === 'ok') {
                        showThankSection(response.tid, feedBack);

                        analyticsLog = {
                            mapp_vs: microAppVersion,
                            ek: 'Submit_success',
                            topic_path: feedBack.summary,
                            jira_id: response.issue_id,
                            file_count: getFIleUploadCount,
                            desc_text: feedBack.description ? 'yes' : 'no'
                        };


                        // anlytics for response after final submit
                        var targetText = categoryTitle.innerHTML;
                        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
                        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
                        analyticsLogObj['fa'] = 'response_from_final_submit_btn_clicked';
                        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
                        analyticsLogObj['s'] = feedBack.description ? 1 : 0;
                        analyticsLogObj['v'] = feedBack.attachments ? feedBack.attachments.length : 0;
                        analyticsLogObj['d'] = feedBack.summary;
                        analyticsLogObj['b'] = response.tid;

                        console.log('anlytics for response after final submit');
                        console.log(analyticsLogObj);
                        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

                        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);


                    } else {
                        platformSdk.ui.showToast('Some error occured, please try again');

                        analyticsLog = {
                            mapp_vs: microAppVersion,
                            ek: 'Submit_failure',
                            topic_path: feedBack.summary,
                            failure_code: code,
                            file_count: getFIleUploadCount,
                            desc_text: feedBack.description ? 'yes' : 'no'
                        };


                        if (ele) {
                            showMaiLink();
                        }

                        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);

                    }
                } catch (e) {
                    console.log(e);
                    submitBtn.classList.remove('progress');
                    platformSdk.ui.showToast('Some error occured, please try again');
                }
            }
        });
    };
    var showMaiLink = function() {
        var mailLinkContainer = document.getElementById('mail-link-container');
        if (mailLinkContainer.style.display === 'block')
            return;

        mailLinkContainer.style.display = 'block';
        var mailLink = mailLinkContainer.getElementsByTagName('a')[0];
        mailLink.addEventListener('click', sendMail);

    };


    var hideMaiLink = function() {
        var mailLinkContainer = document.getElementById('mail-link-container');
        if (mailLinkContainer.style.display === 'none')
            return;

        mailLinkContainer.style.display = 'none';
        var mailLink = mailLinkContainer.getElementsByTagName('a')[0];
        mailLink.removeEventListener('click', sendMail);
    };

    var sendMail = function() {
        PlatformBridge.sendEmail('', feedbackText.value, '');
        analyticsLog = {
            mapp_vs: microAppVersion,
            ek: 'cs_mail_click',
            topic_path: feedBackData.summary
        };

        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);

    };

    var pickFileToUpload = function(ele, callback) {
        console.log(ele);
        console.log(ele.className);
        var self = this;
        platformSdk.nativeReq({
            ctx: self,
            fn: 'chooseFile',
            success: function(fileUrl) {
                ele.classList.remove('chhose-file-in-progress');
                var fileData = JSON.parse(decodeURIComponent(fileUrl));

                if (!(fileData || fileData.filePath)) {
                    platformSdk.ui.showToast('Not able to upload file, Please try again');
                    return;
                }
                if (fileData.filesize > ALLOWED_FILE_SIZE) {
                    platformSdk.ui.showToast('Sorry, files more than ' + platformSdk.appData.helperData.maxFileSize + 'MB can not be uploaded');
                    return;
                }
                if (allowedFileTypes.indexOf(fileData.mimeType) === -1) {
                    platformSdk.ui.showToast('This file type is not supported, Please try again with a valid file type');
                    return;
                }

                if (ele.classList.contains('uploaded')) {
                    deleteImage(ele);
                }

                if (ele.className == "attachment-content") callback(fileData.filePath)
                else {
                    uploadFile(ele, fileData.filePath, true, function() {});
                    addImage(ele, fileData.filePath);
                }
            }
        });
    };

    var uploadFile = function(ele, filePath, doCompress, callback) {
        console.log(filePath);
        var self = this;

        var uploadFileData = {
            filePath: filePath,
            uploadUrl: serverUrls.uploadFile,
            doCompress: doCompress
        };

        console.log('file upload req');
        console.log(JSON.stringify(uploadFileData));
        uploadCount++;
        //submitBtn.setAttribute('disabled', true);
        submitBtn.classList.add('disabled');

        platformSdk.nativeReq({
            ctx: self,
            fn: 'uploadFile',
            data: platformSdk.utils.validateStringifyJson(uploadFileData),
            success: function(fileUrl) {
                console.log('file upload res');
                console.log(fileUrl);
                console.log(JSON.stringify(fileUrl));
                fileUrl = decodeURIComponent(fileUrl);
                var parsedFilePath = JSON.parse(fileUrl);
                console.log(parsedFilePath.path);
                callback(parsedFilePath.path)
                if (ele.classList.contains('uploading')) {
                    ele.classList.remove('uploading');
                    var img = ele.querySelector('img');
                    if (uploadCount > 0)
                        uploadCount--;
                    if (!uploadCount) {
                        //submitBtn.removeAttribute('disabled');
                        submitBtn.classList.remove('disabled');
                    }
                    fileUrl = decodeURIComponent(fileUrl);
                    if (!fileUrl) {
                        img.src = basePath + 'assets/images/plus.png';
                        platformSdk.ui.showToast('Not able to upload file, Please try again');
                        checkConnection();
                        return;
                    } else {
                        var parsedFilePath = JSON.parse(fileUrl);
                        console.log(parsedFilePath.path);
                        ele.classList.add('uploaded');
                        fileUploadList.push(parsedFilePath.path);
                        img.setAttribute('data-file-idx', fileUploadList.length - 1);
                        enableNextAttachment(ele);
                    }

                }
            }
        });
    };

    var enableNextAttachment = function() {
        var children = attachContainer.querySelector('.attachment-row').children;
        var childrenCount = children.length;
        var i = 0;
        while (i < childrenCount) {
            var child = children[i];
            if (child.classList.contains('disabled')) {
                child.classList.remove('disabled');
                break;
            }
            i++;
        }
    };

    var disableNextAttachment = function() {
        var children = attachContainer.querySelector('.attachment-row').children;
        var childrenCount = children.length;
        var i = 0;
        var oneEnabled = false;

        while (i < childrenCount) {
            var child = children[i];
            i++;

            if (child.classList.contains('uploading') || child.classList.contains('uploaded')) {
                continue;
            }
            if (!child.classList.contains('disabled') && oneEnabled) {
                child.classList.add('disabled');
                break;
            } else {
                oneEnabled = true;
            }
        }
    };

    var deleteImage = function(ele) {
        var img = ele.querySelector('img'),
            fileIdx = img.getAttribute('data-file-Idx');


        if (ele.classList.contains('uploading')) {
            ele.classList.remove('uploading');
            if (uploadCount > 0)
                uploadCount--;
            if (!uploadCount) {
                //submitBtn.removeAttribute('disabled');
                submitBtn.classList.remove('disabled');
            }
        }

        // fileUploadList[fileIdx] = null;
        delete fileUploadList[fileIdx];
        fileUploadList.length--;
        ele.classList.remove('uploaded');
        img.src = basePath + 'assets/images/plus.png';

        disableNextAttachment();

    };

    var addImage = function(ele, filePath) {
        var img = ele.querySelector('img');

        img.src = '';
        img.src = 'file:///' + filePath;
        ele.classList.add('uploading');

    };


    var getFIleUploadCount = function() {
        var count = 0;
        var len = fileUploadList.length;
        if (len) {
            for (var i = 0; i < len; i++) {
                if (fileUploadList[i])
                    count++;
            }
        }
        return count;
    }

    var backToHome = function(e) {

        deepLinkBreadCrub = '';

        analyticsLog = {
            mapp_vs: microAppVersion,
            ek: 'goto_help_home_clicked',
        };

        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);


        showMainSection();
        breadCrumb.list.length = 1;
        screenType === 'mainSection';
        breadCrumb.text = breadCrumb.text.substring(0, breadCrumb.text.indexOf('=>'));
        platformSdk.bridge.allowBackPress('false');
        platformSdk.bridge.allowUpPress('false');

        mainTitle.style.display = 'block';
        heroImg.style.display = 'block';
        backNav.classList.remove('back-nav-visible');
        mainCard.classList.remove('back-nav-visible');
        categoryListElem.classList.remove('forward');
        categoryListElem.classList.add('backward');

        updateListInHtml(breadCrumb.list[0]);
    };


    var getDataFromCache = function(key) {
        platformSdk.nativeReq({
            fn: 'getFromCache',
            ctx: this,
            data: key,
            success: function(cachedData) {
                console.log(cachedData);
            }
        });
    }

    var getCachedData = function(callback) {
        platformSdk.nativeReq({
            fn: 'getLargeDataFromCache',
            ctx: this,
            success: function(cachedData) {
                if (cachedData) {
                    cachedData = JSON.parse(decodeURIComponent(cachedData));
                    if (!platformSdk.appData.helperData.isDataSavedLog) {
                        cachedData = window.catData;
                        platformSdk.appData.helperData.isDataSavedLog = 1;
                        platformSdk.updateHelperData(platformSdk.appData.helperData);
                        platformSdk.bridge.putLargeDataInCache(platformSdk.utils.validateStringifyJson(cachedData));

                    }
                    console.log("cachedData parsed");
                    console.log(cachedData);
                    latestPullDataArray = cachedData;
                    // console.log(JSON.stringify(cachedData));

                    if (Object.keys(cachedData).length == 0) {
                        traverseTree(window.catData)
                        callback(window.catData)
                    } else {
                        traverseTree(cachedData)
                        callback(cachedData)
                    }
                } else {
                    console.log("window.catData");
                    console.log(window.catData);
                    cachedData = window.catData;
                    callback(window.catData)
                }

            }
        });
    }


    var setCategoryData = function() {
        showLoader();
        console.log(isFromDeepLink);
        if (!isFromDeepLink) hideLoader();

        console.log('2. inside setCategoryData');
        console.log(platformSdk.getLatestNotifData());
        var data;
        // var latestNotifData = platformSdk.getLatestNotifData();
        var notifData = platformSdk.appData.notifData;

        analyticsLogObj["c"] = "help_home";
        analyticsLogObj["o"] = "help_home";
        analyticsLogObj["fa"] = "screen_open";
        analyticsLogObj['g'] = '';
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = '';
        analyticsLogObj['ra'] = '';
        analyticsLogObj['b'] = '';

        if (notifData && Object.keys(notifData).length) {
            isFromNotification = true;
        }
        console.log('app init data');
        console.log(appInitData);
        console.log(notifData);

        if (platformSdk.appData.helperData.isFromCbot) {
            isFromCbot = true;
            console.log('##########show app from cbot');
            // var pastIssueObj = {
            //     active: false,
            //     nextScreen: 5,
            //     subCat: [],
            //     text: 'Reported issues',
            //     type: 'pastIssue'
            // }
            showPastIssueSection(pastIssueObj)
            categoryTitle.innerText = pastIssueObj.text;
            backNav.style.display = 'block';
            pastIssuePageSize = 1;
            getPastIssues(null, pastIssuePageSize, getPastIssueList);

            // pastIssueSectionFromCbot.style.display = 'block'

            return;
        }


        if (appInitData.sourceName && appInitData.sourceName === "Conversation") {
            console.log('%%%%%%%%%%%% show past issues from chat bot %%%%%%%%%%%');
            isFromReportedIssueDeeplink = true;
            showPastIssueSection(pastIssueObj)
            backNav.style.display = 'block';
            categoryTitle.innerText = pastIssueObj.text;
            pastIssuePageSize = 1;
            getPastIssues(null, pastIssuePageSize, getPastIssueList);
        }

        if (appInitData.sourceName === "bot_notif" && notifData) {
            console.log('^^^^^^^^^^^show agent reply page^^^^^^^^^^^^^^');
            console.log(notifData);
            if (Object.keys(notifData).length > 1) {
                showPastIssueSection(pastIssueObj)
                backNav.style.display = 'block';
                categoryTitle.innerText = pastIssueObj.text;
                pastIssuePageSize = 1;
                getPastIssues(null, pastIssuePageSize, getPastIssueList);

            } else {
                for (var key in notifData) {
                    if (notifData.hasOwnProperty(key)) {
                        currentIssue = notifData[key]
                        getIssueDetails(notifData[key]);
                        console.log(notifData[key]);
                        console.log(key + " -> " + notifData[key]);
                    }
                }
                // console.log(latestNotifData);
                // currentIssue = latestNotifData;
                backNav.classList.add('back-nav-visible');
                mainCard.classList.add('back-nav-visible');
                // getIssueDetails(latestNotifData);
            }

            platformSdk.bridge.deleteAllNotifData();

        } else {
            getCachedData(function(cachedData) {
                console.log(cachedData);
                // traverseTree(cachedData)
                if (Object.keys(cachedData).length) {
                    addSubCategories({
                        subCat: cachedData
                    }, isFromReportedIssueDeeplink ? 'Reported Issues' : 'how can we help?');
                    return;
                } else {
                    addSubCategories({
                        subCat: window.catData
                    }, isFromReportedIssueDeeplink ? 'Reported Issues' : 'how can we help?');
                }
            });
            analyticsLogObj['v'] = 'settings';
        }
        console.log('analytics for home screen source');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        // if (notifData && notifData.type == "agentReply") {
        if (!isFromReportedIssueDeeplink) PlatformBridge.hideNativeLoader();
        console.log('****isFromReportedIssueDeeplink *****', isFromReportedIssueDeeplink);

    };

    var showDataFromAnotherApp = function(extraData) {
        // hideLoader();
        PlatformBridge.hideNativeLoader();
        appName = extraData.appName;
        if (extraData.title)
            mainTitle.innerText = extraData.title;
        addSubCategories({
            subCat: extraData.data
        }, extraData.title ? extraData.title : 'how can we help?');
    };

    var showDataFromDeepLinkId = function(deepLinkId) {
        console.log(deepLinkId);
        categoryListElem.innerHTML = '';
        getCachedData(function(cachedData) {

            if (cachedData.length == 0) cachedData = window.catData;

            var deepLinkArray = platformSdk.appData.helperData.deepLinkPathObj[deepLinkId];
            console.log(deepLinkArray);
            var deepLinkFinalObj = {};
            deepLinkArray.forEach(function(item, i) {
                console.log(item);
                if (i === 0) {
                    console.log(cachedData[item]);
                    deepLinkFinalObj = cachedData[item];
                    if (typeof(cachedData[item]) == "object" && cachedData[item].hasOwnProperty('text')) {
                        console.log('add breadCrumb');
                        deepLinkBreadCrub = cachedData[item].text;
                    }
                    // if (cachedData[item].hasOwnProperty('deepLinkId')) {
                    //
                    // }

                } else {
                    try {
                        deepLinkFinalObj = deepLinkFinalObj[item];
                        console.log(deepLinkFinalObj);
                        console.log(typeof deepLinkFinalObj);
                        if (typeof deepLinkFinalObj == "object" && deepLinkFinalObj.hasOwnProperty('text')) {
                            console.log('add breadCrumb');
                            deepLinkBreadCrub = deepLinkBreadCrub + '=>' + deepLinkFinalObj.text;
                        }
                    } catch (e) {
                        console.log('******* some error occured in creating sumary breadCrumb **********');
                    }
                    // deepLinkBreadCrub + '=>' + deepLinkFinalObj[item].text;
                }
            })

            console.log(deepLinkBreadCrub);
            console.log(deepLinkFinalObj);
            console.log(cachedData);
            addSubCategories(deepLinkFinalObj, deepLinkFinalObj.text);
            hideLoader();
            // if (Object.keys(cachedData).length) {
            //     addSubCategories(deepLinkCategory, deepLinkCategory.text);
            //     return;
            // }
        });
    }

    // traverse though array of objects;
    var traverseTree = function(o, parents_theme, parents_priority) {
        var parent = {};
        for (var item in o) {
            if (!!o[item] && typeof(o[item]) == "object") {
                parent = o[item];
                var theme = parents_theme;
                var priority = parents_priority;

                if (o[item].hasOwnProperty('theme')) theme = o[item].theme;
                else o[item].theme = parents_theme;

                if (o[item].hasOwnProperty('priority')) priority = o[item].priority;
                else o[item].priority = parents_priority;

                traverseTree(o[item], theme, priority);
                if (o[item].hasOwnProperty('deepLinkId')) {
                    createDeepLinkedPath(o, o[item]['deepLinkId']);
                }
            }
        }
    }


    // platformSdk.bridge.putLargeDataInCache(platformSdk.utils.validateStringifyJson(parsedJSON));
    // platformSdk.bridge.updateHelperData(platformSdk.utils.validateStringifyJson(helperData));

    var deepLinkPathObj = {};
    var createDeepLinkedPath = function(cachedData, deepLinkValue) {
        console.log(latestPullDataArray);
        if (latestPullDataArray.length == 0) latestPullDataArray = window.catData;
        var pathArray = [];
        var arrayPath = ""
        var path = getPath(latestPullDataArray, 'deepLinkId', deepLinkValue); // output will be path = "0.1.2.subCat.0.1.2.subCat.0.subCat.0.1.2.isDeepLinked"
        // console.log(path);
        path = path.split(".");
        if (path[path.length - 1] == 'deepLinkId') path.splice(-1, 1);
        if (path[path.length - 1] == 'subCat') path.splice(-1, 1);

        path = path.join('');
        path = path.split('subCat');
        path.forEach(function(item, index) {
            item = item.length - 1;
            if (index === 0) {
                pathArray.push(item)
                // arrayPath = arrayPath+"["+item+"]";
            } else {
                pathArray.push('subCat');
                pathArray.push(item);
                // arrayPath = arrayPath+"['subCat']["+item+"]";
            }
        });
        deepLinkPathObj[deepLinkValue] = pathArray;

        // deepLinkPathObj[deepLinkValue] = arrayPath;
        platformSdk.appData.helperData.deepLinkPathObj = deepLinkPathObj;
        platformSdk.updateHelperData(platformSdk.appData.helperData);

        // })

        // eval(arrayPath)
    }

    function getPath(obj, key, value, path) {

        if (typeof obj !== 'object') {
            return;
        }

        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                var t = path;
                var v = obj[k];
                if (!path) {
                    path = k;
                } else {
                    path = path + '.' + k;
                }
                if (v === value) {
                    if (key === k) {
                        return path;
                    } else {
                        path = t;
                    }
                } else if (typeof v !== 'object') {
                    path = t;
                }
                var res = getPath(v, key, value, path);
                if (res) {
                    return res;
                }
            }
        }

    }


    var categorySearch = platformSdk.utils.debounce(function(e) {

        if (endSearch) return;

        var val = searchField.value.toLowerCase();
        var currentCategoryList = breadCrumb.list[breadCrumb.list.length - 1];
        var filteredList = [];
        for (var i = 0; i < currentCategoryList.length; i++) {
            var item = currentCategoryList[i];
            item.index = i;
            if (item.text.toLowerCase().indexOf(val) > -1) {
                filteredList.push(item);
            }
        }

        updateListInHtml(filteredList, true);
    }, 300);

    var setGlobals = function() {
        basePath = document.body.getAttribute('data-base-path');
        platformSdk.appData.helperData.maxFileSize = platformSdk.appData.helperData.maxFileSize || 25;
        ALLOWED_FILE_SIZE = parseInt(platformSdk.appData.helperData.maxFileSize) * 1024 * 1024;

        serverUrls = platformSdk.appData.helperData.serverUrls;
        allowedFileTypes = platformSdk.appData.helperData.allowedTypes;

        feedBackData.appVersion = platformSdk.appData.appVersion;
    };

    var logSearchClick = function(e) {
        endSearch = false;
        analyticsLog = {
            mapp_vs: microAppVersion,
            ek: 'search_click',
            parent_topic: parentTopic,
            level: breadCrumb.list.length - 1
        };


        if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);

        var logSearchClickOnlyOnce = function() {

            analyticsLog = {
                mapp_vs: microAppVersion,
                ek: 'search_init',
                parent_topic: parentTopic,
                level: breadCrumb.list.length - 1
            };

            if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);


            this.removeEventListener('keyup', logSearchClickOnlyOnce);

        };

        this.addEventListener('keyup', logSearchClickOnlyOnce);
    };

    var submitHandler = function(e) {

        // analytics for ticket creation button
        var targetText = categoryTitle.innerHTML;
        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['fa'] = 'issue_create_btn_clicked';
        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = ''; // count for open issues
        analyticsLogObj['ra'] = ''; // count for closed issues
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = '';

        console.log('analytics for ticket creation button');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        checkConnection();
        if (this.classList.contains('disabled')) {
            return;
        }
        this.classList.add('progress');
        feedBackData.description = feedbackText.value;
        if (fileUploadList.length) {
            feedBackData.attachments = fileUploadList;

            var tempList = [];
            for (var i = 0; i < fileUploadList.length; i++) {
                if (fileUploadList[i]) {
                    tempList.push(fileUploadList[i]);
                }
            }
        }
        platformSdk.nativeReq({
            ctx: this,
            fn: 'checkConnection',
            success: function(netType) {
                if (netType > 0) {
                    submitFeedback(this, feedBackData);
                } else {
                    submitBtn.classList.add('disabled');
                }
            }
        });
    };

    var showSearchField = function() {
        var container = document.getElementById('title-search-icon-container');
        backNav.classList.add('searching');
        container.style.display = 'none';
        searchFieldContainer.style.display = 'inline-block';
        searchField.value = '';
        searchField.focus();
    };

    var hideSearchField = function() {
        var container = document.getElementById('title-search-icon-container');
        backNav.classList.remove('searching');
        container.style.display = 'inline-block';
        searchFieldContainer.style.display = 'none';
    };

    var setDataCallback = platformSdk.events.subscribe('webview/data/loaded', function(e) {

        console.log('1. inside setDataCallback');
        appInitData = platformSdk.appData;

        //   PlatformBridge.hideNativeLoader();

        try {
            var extraData = JSON.parse(appInitData.extra_data);
            if (extraData.deepLinkId && extraData.deepLinkId === 'reported_issues') isFromReportedIssueDeeplink = true;
            if (!extraData.deepLinkId) {
                console.log('################# show the main section #################');
                //   isFromReportedIssueDeeplink = true;
                mainCard.style.display = 'block';
            }
            //   showLoader();
        } catch (e) {
            mainCard.style.display = 'block';
        }
        platformSdk.appData.helperData.tempNotifData = platformSdk.appData.notifData;
        platformSdk.updateHelperData(platformSdk.appData.helperData);

        // analytics for home screen open
        analyticsLogObj["c"] = "help_home";
        analyticsLogObj["o"] = "help_home";
        analyticsLogObj["fa"] = "screen_open";
        analyticsLogObj['g'] = '';
        analyticsLogObj['s'] = '';
        analyticsLogObj['ra'] = '';
        analyticsLogObj['b'] = '';

        // PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj))

        // platformSdk.utils.logAnalytics("true", "click", analyticsLogObj);  //platformSdk.utils.logAnalytics(isUI, type, analyticEvents)


        var appData = platformSdk.appData,
            helperData = appData.helperData,
            extraData;

        setGlobals();
        console.log(appData);
        // if (appData.extra_data) {
        //     extraData = appData.extra_data;
        //     try {
        //            extraData = JSON.parse(extraData);
        //            console.log(extraData);
        //            if (extraData.deepLinkId) {
        //                isFromDeepLink = true;
        //            }
        //        } catch (error) {
        //            console.log("error in extraData parsing: ", error);
        //        }
        // }
        if (appData.extra_data) {
            extraData = appData.extra_data;
            try {
                extraData = JSON.parse(extraData);
                console.log(extraData);
            } catch (error) {
                console.log("error in extraData parsing: ", error);
            }
            if (extraData && extraData.data && Object.keys(extraData.data).length) {
                // showDataFromAnotherApp(extraData);
                if (extraData.deepLinkId) {
                    isFromDeepLink = true;
                    analyticsLogObj['v'] = 'deeplinking';
                    analyticsLogObj['f'] = extraData.deepLinkId;
                    console.log('analytics for home screen source deepLinkId');
                    console.log(analyticsLogObj);
                    PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));
                    // showDataFromDeepLinkId(extraData.deepLinkId);
                } else {
                    showDataFromAnotherApp(extraData);
                    return;
                }

                if (extraData.data) deepLinkedData = extraData.data;
            }
        }

        console.log(helperData.pullVersionUrl);

        if (helperData.pullVersionUrl) {
            console.log('pull version req');
            console.log(helperData.pullVersionUrl);
            showLoader();

            platformSdk.nativeReq({
                fn: 'doGetRequest',
                ctx: this,
                data: helperData.pullVersionUrl,

                success: function(response, code) {
                    response = JSON.parse(decodeURIComponent(response));
                    console.log('pull version res');
                    console.log(response);
                    if (response.status == "success") {
                        var data = JSON.parse(response.response);
                        if (data) {
                            // showLoader();
                            var version = data.version;
                            console.log(version);
                            console.log(helperData.version);
                            var pullDataUrl = data.data_path;
                            if (!helperData.version)
                                helperData.version = 0;

                            if (version != helperData.version) {
                                showLoader();
                                console.log('pull data req')
                                console.log(pullDataUrl);
                                platformSdk.nativeReq({
                                    fn: 'doGetRequest',
                                    ctx: this,
                                    data: pullDataUrl,
                                    success: function(response, code) {
                                        response = JSON.parse(decodeURIComponent(response));
                                        console.log(response);
                                        if (response.status == "success") {
                                            console.log("pull data url res");
                                            console.log(response.response);
                                            console.log(JSON.parse(response.response));
                                            var parsedJSON = JSON.parse(response.response);
                                            if (response.response) {
                                                latestPullDataArray = parsedJSON;
                                                traverseTree(parsedJSON);

                                                console.log(parsedJSON);
                                                addSubCategories({
                                                    subCat: parsedJSON
                                                }, 'how can we help?');
                                                showPrevData();

                                                // if (isFromDeepLink) {
                                                //     console.log('%%%%%%%%%%% get data from deeplink %%%%%');
                                                //     getDataFromOtherApp(appData)
                                                // }

                                                getDataFromOtherApp(appData)

                                                // if (isFromReportedIssueDeeplink) {
                                                //     console.log('%%%%%%%%%%% get reported issue data %%%%%');
                                                // }

                                                platformSdk.bridge.putLargeDataInCache(platformSdk.utils.validateStringifyJson(parsedJSON));
                                                helperData.version = version;
                                                platformSdk.bridge.updateHelperData(platformSdk.utils.validateStringifyJson(helperData));

                                                hideLoader();
                                            }
                                        } else {
                                            console.log('********** handle offline deeplinking ***********');
                                            var parsedJSON = window.catData;

                                            latestPullDataArray = parsedJSON;
                                            traverseTree(parsedJSON);

                                            addSubCategories({
                                                subCat: parsedJSON
                                            }, 'how can we help?');
                                            showPrevData();

                                            // if (isFromDeepLink) {
                                            //     console.log('%%%%%%%%%%% get data from deeplink %%%%%');
                                            //     getDataFromOtherApp(appData)
                                            // }

                                            getDataFromOtherApp(appData)

                                            // if (isFromReportedIssueDeeplink) {
                                            //     console.log('%%%%%%%%%%% get reported issue data %%%%%');
                                            // }

                                            platformSdk.bridge.putLargeDataInCache(platformSdk.utils.validateStringifyJson(parsedJSON));
                                            helperData.version = version;
                                            platformSdk.bridge.updateHelperData(platformSdk.utils.validateStringifyJson(helperData));

                                            hideLoader();
                                        }

                                        var ae = {};
                                        ae["ek"] = "micro_app";
                                        ae["event"] = "cs_file_download";
                                        ae["fld1"] = response.status;
                                        ae["fld5"] = response.status_code;
                                        platformSdk.utils.logAnalytics("true", "click", ae);
                                    },
                                    error: function(response, code) {
                                        hideLoader();
                                    }
                                });

                            } else {
                                console.log('%%%%%%%%%%% get deeplink data from cache%%%%%%%%%');
                                getDataFromOtherApp(appData);

                            }
                        }

                    } else hideLoader();

                    var ae = {};
                    ae["ek"] = "micro_app";
                    ae["event"] = "cs_version";
                    ae["fld1"] = response.status;
                    ae["fld5"] = response.status_code;
                    platformSdk.utils.logAnalytics("true", "click", ae);




                },
                error: function(response, code) {
                    hideLoader();
                }
            });
        }

        if (helperData.clearCache === 'true') {
            clearAppCache(helperData)
        }

        setCategoryData();

        platformSdk.nativeReq({
            fn: 'getUserProfile',
            ctx: this,
            success: function(res) {
                res = JSON.parse(decodeURIComponent(res));
                console.log(res);
                deviceName = res.name.replace(/\s/g, '').charAt(0).toUpperCase();
                console.log(deviceName);
            }
        });

        setDataCallback.remove();
    });

    var getDataFromOtherApp = function(appData) {
        console.log(appData);


        if (appData.extra_data) {

            var extraData = appData.extra_data;

            console.log('get data from other app');

            try {
                extraData = JSON.parse(extraData);
                console.log(extraData);
            } catch (error) {
                console.log("error in extraData parsing: ", error);
            }


            if (extraData && extraData.data && Object.keys(extraData.data).length) {
                // showDataFromAnotherApp(extraData);
                if (extraData.deepLinkId) {
                    isFromDeepLink = true;
                    mainCard.style.display = 'block';
                    heroImg.style.display = 'none';
                    mainTitle.style.display = 'none';
                    showDataFromDeepLinkId(extraData.deepLinkId);
                }

                if (extraData.data) deepLinkedData = extraData.data;

                return;
            }
        }
    }

    var getPastIssues = function(target, pageSize, callback) {
        console.log('****isFromReportedIssueDeeplink *****', isFromReportedIssueDeeplink);
        console.log(pageSize);
        var appData = platformSdk.appData,
            helperData = appData.helperData,
            extraData;
        if (target == 'fromIssueList') showSmallLoader();
        else if (!isFromReportedIssueDeeplink) {
            showLoader();
        } else showLoader();
        // showLoader();
        console.log('get past issue req');
        console.log(platformSdk.appData.helperData.serverUrls.createIssue + '?page=' + pageSize);
        platformSdk.nativeReq({
            fn: 'doGetRequest',
            ctx: this,
            data: platformSdk.appData.helperData.serverUrls.createIssue + '?page=' + pageSize,
            success: function(response, code) {
                if (isFromReportedIssueDeeplink) PlatformBridge.hideNativeLoader();
                hideLoader();
                hideSmallLoader();
                console.log(JSON.parse(decodeURIComponent(response)));
                console.log(JSON.stringify(decodeURIComponent(response)));
                response = JSON.parse(decodeURIComponent(response));
                if (response.status == "success") {
                    console.log('get past issue res');
                    response = JSON.parse(response.response)
                    console.log(JSON.stringify(response));
                    response.target = target;
                    callback(response);
                    platformSdk.bridge.deleteAllNotifData();

                } else {
                    hideLoader();
                    hideSmallLoader();
                    platformSdk.ui.showToast('Some error occured, please try again');
                }
                var ae = {};
                ae["ek"] = "micro_app";
                ae["event"] = "cs_file_download";
                ae["fld1"] = response.status;
                ae["fld5"] = response.status_code;
                platformSdk.utils.logAnalytics("true", "click", ae);
            },
            error: function(response, code) {
                console.log(response);
                platformSdk.ui.showToast('Some error occured, please try again');
                hideLoader();
                hideSmallLoader();
            }
        });
    }


    var getConversationList = function(target, issueDetails, pageSize, callback) {
        console.log(issueDetails);
        // showLoader()
        if (target === 'fromIssueConversationList') showConvSmallLoader();
        else showLoader();
        var appData = platformSdk.appData,
            helperData = appData.helperData,
            extraData;

        console.log('get issue conversation req');
        console.log(platformSdk.appData.helperData.serverUrls.createIssue + '/' + issueDetails.id + '/conversation?page=' + pageSize);
        platformSdk.nativeReq({
            fn: 'doGetRequest',
            ctx: this,
            data: platformSdk.appData.helperData.serverUrls.createIssue + '/' + issueDetails.id + '/conversation?page=' + pageSize,
            success: function(response, code) {
                console.log('get issue conversation res');
                console.log(JSON.parse(decodeURIComponent(response)));
                response = JSON.parse(decodeURIComponent(response));

                // hideLoader();
                if (response.status == "success") {
                    console.log("response from past issue api");
                    response = JSON.parse(response.response)
                    console.log(JSON.stringify(response));
                    var conversations = response.conv;
                    var ticketDetails = response.ticket;
                    ticketDetails.attachments = issueDetails.attachments; // attach attachments from issueDetails as it is not coming from response * have to come in response *
                    // issueConversationList.innerHTML = '';
                    if (response.stat == "ok") {
                        callback(ticketDetails, conversations)
                        //createIssueConversionTree(issueDetails, conversations);
                    }

                } else {
                    hideLoader();
                    hideConvSmallLoader();
                    platformSdk.ui.showToast('Some error occured, please try again');
                }
                var ae = {};
                ae["ek"] = "micro_app";
                ae["event"] = "cs_file_download";
                ae["fld1"] = response.status;
                ae["fld5"] = response.status_code;
                platformSdk.utils.logAnalytics("true", "click", ae);


                platformSdk.bridge.deleteAllNotifData();
                platformSdk.appData.helperData.tempNotifData = {};
                platformSdk.updateHelperData(platformSdk.appData.helperData);
            },
            error: function(response, code) {
                console.log(response);
                hideLoader();
                hideConvSmallLoader();
            }
        });
    }

    var containsObject = function(list, obj, key) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i][key] === obj[key]) {
                return true;
            }
        }

        return false;
    }

    // var agentReplyAdded = false;
    var getConversationListCallback = function(issueDetails, list) {


        // analytics for issue chat thread opened
        analyticsLogObj['c'] = 'help_past_issues';
        analyticsLogObj['o'] = 'issue_chat_screen';
        analyticsLogObj['fa'] = 'screen_open';
        analyticsLogObj['g'] = 1;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = ''; // count for open issues
        analyticsLogObj['ra'] = ''; // count for closed issues
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = issueDetails.id;

        console.log('analytics for issue chat thread opened');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        var firstConvObj = {
            attachments: issueDetails.attachments,
            sent: true,
            text: issueDetails.desc,
            ts: issueDetails.created_at,
            isfirstConversation: true
        }


        console.log(conversationArrayList);
        issueConversationList.innerHTML = '';
        console.log(issueDetails);
        console.log(list);

        conversationArrayList = conversationArrayList.concat(list);

        console.log(conversationArrayList);
        console.log(JSON.stringify(conversationArrayList));
        if (list.length < 10) {
            console.log('push first item');
            isEmptyIssueConversationResponse = true;
            conversationArrayList.push(firstConvObj);
        }

        console.log('****** from agent reply ******');
        console.log(conversationArrayList);
        createIssueConversionTree(issueDetails, conversationArrayList);
    }

    var getOpenClosedIssues = function(tickets) {
        var openTickets = [],
            closedTickets = [];
        for (var i = 0; i < tickets.length; i++) {
            if (tickets[i].status == 'Open') {
                openTickets.push(tickets[i]);
            }
            if (tickets[i].status == 'Closed' || tickets[i].status == 'Resolved') {
                closedTickets.push(tickets[i]);
            }
        }

        return {
            'openTickets': openTickets,
            'closedTickets': closedTickets
        }
    }

    Array.prototype.move = function(old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this;
    };

    var getPastIssueList = function(list) {
        if (list.stat == 'fail') {
            platformSdk.ui.showToast('Some error occured, please try again');
            return;
        }
        console.log(list);
        pastIssueArrayList = list.tickets;
        // pastIssueArrayList = [];
        if (pastIssueArrayList.length === 0) isEmptyPastIssueResponse = true;
        var tempNotifData = platformSdk.appData.helperData.tempNotifData;

        for (var i = 0; i < pastIssueArrayList.length; i++) {
            // console.log(pastIssueArrayList[i]);
            for (var key in tempNotifData) {
                if (tempNotifData.hasOwnProperty(key)) {
                    if (pastIssueArrayList[i].id === tempNotifData[key].id) {
                        console.log('@@@ matched obj @@@');
                        tempNotifData[key]['newNotif'] = true
                        console.log(key, tempNotifData[key]);
                        pastIssueArrayList.splice(i, 1);
                        pastIssueArrayList.unshift(tempNotifData[key])

                    }
                }
            }
        }
        if (pastIssueArrayList.length > 0) createUlElement(pastIssueArrayList);
        else if (pastIssueArrayList.length == 0 && list.target == 'fromIssueList') {
            pastIssueSection.style.display = "block";
            // noContent.innerHTML = "You don't have any past issues"
            noContentSection.style.display = 'none';
        } else {
            pastIssueSection.style.display = "none";
            // noContent.innerHTML = "You don't have any past issues"
            noContentSection.style.display = 'block';
        }

        var openClosedTickets = getOpenClosedIssues(list.tickets)
        console.log(openClosedTickets);

        //analytics for response from past issues api
        // var targetText = categoryTitle.innerHTML;
        analyticsLogObj['c'] = 'help_past_issues';
        analyticsLogObj['o'] = 'past_issues_screen';
        analyticsLogObj['fa'] = 'screen_open';
        analyticsLogObj['g'] = 1;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = openClosedTickets.openTickets.length; // count for open issues
        analyticsLogObj['ra'] = openClosedTickets.closedTickets.length; // count for closed issues
        analyticsLogObj['v'] = list.tickets ? list.tickets.length : 0;
        analyticsLogObj['b'] = '';

        console.log('analytics for response from past issues api');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

    }

    var clearAppCache = function(helperData) {
        platformSdk.bridge.putLargeDataInCache('{}');
        helperData.clearCache = 'false';
        platformSdk.bridge.updateHelperData(platformSdk.utils.validateStringifyJson(helperData));
    }


    var addAgentMessage = function(message) {
        console.log(message);
    }

    var appendNewUserMessage = function(message) {

        console.log(message);
        var currentDate = new Date();
        var item = document.createElement('li');
        item.classList.add('conv-class');

        var convListContainer = document.createElement('div')
        var convPostDetailsDiv = document.createElement('div');
        var convDetailsDiv = document.createElement('div');
        var borderDiv = document.createElement('div');
        var img = document.createElement('img');
        var convText = document.createElement('P');
        convText.style['line-height'] = '1';
        img.width = 200;
        img.height = 300;
        convPostDetailsDiv.classList.add('conversation-meta-data');
        convDetailsDiv.classList.add('conversation-description');
        convListContainer.classList.add('conversation-list-container');
        var profilePicSpan = document.createElement('span')
        profilePicSpan.classList.add('profile-circle');
        profilePicSpan.innerHTML = deviceName;
        convPostDetailsDiv.appendChild(profilePicSpan);

        var userSpan = document.createElement('span');
        var timeStampSpan = document.createElement('span');
        userSpan.classList.add('conversation-post-details')

        userSpan.innerHTML = 'Me';
        timeStampSpan.classList.add('conversation-timestamp');
        //  timeStampSpan.innerHTML = ' '+getFormatedDate(currentDate)+' at '+ getFormatedTime(currentDate);
        timeStampSpan.innerHTML = ' ' + getFormatedDate(currentDate);

        convPostDetailsDiv.appendChild(userSpan);
        convPostDetailsDiv.appendChild(timeStampSpan);
        item.classList.add('left-conv');
        //    profilePicSpan.style['padding-left'] = '11px';

        var text = document.createTextNode(message.message);
        convText.appendChild(text)
        convDetailsDiv.appendChild(convText);
        if (message.filePath) {
            img.src = '';
            img.src = 'file:///' + message.filePath;
            convDetailsDiv.appendChild(img)
        }

        convListContainer.appendChild(convPostDetailsDiv);
        convListContainer.appendChild(convDetailsDiv);

        item.appendChild(convListContainer);
        //    item.appendChild(borderDiv);

        issueConversationList.appendChild(item);

        setTimeout(function() {
            lastConversationSpace.scrollIntoView({
                block: "end",
                behavior: "smooth"
            });
        }, 300)

    }

    var sendNewmessage = function(ticketId, message, callback) {
        console.log(ticketId);
        console.log(message);
        var conversationAttachments = []
        if (message.uploadedFile) conversationAttachments.push(message.uploadedFile)
        checkConnection();
        platformSdk.nativeReq({
            ctx: this,
            fn: 'checkConnection',
            success: function(netType) {

                if (netType > 0) {
                    if (message) {
                        // showLoader();
                        var data = JSON.stringify({
                            url: platformSdk.appData.helperData.serverUrls.createIssue + '/' + ticketId + "/reply",
                            params: {
                                "msg": message.message,
                                "attachments": conversationAttachments
                            }
                        });

                        console.log('issue reply req')
                        console.log(data);
                        checkConnection();
                        platformSdk.nativeReq({
                            fn: 'doPostRequest',
                            ctx: this,
                            data: data,
                            success: function(res) {
                                res = decodeURIComponent(res);
                                console.log('issue reply res')
                                console.log(res);
                                try {
                                    res = JSON.parse(res);
                                    var response = JSON.parse(res.response);
                                    console.log(response);
                                    callback(response)

                                } catch (e) {
                                    callback({
                                        clientError: true
                                    })

                                    // console.log(e);
                                }
                            },
                            error: function(response, code) {
                                console.log(response);
                                hideLoader();
                            }
                        });
                    } else hideLoader();
                }
            }
        });
    }

    attachContainer.addEventListener('click', function(e) {
        var target = e.target || e.which;

        if (target.classList.contains('delete-file')) {
            deleteImage(target.parentNode);
            return false;
        }

        if (this.classList.contains('offline')) {
            return;
        }

        if (target.classList.contains('attach-file')) {
            // analytics for attachment btn clicked
            var targetText = categoryTitle.innerHTML;
            analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
            analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
            analyticsLogObj['fa'] = 'attachmemnt_btn_clicked';
            analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
            analyticsLogObj['s'] = '';
            analyticsLogObj['f'] = ''; // count for open issues
            analyticsLogObj['ra'] = ''; // count for closed issues
            analyticsLogObj['v'] = '';
            analyticsLogObj['b'] = '';

            console.log('analytics for attachment btn clicked');
            console.log(analyticsLogObj);
            PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

            target = target.parentNode;
        }

        if (target.classList.contains('disabled') || target.classList.contains('uploading') || target.classList.contains('uploaded')) {
            return;
        }

        if (target.classList.contains('attachment')) {
            console.log(target);
            pickFileToUpload(target);
        }
    });

    messageSection.addEventListener('click', function(e) {
        var target = e.target || e.which;
        console.log(target);
        if (target.classList.contains('attachment-content')) {
            console.log(target);
            pickFileToUpload(target, function(filePath) {
                console.log(filePath);
                if (filePath) {
                    submitMessageBtn.classList.add('sending-message');
                    submitMessageBtn.classList.add('progress');
                }
                uploadFile(target, filePath, true, function(uploadedFile) {
                    console.log(uploadedFile);
                    var ticketId = categoryTitle.getAttribute('data-index');
                    var message = {
                        message: newMessage.value,
                        filePath: filePath,
                        uploadedFile: uploadedFile
                    };
                    submitMessageBtn.classList.remove('sending-message');
                    submitMessageBtn.classList.remove('progress');
                    sendNewmessage(ticketId, message, function(response) {
                        if (response.clientError) {
                            hideLoader();
                            submitMessageBtn.classList.remove('sending-message');
                            submitMessageBtn.classList.remove('progress');
                            platformSdk.ui.showToast('Some error occured, please try again');
                            return;
                        }
                        if (response.stat == 'ok') {
                            // hideLoader();
                            newMessage.value = '';
                            submitMessageBtn.disabled = true;
                            appendNewUserMessage(message)
                            submitMessageBtn.classList.remove('sending-message');
                            submitMessageBtn.classList.remove('progress');

                        } else {
                            platformSdk.ui.showToast('Some error occured, please try again');
                            // hideLoader();
                            submitMessageBtn.classList.remove('sending-message');
                            submitMessageBtn.classList.remove('progress');
                            if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);

                        }
                    })
                });
            });
        }

    });

    searchIcon.addEventListener('click', showSearchField);

    unhappyBtn.addEventListener('click', function(e) {

        //analytics for unhappy btn action
        var targetText = categoryTitle.innerHTML;
        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['fa'] = 'unhappy_btn_clicked';
        analyticsLogObj['ra'] = 'user_not_satisfied';
        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = '';
        analyticsLogObj['ra'] = '';
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = '';

        console.log('analytics for unhappy btn action');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

        console.log(e);
        console.log(selectedListCategory);
        showLastSection(selectedListCategory)
    });

    resolvedBtn.addEventListener('click', function(e) {
        resolvedSection.style.display = 'none';
        messageSection.style.display = 'block';
        var ticketId = categoryTitle.getAttribute('data-index');


        // analytics for User clicks on 'still facing issue' on issue marked as resolved
        analyticsLogObj['c'] = 'help_past_issues';
        analyticsLogObj['o'] = 'issue_thread';
        analyticsLogObj['fa'] = 'click_still_facing_issue';
        analyticsLogObj['g'] = 1;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = '';
        analyticsLogObj['ra'] = '';
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = ticketId;

        console.log('analytics for User clicks on still facing issue on issue marked as resolved');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));

    });

    submitMessageBtn.addEventListener('click', function(e) {

        var ticketId = categoryTitle.getAttribute('data-index');
        var message = {
            message: newMessage.value
        };
        submitMessageBtn.classList.add('sending-message');
        submitMessageBtn.classList.add('progress');
        submitMessageBtn.disabled = true;

        sendNewmessage(ticketId, message, function(response) {
            if (response.clientError) {
                hideLoader();
                submitMessageBtn.classList.remove('sending-message');
                submitMessageBtn.classList.remove('progress');
                platformSdk.ui.showToast('Some error occured, please try again');
                return;
            }
            if (response.stat == 'ok') {
                // hideLoader();
                newMessage.value = '';
                submitMessageBtn.disabled = true;
                appendNewUserMessage(message)
                submitMessageBtn.classList.remove('sending-message');
                submitMessageBtn.classList.remove('progress');

            } else {
                platformSdk.ui.showToast('Some error occured, please try again');
                // hideLoader();
                submitMessageBtn.classList.remove('sending-message');
                submitMessageBtn.classList.remove('progress');
                if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics('true', 'click', analyticsLog);

            }
        })

        // analytics for User enters message (clicks send)
        analyticsLogObj['c'] = 'help_past_issues';
        analyticsLogObj['o'] = 'issue_chat_screen';
        analyticsLogObj['fa'] = 'message_entered';
        analyticsLogObj['g'] = 1;
        analyticsLogObj['s'] = message ? message : '';
        analyticsLogObj['f'] = ''; // count for open issues
        analyticsLogObj['ra'] = ''; // count for closed issues
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = ticketId;

        console.log('analytics for User enters message (clicks send)');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));


    });

    newMessage.onkeyup = function() {
        var allFilled = true;
        if (this.value == '') allFilled = false;
        submitMessageBtn.disabled = !allFilled;
    }

    if (newMessage.value == '') submitMessageBtn.disabled = true;

    happyBtn.addEventListener('click', function(e) {
        console.log('isFromDeepLink', isFromDeepLink);
        //analytics for happy btn action
        var targetText = categoryTitle.innerHTML;
        analyticsLogObj['c'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['o'] = 'help_' + getFormattedText(targetText);
        analyticsLogObj['fa'] = 'happy_btn_clicked';
        analyticsLogObj['ra'] = 'user_satisfied';
        analyticsLogObj['g'] = isHikePayIssue(breadCrumb) ? 1 : 0;
        analyticsLogObj['s'] = '';
        analyticsLogObj['f'] = ''; // count for open issues
        analyticsLogObj['ra'] = ''; // count for closed issues
        analyticsLogObj['v'] = '';
        analyticsLogObj['b'] = '';

        console.log('analytics for happy btn action');
        console.log(analyticsLogObj);
        PlatformBridge.logAnalyticsV2(JSON.stringify(analyticsLogObj));
        if (isFromDeepLink) {
            PlatformBridge.closeWebView();
            return;
        }
        backToHome();
    })

    // happyBtn.addEventListener('click', backToHome);

    backNav.addEventListener('click', function(e) {
        console.log(' html back press clicked');
        if (isFromReportedIssueDeeplink) {

        }
        var target = e.target;
        if (target.classList.contains('search-field-container') || target.classList.contains('search-field') || target.classList.contains('search-icon-container') || target.classList.contains('search-icon')) {

        } else {
            platformSdk.events.publish('onBackPressed');
        }
    });

    categoryListElem.addEventListener('click', function(e) {

        if (backNav.style.display == "none") {
            backNav.style.display = 'block';
        }
        console.log(backNav.style.display);

        console.log('category list clicked');
        var target = e.target;
        console.log(target.textContent);
        console.log(target.nodeName);

        if (target.nodeName == 'UL') return;

        target.textContent = target.textContent.replace(/[0-9]/g, '');
        while (target.parentElement && (target.nodeName !== 'LI')) {
            target = target.parentNode;
            console.log(target);
        }

        if (target.nodeName === 'LI') {
            console.log('go to next category');
            gotoNextCategory(target);
        }

        if (target.hasAttribute("data-next-screen") && target.getAttribute("data-next-screen") === "5") {
            pastIssueList.innerHTML = '';
            pastIssuePageSize = 1;
            console.log(platformSdk.appData.helperData.deepLinkPathObj);
            // getDataFromCache('deepLinkPathObj');
            getPastIssues(target, pastIssuePageSize, getPastIssueList);
        }


    });

    thanksSectionTicketId.addEventListener('click', function() {
        console.log(this.value);
    })


    var docheight = getDocHeight()

    function getDocHeight() {

        var D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        )
    }



    function amountscrolled() {
        var winheight = window.innerHeight || (document.documentElement || document.body).clientHeight
        var docheight = getDocHeight()
        var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop
        var trackLength = docheight - winheight
        var pctScrolled = Math.floor(scrollTop / trackLength * 100)
        return pctScrolled;
    }

    var amountscrolledCount = 0;
    window.addEventListener("scroll", function() {

        if (amountscrolled() == 100 && screenType == 'pastIssueSection' && !isEmptyPastIssueResponse) {
            pastIssuePageSize++;
            // showLoader();
            showSmallLoader();
            getPastIssues('fromIssueList', pastIssuePageSize, getPastIssueList);
        }

        if (amountscrolled() == 0 && screenType == 'pastIssueDetails' && !isEmptyIssueConversationResponse) {
            amountscrolledCount++;
            if (amountscrolledCount === 1) {
                console.log('get prev conversation list');
                console.log(currentIssue);
                conversationPageSize++;
                console.log(conversationPageSize);
                getConversationList('fromIssueConversationList', currentIssue, conversationPageSize, getConversationListCallback);
            }

        } else amountscrolledCount = 0;
    }, false)

    homeBtn.addEventListener('click', backToHome);

    searchField.addEventListener('input', categorySearch);

    searchField.addEventListener('focus', logSearchClick);

    submitBtn.addEventListener('click', submitHandler);

    platformSdk.events.subscribe('onBackPressed', gotoPrevCategory);
    platformSdk.events.subscribe('onUpPressed', gotoPrevCategory);

}(window, window.platformSdk);
